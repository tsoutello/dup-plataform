<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("author_id");
            $table->unsignedBigInteger("client_id");
            $table->string("name");
            $table->unsignedBigInteger("module_model");
            $table->double("module_model_height", 2, 2)->nullable();
            $table->double("module_model_width", 2, 2)->nullable();
            $table->double("module_model_depth", 2, 2)->nullable();
            $table->double("left_finishing_height", 2, 2)->nullable();
            $table->double("left_finishing_width", 2, 2)->nullable();
            $table->double("left_finishing_depth", 2, 2)->nullable();
            $table->unsignedBigInteger("doors_type");
            $table->unsignedBigInteger("wood_thickness_measure")->nullable();
            $table->integer("custom_wood_thickness_measure")->nullable();
            $table->unsignedBigInteger("internal_color")->nullable();
            $table->string("custom_internal_color_code")->nullable();
            $table->string("custom_internal_color_alias")->nullable();
            $table->unsignedBigInteger("external_color")->nullable();
            $table->string("custom_external_color_code")->nullable();
            $table->string("custom_external_color_alias")->nullable();
            $table->unsignedBigInteger("door_puller_type")->nullable();
            $table->string("custom_door_puller_type")->nullable();
            $table->string("custom_door_puller_color_code")->nullable();
            $table->string("custom_door_puller_color_alias")->nullable();
            $table->unsignedBigInteger("drawer_puller_type")->nullable();
            $table->string("custom_drawer_puller_type")->nullable();
            $table->string("custom_drawer_puller_color_code")->nullable();
            $table->string("custom_drawer_puller_color_alias")->nullable();
            $table->unsignedBigInteger("slider");
            $table->unsignedBigInteger("shelf_support");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budgets');
    }
}
