<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_models', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->integer("doors_amount");
            $table->integer("drawers_amount");
            $table->integer("shelves_amount");
            $table->double("width", 4, 2);
            $table->double("height", 4, 2);
            $table->double("depth", 4, 2);
            $table->unsignedBigInteger("image");
            $table->longtext("description")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_models');
    }
}
