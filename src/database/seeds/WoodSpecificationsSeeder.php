<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WoodSpecificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert the data into wood_specifications table
        DB::table('wood_specifications')->insert([
            ["thickness_measure" => 15, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["thickness_measure" => 18, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["thickness_measure" => 25, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);
    }
}
