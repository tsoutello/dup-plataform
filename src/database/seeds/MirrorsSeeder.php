<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MirrorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert the data on mirrors table
        DB::table('mirrors')->insert([
            ["name" => "Comum", "color_name" => "Espelho comum", "image" => 40, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Vidro", "color_name" => "Vidro Branco", "image" => 41, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Bronze", "color_name" => "Espelho Bronze", "image" => 38, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Bronze", "color_name" => "Bronze Reflecto", "image" => 39, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);
    }
}
