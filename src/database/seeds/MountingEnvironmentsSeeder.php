<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MountingEnvironmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert the data into mounting_environments table
        DB::table('mounting_environments')->insert([
            ["ambient_name" => "Quarto", "ambient_image" => 5, "available" => true, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["ambient_name" => "Cozinha", "ambient_image" => 2, "available" => false, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["ambient_name" => "Banheiro", "ambient_image" => 1, "available" => false, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["ambient_name" => "Sala", "ambient_image" => 3, "available" => false, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["ambient_name" => "Outro Ambiente", "ambient_image" => 4, "available" => false, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);
    }
}
