<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SlidersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert the data into sliders table
        DB::table('sliders')->insert([
            // Telescopic
            ["name" => "Telescópia PESADA", "type" => "Telescópica", "image" => 0, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Telescópia PESADA FECHO TOQUE", "type" => "Telescópica", "image" => 0, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],

            // Invisible
            ["name" => "Inivisível Com amortecimento", "type" => "Invisível", "image" => 0, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Inivisível ONETOUCH", "type" => "Invisível", "image" => 0, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);
    }
}
