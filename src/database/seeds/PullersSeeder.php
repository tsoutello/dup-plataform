<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PullersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert the data into pullers table
        DB::table('pullers')->insert([
            // Type G
            ["name" => "Tipo G", "sub_alias" => "Perfil Tipo G", "image" => 57, "top_seller" => true, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);

        DB::table('pullers')->insert([
            ["name" => "Concha", "sub_alias" => "Puxador Concha", "image" => 56, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Cava", "sub_alias" => "Puxador Cava 45", "image" => 54, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Ponto", "sub_alias" => "Inox Polido", "image" => 55, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Collegato", "sub_alias" => "Perfil Collegato", "image" => 53, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Anti Empeno", "sub_alias" => "Inox polido", "image" => 52, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
        ]);
    }
}
