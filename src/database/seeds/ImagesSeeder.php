<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert the images on images table
        DB::table('images')->insert([
            // Environments 1
            ["src" => "uploads/environments/bathroom.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/environments/kitchen.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/environments/living-room.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/environments/other.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/environments/room.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],

            // Models 6
            ["src" => "uploads/models/room/wardrobe/model-1.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-2.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-3.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-4.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-5.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-6.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-7.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-8.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-9.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-10.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-11.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-12.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-13.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-14.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-15.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-17.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-18.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-19.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-20.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-21.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/models/room/wardrobe/model-22.jpg", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);

        DB::table('images')->insert([
            // Other budgets types 27
            ["alias" => "individual-pieces", "src" => "uploads/generic/individual-pieces.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["alias" => "ironware", "src" => "uploads/generic/ironware.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
        ]);

        DB::table('images')->insert([
            // Furnitures 29
            ["src" => "uploads/furnitures/bedside-table.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/furnitures/closet.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/furnitures/headboard.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/furnitures/panel.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/furnitures/suitcase.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/furnitures/wardrobe.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],

            // Structures 35
            ["src" => "uploads/structures/wardrobe/sliding-doors.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/structures/wardrobe/swing-doors.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);

        DB::table('images')->insert([
            // Generic 37
            ["alias" => "advanced-customization", "src" => "uploads/generic/advanced-customization.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
        ]);

        DB::table('images')->insert([
            ["src" => "uploads/generic/bronze-mirror.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/generic/bronze-reflector-mirror.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/generic/common-mirror.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/generic/white-glass.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);

        DB::table('images')->insert([
            // Advanced customization items 42
            ["alias" => "wardrobe-advanced-customization-item", "src" => "advanced-customization/built-in-socket.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["alias" => "wardrobe-advanced-customization-item", "src" => "advanced-customization/ceiling-slope.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["alias" => "wardrobe-advanced-customization-item", "src" => "advanced-customization/machining.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["alias" => "wardrobe-advanced-customization-item", "src" => "advanced-customization/mirror-doors.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["alias" => "wardrobe-advanced-customization-item", "src" => "advanced-customization/tamponade.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["alias" => "wardrobe-advanced-customization-item", "src" => "advanced-customization/thickened-side.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],

            // Pullers 48
            ["alias" => "puller-anodizado", "src" => "uploads/pullers/anodizado.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["alias" => "puller-champagne", "src" => "uploads/pullers/champagne.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["alias" => "puller-preto", "src" => "uploads/pullers/preto.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["alias" => "no-puller", "src" => "uploads/pullers/no-puller.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);

        DB::table('images')->insert([
            ["src" => "uploads/pullers/anti-beding.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/pullers/collegato.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/pullers/pit.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/pullers/point.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/pullers/shell.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["src" => "uploads/pullers/type-g.png", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);
    }
}
