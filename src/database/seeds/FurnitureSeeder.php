<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FurnitureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert the following into furnitures table
        DB::table('furnitures')->insert([
            ["name" => "Guarda-Roupas", "image" => 34, "mounting_environment" => 1, "available" => true, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Criado-mudo", "image" => 29, "mounting_environment" => 1, "available" => false, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Cabeceira", "image" => 31, "mounting_environment" => 1, "available" => false, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Closet", "image" => 30, "mounting_environment" => 1, "available" => false, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Maleiro", "image" => 32, "mounting_environment" => 1, "available" => false, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Painel", "image" => 33, "mounting_environment" => 1, "available" => false, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);
    }
}
