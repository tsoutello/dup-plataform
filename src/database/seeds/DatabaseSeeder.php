<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Call the app's seeders
        $this->call(ColorsSeeder::class);
        $this->call(FurnitureSeeder::class);
        $this->call(ImagesSeeder::class);
        $this->call(MirrorsSeeder::class);
        $this->call(ModuleModelsSeeder::class);
        $this->call(MountingEnvironmentsSeeder::class);
        $this->call(PullersSeeder::class);
        $this->call(ShelfSupportsSeeder::class);
        $this->call(SlidersSeeder::class);
        $this->call(StructuresSeeder::class);
        $this->call(WoodSpecificationsSeeder::class);
    }
}
