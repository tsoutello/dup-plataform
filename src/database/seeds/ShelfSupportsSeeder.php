<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShelfSupportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert the data into shelf_supports table
        DB::table('shelf_supports')->insert([
            ["name" => "Pino Pratelei...", "sub_alias" => "Perfil Tipo G", "image" => 0, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "VB", "sub_alias" => "Puxador Cava 45", "image" => 0, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "VB 56", "sub_alias" => "Puxador Concha", "image" => 0, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "VB Duplo", "sub_alias" => "Sem Puxador", "image" => 0, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);
    }
}
