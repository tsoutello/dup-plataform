<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StructuresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert the data on structures table
        DB::table('structures')->insert([
            ["name" => "Portas de CORRER", "image" => 35, "furniture" => 1, "available" => true, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "Portas de GIRO", "image" => 36, "furniture" => 1, "available" => false, "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);
    }
}
