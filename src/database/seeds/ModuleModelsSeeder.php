<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModuleModelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert the following into module_models table
        DB::table('module_models')->insert([
            // 2 doors
            ["name" => "MODELO 1 (2P8GAV2CAB)", "doors_amount" => 2, "drawers_amount" => 8, "shelves_amount" => 0, "width" => 1.80, "height" => 2.65, "depth" => 0.60, "image" => 6, "description" => "2 Portas deslizantes, 2 Cabideiros, 8 Gavetas e 2 Maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 2 (2P6GAV2SAP2CAB)", "doors_amount" => 2, "drawers_amount" => 6, "shelves_amount" => 0, "width" => 1.80, "height" => 2.65, "depth" => 0.60, "image" => 7, "description" => "2 Portas deslizantes, 2 Cabideiros, 6 Gavetas, 2 Sapateiros e 2 Maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 3 (2P4GAV4SAP2CAB)", "doors_amount" => 2, "drawers_amount" => 4, "shelves_amount" => 0, "width" => 1.80, "height" => 2.65, "depth" => 0.60, "image" => 8, "description" => "2 Portas deslizantes, 2 Cabideiros, 4 Gavetas, 4 Sapateiros e 2 Maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 4 (4GAV3CAB)", "doors_amount" => 4, "drawers_amount" => 8, "shelves_amount" => 0, "width" => 1.80, "height" => 2.65, "depth" => 0.60, "image" => 9, "description" => "2 Portas deslizantes, 3 Cabideiros, 4 Gavetas e 2 Maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 5 (3GAV1SAP3CAB)", "doors_amount" => 3, "drawers_amount" => 8, "shelves_amount" => 0, "width" => 1.80, "height" => 2.65, "depth" => 0.60, "image" => 10, "description" => "2 Portas deslizantes, 3 Cabideiros, 3 Gavetas, 1 Sapateiros e 2 Maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 6 (2GAV2SAP3CAB)", "doors_amount" => 2, "drawers_amount" => 8, "shelves_amount" => 0, "width" => 1.80, "height" => 2.65, "depth" => 0.60, "image" => 11, "description" => "2 Portas deslizantes, 3 Cabideiros, 2 Gavetas, 2 Sapateiros e 2 Maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],

            // 3 doors
            ["name" => "MODELO 7 (8GAV3CAB5PRATP)", "doors_amount" => 3, "drawers_amount" => 8, "shelves_amount" => 5, "width" => 2.20, "height" => 2.65, "depth" => 0.60, "image" => 12, "description" => "3 Portas deslizantes, 3 Cabideiros, 8 Gavetas, 5 Prateleiras e 2 Maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 8 (6GAV2SAP3CAB5PRATP)", "doors_amount" => 3, "drawers_amount" => 6, "shelves_amount" => 5, "width" => 2.20, "height" => 2.65, "depth" => 0.60, "image" => 13, "description" => "3 Portas deslizantes, 3 Cabideiros, 6 Gavetas, 2 Sapateiro, 5 Prateleiras e 3 Maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 9 (4GAV4SAP3CAB5PRATP)", "doors_amount" => 3, "drawers_amount" => 4, "shelves_amount" => 5, "width" => 2.20, "height" => 2.65, "depth" => 0.60, "image" => 14, "description" => "3 Portas deslizantes, 3 Cabideiros, 4 Gavetas, 4 Sapateiro, 5 Prateleiras  e 3 Maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 10 (6GAV2SAP2CAB5PRATG)", "doors_amount" => 3, "drawers_amount" => 6, "shelves_amount" => 5, "width" => 2.20, "height" => 2.65, "depth" => 0.60, "image" => 15, "description" => "3 Portas deslizantes, 2 Cabideiros, 6 Gavetas, 2 Sapateiro, 5 Prateleiras G e 2 Maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 11 (4GAV4SAP2CAB5PRATG)", "doors_amount" => 3, "drawers_amount" => 4, "shelves_amount" => 5, "width" => 2.20, "height" => 2.65, "depth" => 0.60, "image" => 16, "description" => "3 Portas deslizantes, 2 Cabideiros, 4 Gavetas, 4 Sapateiro, 5 Prateleiras G e 2 Maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 12 (4GAV4SAP2CAB5PRATG1GAVP)", "doors_amount" => 3, "drawers_amount" => 5, "shelves_amount" => 5, "width" => 2.20, "height" => 2.65, "depth" => 0.60, "image" => 17, "description" => "3 Portas deslizantes, 2 Cabideiros, 4 Gavetas, 4 Sapateiro, 5 Prateleiras G, 2 Maleiros e 1 Gaveta P <br><br>3 divisões com 2 portas", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 13 (4GAV8SAP2CAB2PRAT)", "doors_amount" => 3, "drawers_amount" => 4, "shelves_amount" => 2, "width" => 2.20, "height" => 2.65, "depth" => 0.60, "image" => 18, "description" => "3 Portas deslizantes, 2 cabideiros, 4 gavetas, 8 sapateiros, 2 prateleiras, 3 maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 14 (4GAV4SAP2CAB4PRAT)", "doors_amount" => 3, "drawers_amount" => 4, "shelves_amount" => 4, "width" => 2.20, "height" => 2.65, "depth" => 0.60, "image" => 19, "description" => "3 Portas deslizantes, 2 cabideiros, 4 gavetas, 4 sapateiros, 4 prateleiras, 3 maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 15 (8GAV2CAB4PRAT)", "doors_amount" => 3, "drawers_amount" => 8, "shelves_amount" => 4, "width" => 2.20, "height" => 2.65, "depth" => 0.60, "image" => 20, "description" => "3 Portas deslizantes, 2 cabideiros, 8 gavetas, 4 prateleiras, 3 maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 16 (4GAV4SAP3CAB2PRAT)", "doors_amount" => 3, "drawers_amount" => 4, "shelves_amount" => 2, "width" => 2.20, "height" => 2.65, "depth" => 0.60, "image" => 21, "description" => "3 Portas deslizantes, 3 cabideiros, 4 gavetas, 4 sapateiros, 2 prateleiras, 3 maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 17 (6GAV6SAP2CAB2PRAT)", "doors_amount" => 3, "drawers_amount" => 6, "shelves_amount" => 2, "width" => 2.20, "height" => 2.65, "depth" => 0.60, "image" => 22, "description" => "3 Portas deslizantes, 2 cabideiros, 6 gavetas, 6 sapateiros, 2 prateleiras, 2 maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],

            // 4 doors
            ["name" => "MODELO 18 (8GAV4SAP4CAB7PRAT)", "doors_amount" => 4, "drawers_amount" => 8, "shelves_amount" => 7, "width" => 3.30, "height" => 2.65, "depth" => 0.60, "image" => 23, "description" => "4 Portas deslizantes, 4 cabideiros, 8 gavetas, 4 sapateiros, 7 prateleiras, 4 maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 19 (6GAV6SAP4CAB7PRAT)", "doors_amount" => 4, "drawers_amount" => 6, "shelves_amount" => 7, "width" => 3.30, "height" => 2.65, "depth" => 0.60, "image" => 24, "description" => "4 Portas deslizantes, 4 cabideiros, 6 gavetas, 6 sapateiros, 7 prateleiras, 4 maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 20 (4GAV8SAP4CAB7PRAT)", "doors_amount" => 4, "drawers_amount" => 4, "shelves_amount" => 7, "width" => 3.30, "height" => 2.65, "depth" => 0.60, "image" => 25, "description" => "4 Portas deslizantes, 4 cabideiros, 4 gavetas, 8 sapateiros, 7 prateleiras, 4 maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 21 (8GAV4CAB10PRAT)", "doors_amount" => 4, "drawers_amount" => 8, "shelves_amount" => 8, "width" => 3.30, "height" => 2.65, "depth" => 0.60, "image" => 26, "description" => "4 Portas deslizantes, 4 cabideiros, 8 gavetas, 10 prateleiras, 4 maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')],
            ["name" => "MODELO 22 (4GAV4SAP5CAB3PRAT)", "doors_amount" => 4, "drawers_amount" => 4, "shelves_amount" => 3, "width" => 3.30, "height" => 2.65, "depth" => 0.60, "image" => 26, "description" => "4 Portas deslizantes, 5 cabideiros, 4 gavetas, 4 sapateira, 3 prateleiras, 4 maleiros", "created_at" =>  date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')]
        ]);
    }
}
