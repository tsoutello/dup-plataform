<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Redirect from / to home
Route::get('/', function () {
    // Redirect to home
    return redirect('/home');
});

// User routes
Route::middleware(['auth'])->group(function () {
    // App Home
    Route::get('/home', 'HomeController@index')->name('home');
    
    
    // User logout
    Route::get('/sair', function () {
        // Logout the user
        Auth::logout();
        
        // Redirect to te login paage
        return redirect('/login');
    })->name('sair');
});

// Authenticated-only routes
Route::middleware(['auth', 'web'])->group(function () {
    /* ------ Ajax Routes ------*/
    Route::post('/user/clients', 'BudgetController@ajaxSearchClients')->name('search-clients');
    Route::post('/models/get-all', 'ModulesController@ajaxGetAll');
    Route::post('/models/get', 'ModulesController@ajaxGetById');
    Route::post('/module/get-slider', 'ModulesController@ajaxGetSliderById');
    Route::post('/budget/prepare/structure', 'BudgetController@ajaxPrepareStructure');
    Route::post('/budget/prepare/appearance', 'BudgetController@ajaxPrepareAppearance');
    Route::post('/budget/update/furniture-name', 'BudgetController@ajaxUpdateFurnitureName');
    Route::post('/budget/new/get', 'BudgetController@ajaxGetBudgetData');
    Route::post('/budget/prepare/new-furniture', 'BudgetController@ajaxPrepareNewFurniture');
    Route::post('/budget/prepare/new-environment', 'BudgetController@ajaxPrepareNewEnvironment');
    
    /* ------ GET Routes ------*/
    Route::get('/budget/new/client', 'BudgetController@client')->name('new-budget-client');
    Route::get('/budget/new', 'BudgetController@initials')->name('new-budget-initial');
    Route::get('/budget/new/furniture', 'BudgetController@furniture')->name('new-budget-furniture');
    Route::get('/budget/new/structure', 'BudgetController@structure')->name('new-budget-structure');
    Route::get('/budget/new/appearance', 'BudgetController@appearance')->name('new-budget-appearance');
    Route::get('/budget/new/overview', 'BudgetController@overview')->name('new-budget-overview');
    Route::get('/budget/new/create', 'BudgetController@create')->name('new-budget-create');
    Route::get('/budget/{id}', 'BudgetController@getOverview');
    Route::get('/budget/new/ironwares', 'BudgetController@ironwaresOnlyView')->name('new-ironwares-budgets');
    Route::get('/budget/new/pieces', 'BudgetController@piecesOnlyView')->name('new-pieces-budgets');


    /* ------ POST Routes ------*/
    Route::post('/budget/new/client', 'BudgetController@addClientToBudget');
    Route::post('/budget/new', 'BudgetController@addInitials');
    Route::post('/budget/new/furniture', 'BudgetController@addFurniture');
    Route::post('/budget/new/structure', 'BudgetController@addStructure');
    Route::post('/budget/new/appearance', 'BudgetController@addAppearance');
    Route::post('/budget/new/ironwares', 'BudgetController@createIronwaresBudget');
    Route::post('/budget/new/pieces', 'BudgetController@createPiecesOnlyBudget');
});

// Authentication routes
Auth::routes();