/**
 * Event handler for password togglers clicks.
 * 
 * @returns {void}
 */
$(".password-toggler").click(function () {
    // Get the prev element to the handler, always the input field
    let field = $(this).prev();

    // Stores the next type for $(field)
    let $nextType = "";

    // Verifies the "type" attribute of $(field)
    if ($(field).attr("type") === "password") {
        // Set $nextType to "text"
        $nextType = "text";
    } else {
        // Set $nextType to "password"
        $nextType = "password";
    }

    // Set "type" attribute of $(field) to $nextType
    $(field).attr("type", $nextType);

    // Set focus back to the field
    $(field).focus();
});