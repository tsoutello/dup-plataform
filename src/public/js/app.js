/**
 * Uncheck the sidebar toggler when document is loaded.
 * 
 * @return {void}
 */
$(document).ready(() => {
    // Stores the sidebar toggler element
    toggler = $("#sidebar-toggler");

    // Verifies if the $(toggler) element exists
    if (typeof toggler !== undefined) {
        // Uncheck $(toggler)
        $(toggler).prop("checked", false);

        // Removes "d-none" class from $(".sidebar")
        $(".sidebar").removeClass("d-none");
    }
})