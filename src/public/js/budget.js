/**
 * Create (or reset) a HTML table.
 * 
 * @param {string} target
 * @param {object} $data
 * @param {array} $actions (optional)
 * 
 * @return void
 */
function table(target, $data, $actions=[]) {
    // Stores the table HTML
    let $table = "";

    // Loop $data
    for ($row in $data) {
        // Add a <tr> tag to $table
        $table += "<tr>";

        // Loop $data[$row]
        for ($item in $data[$row]) {
            // Verifies if the current item value is null
            if ($data[$row][$item] === null) {
                // Skip to next loop executations
                continue;
            }

            // Add a <td> tag with $item value to $table
            $table += `<td>${$data[$row][$item]}</td>`;
        }

        if ($actions !== {}) {
            // Adds a <td> to $table
            $table += "<td>";

            // Loop $actions
            for ($action in $actions) {
                // Add a action button to $table
                $table += `<button style="width: auto" class="btn btn-sm btn-${$actions[$action][0]} ${$action}">${$actions[$action][1]}</button>`
            }

            // Close the opened <td>
            $table += "</td>";
        }

        // Closes open <tr> tag in $table
        $table += "</tr>";
    }

    // Add $table HTML to target tbody
    $(`${target} tbody`).html($table);
}