// Set #module-total-width width automatically on changes in finishing settings
function getModuleTotalWidth() {
    // Get module viewers width
    let $leftFinishingWidth  = $(".left-finishing-view").width();
    let $rightFinishingWidth = $(".right-finishing-view").width();
    let $moduleHeightWidth   = $(".module-height-view").width();
    let $moduleImageWidth    = $(".module-size-view").width();
    let $computedWidth       = $leftFinishingWidth + $rightFinishingWidth + $moduleHeightWidth + $moduleImageWidth;

    // Set new width to the target
    $("#module-total-width").css({
        width: `${$computedWidth}px`
    });

    // Stores module width (in real word)
    let $moduleWidth = 0;

    // Get the finishing setting that was checked
    let $finishingSetting = $("input[name='finishing']:checked").val();

    // Switch in $finishingSetting
    switch ($finishingSetting) {
        // Left finishing selected
        case "left-finishing":
                // Add the left finishing width to $moduleWidth
                $moduleWidth += parseInt($("#left-finishing-view-measure").text());

                // End case
                break;

            // Right finishing selected
            case "right-finishing":
                // Add the right finishing width to $moduleWidth
                $moduleWidth += parseInt($("#right-finishing-view-measure").text());
                
                // End case
                break;

            // Both finshings selected
            case "both-finishing":
                // Add the both right and left finishings width to $moduleWidth
                $moduleWidth += (
                    parseInt($("#left-finishing-view-measure").text()) +
                    parseInt($("#right-finishing-view-measure").text())
                );

                // End case
                break;

            // No finishing selected, or any invalid set of ids
            default:
                // End default
                break;
    }

    // Add the module width to $moduleWidth
    $moduleWidth += parseInt($("#module-width-measure").attr("data-width-measure"));

    // Refresh module width viewer value
    $("#module-total-width-value").text($moduleWidth);
}

// Verifies which finishing option was selected and executes the necessary tweaks in DOM
function getFinishingViewConfiguration() {
    // Loop all finishing radio inputs
    $("input[name='finishing']").each(function () {
        // Get the current element ID
        let $targetId = $(this).attr('id');

        // Verifies if the current element is checked
        if ($(this).is(':checked') == false) {
            // Skips to the next element
            return;
        }

        // Switch in $targetId
        switch ($targetId) {
            // Left finishing selected
            case "left-finishing":
                // Hide right finishing viewer and controls
                $(".right-finishing-view").addClass("d-none");
                $("#right-finishing-control").addClass("d-none");

                // Show left finishing viewer and controls
                $(".left-finishing-view").removeClass("d-none");
                $("#left-finishing-control").removeClass("d-none");
                
                // End case
                break;

            // Right finishing selected
            case "right-finishing":
                // Show right finishing viewer and controls
                $(".right-finishing-view").removeClass("d-none");
                $("#right-finishing-control").removeClass("d-none");

                // Hide left finishing viewer and controls
                $(".left-finishing-view").addClass("d-none");
                $("#left-finishing-control").addClass("d-none");
                
                // End case
                break;

            // Both finshings selected
            case "both-finishing":
                // Show right finishing viewer
                $(".right-finishing-view").removeClass("d-none");
                $("#right-finishing-control").removeClass("d-none");

                // Show left finishing viewer and controls
                $(".left-finishing-view").removeClass("d-none");
                $("#left-finishing-control").removeClass("d-none");
                
                // End case
                break;

            // No finishing selected, or any invalid set of ids
            default:
                // Hide right finishing viewer
                $(".right-finishing-view").addClass("d-none");
                $("#right-finishing-control").addClass("d-none");

                // Hide left finishing viewer
                $(".left-finishing-view").addClass("d-none");
                $("#left-finishing-control").addClass("d-none");
                
                // End default
                break;
        }

        // Get new module width size
        getModuleTotalWidth();
    });
}

// Link the value of all height inputs
function changeModelHeight() {
    // Stores the linked inputs
    let $inputs = [
        $('input[name="module-height"]'),
        $('input[name="left-finishing-height"]'),
        $('input[name="right-finishing-height"]'),
    ];

    // Get the item value
    let $newHeight = $(this).val();

    // Loop in $inputs
    for ($i in $inputs) {
        // Change the linked input value
        $($inputs[$i]).val($newHeight);
    }

    // Change the module height viewer value
    $("#module-height-viewer").text(`${$newHeight} CHÃO AO TETO`);
}

// Link the value of all depth inputs
function changeModelDepth() {
    // Stores the linked inputs
    let $inputs = [
        $('input[name="module-depth"]'),
        $('input[name="left-finishing-depth"]'),
        $('input[name="right-finishing-depth"]'),
    ];

    // Get the item value
    let $newDepth = $(this).val();

    // Loop in $inputs
    for ($i in $inputs) {
        // Change the linked input value
        $($inputs[$i]).val($newDepth);
    }
}

// Link the left finishing control with his viewer
function leftFinishingViewerLink() {
    // Get the value of left finishing width control
    let $leftFinishingWidth = $("input[name='left-finishing-width']").val();

    // Verifies if the finishings are linked
    if ($("#both-finishing").is(":checked") == true) {
        // Set this value of control and viewer to right finishing too
        $("input[name='right-finishing-width']").val($leftFinishingWidth);
        $("#right-finishing-view-measure").text($leftFinishingWidth);
    }

    // Verifies if new width is null
    if ($leftFinishingWidth.length == 0) {
        // Set new width to zero
        $leftFinishingWidth = "0";
    }

    // Set the left finishing viewer value
    $("#left-finishing-view-measure").text($leftFinishingWidth);

    // Refresh module total width
    getModuleTotalWidth();
}

// Link the right finishing control with his viewer
function rightFinishingViewerLink() {
    // Get the value of right finishing width control
    let $rightFinishingWidth = $("input[name='right-finishing-width']").val();

    // Verifies if the finishings are linked
    if ($("#both-finishing").is(":checked") == true) {
        // Set this value of control and viewer to left finishing too
        $("input[name='left-finishing-width']").val($rightFinishingWidth);
        $("#left-finishing-view-measure").text($rightFinishingWidth);
    }

    // Verifies if new width is null
    if ($rightFinishingWidth.length == 0) {
        // Set new width to zero
        $rightFinishingWidth = "0";
    }

    // Set the right finishing viewer value
    $("#right-finishing-view-measure").text($rightFinishingWidth);

    // Refresh module total width
    getModuleTotalWidth();
}

// Link module width with his viewer
function moduleWidhtViewerLink() {
    // Get the value to the new module width
    let $newWidth = $("input[name='module-width']").val();

    // Verifies if new width is null
    if ($newWidth.length == 0) {
        // Set new width to zero
        $newWidth = "0";
    }

    // Set the viewer width attribute to new width
    $("#module-width-measure").attr("data-width-measure", $newWidth);

    // Refresh module total width
    getModuleTotalWidth();
}

// Manage model selection modal functions
function openModelsSelection(e) {
    // Stop the anchor event
    e.preventDefault();

    // Ajax to get modules
    $.ajax({
        url: '/models/get-all',
        method: 'POST',
        data: {
            _token: $("input[name='_token']").val()
        },

        success: ($response) => {
            // Parse received JSON data
            let $data = JSON.parse($response);
            
            // Stores modal HTML content
            let $html = "";
            
            // Loop in $data
            for ($group in $data) {
                // Add a models section to $html
                $html += `
                    <!-- modules_section_start -->
                    <div class="row">
                        <div class="col-12">
                            <!-- model_section_title -->
                            <h4 class="module-model-section-title">${$data[$group]['doors']} portas</h4>
                        </div>
                    </div>
                    <!-- modules_section_end -->
                `;
                        
                // Loop to get models in group
                for ($module in $data[$group]["data"]) {
                    // Stores array path, to short code
                    $module = $data[$group]["data"][$module];

                    // Module data, separed in variables
                    let $id          = $module['id'];
                    let $image       = `/${$module['image']}`;
                    let $name        = $module['name'];
                    let $doors       = $module['doors'] + ' portas';
                    let $shelves     = ($module['shelves'] == "1" ? $module['shelves'] + " prateleira" : ($module['shelves'] == 0 ? "Nenhuma prateleira" :$module['shelves'] + " prateleiras"));
                    let $drawers     = ($module['drawers'] == "1" ? $module['drawers'] + " gaveta" : ($module['drawers'] == 0 ? "Nenhuma gaveta" :$module['drawers'] + " gavetas"));
                    let $description = $module['description'];

                    // Mount module DOM tree
                    $html += `
                        <!-- section_model_start -->
                        <div class="row white-box mt-2" style="width: 104%">
                            <!-- module_image_wrapper_start -->
                            <div class="col-5 py-2">
                                <!-- module_image -->
                                <img src="${$image}" height="128" width="112" class="border rounded select-model" data-model-id="${$id}">
                            </div>
                            <!-- module_image_wrapper_end -->

                            <!-- module_info_wrapper_start -->
                            <div class="col-7 d-flex flex-column pl-2 pt-3">
                                <!-- module_name -->
                                <h5 class="poppins font-medium text-uppercase text-primary-gray" style="font-size: .8rem">${$name}</h5>
                                
                                <!-- module_drawers -->
                                <span class="poppins font-small text-secondary-gray">${$drawers}</span>
                                
                                <!-- module_shelves -->
                                <span class="poppins font-small text-secondary-gray">${$shelves}</span>
                                
                                <!-- module_doors -->
                                <span class="poppins font-small text-secondary-gray">${$doors}</span>

                                <!-- open_module_details -->
                                <a class="poppins font-small font-regular pt-2 open-model-details" data-toggle="collapse" href="#module-details-${$id}" role="button">Ver detalhes</a>
                            </div>
                            <!-- module_info_wrapper_end -->
                        </div>
                        <!-- section_model_end -->

                        <!-- previous_model_description_start -->
                        <div class="row description-box collapse" id="module-details-${$id}">
                            <!-- module_image_wrapper_start -->
                            <div class="col-4 py-4">
                                <!-- module_image -->
                                <img src="${$image}" height="82" width="71" class="border rounded muted">
                            </div>
                            <!-- module_image_wrapper_end -->

                            <!-- module_info_wrapper_start -->
                            <div class="col-7 d-flex flex-column pl-2 pt-3">
                                <!-- module_info_start -->
                                <h5 class="poppins font-small font-medium text-uppercase text-primary-gray">
                                    ${$drawers}, ${$shelves}, ${$doors}
                                </h5>
                                <!-- module_info_end -->

                                <!-- module_description_start -->
                                <span class="poppins font-extra-small font-medium text-secondary-gray">
                                    ${$description}
                                </span>
                                <!-- module_description_end -->
                            </div>
                            <!-- module_info_wrapper_end -->
                        </div>
                        <!-- previous_model_description_end -->
                    `;
                }
            }

            // Fill the models modal
            $("#module-models-body").html($html);

            // Set event listener to handle model selections
            $(".select-model").on("click", selectModel);

            // Open the models modal
            $("#module-models-modal").modal('toggle');
        }
    });
}

// Set a selected model to use in structure
function selectModel() {
    // Get model id
    let $id = $(this).attr('data-model-id');

    // Ajax to get model information
    $.ajax({
        url: '/models/get',
        method: 'POST',
        data: {
            id: $id,
            _token: $("input[name='_token']").val()
        },

        success: ($response) => {
            // Parse JSON response
            let $data = JSON.parse($response);

            console.log($data);

            // Stores returned data in separated variables
            let $id     = $data["id"];
            let $name   = $data["name"];
            let $doors  = $data["doors"];
            let $image  = `/${$data["image"]}`;
            let $height = parseFloat($data["height"]) * 1000;
            let $width  = parseFloat($data["width"]) * 1000;
            let $depth  = parseFloat($data["depth"]) * 1000;

            // Get the old model src
            let $oldSrc = $("#model-image").attr("src");

            // Replace the old image src with the new
            $(`img[src='${$oldSrc}']`).attr("src", $image);

            // Replace the module size controls
            $("input[name='module-height']").val($height);
            $("input[name='module-width']").val($width);
            $("input[name='module-depth']").val($depth);

            // Set new model id
            $("input[name='model-id']").val($id);

            // Replace model sizes viewer
            $("#module-width-measure").attr('data-width-measure', $width);
            $("#module-height-viewer").text(`${$height} CHÃO AO TETO`);

            // Replace model data
            $("#model-doors").text(`${$doors} portas`);
            $("#model-name").text($name);

            // Get new module width size
            getModuleTotalWidth();

            // Close the models modal
            $("#module-models-modal").modal('toggle');
        }
    });
}