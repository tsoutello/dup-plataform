@extends('layouts.home.app')
@section('title', 'Início')

@push('css')
    <style type="text/css">
        .white-box {
            margin: .25rem 0.0rem;
            background: #fff;
            border: 1.5px solid #E7E7E7;
            box-sizing: border-box;
            box-shadow: 0px 3px 12px rgba(0, 0, 0, 0.1);
            border-radius: 4px;
        }
    </style>
@endpush

@section('content')
    <!-- content_wrapper_start -->
    <div class="container">
        <!-- page_sum_start -->
        <div class="row">
            <!-- budgets_sum_start -->
            <div class="col-sm-6 d-flex flex-column poppins">
                <!-- budgets_num_text -->
                <h4 class="text-dark-blue font-medium font-size-default">Seus Orçamentos:</h4>

                <!-- budgets_num -->
                <h6 class="text-secondary-gray font-medium font-small">{{ $budgetsNum }} orçamentos</h6>
            </div>
            <!-- budgets_sum_end -->

            <!-- create_new_budget_start -->
            <div class="col-sm-6">
                <!-- new_budget_button_start -->
                <a href="{{ route('new-budget-client') }}" class="btn btn-md btn-outline-primary poppins text-uppercase font-small">
                    <!-- add_icon -->
                    <img src="{{ asset('assets/icons/home/primary-blue-add.png') }}" alt="primary-blue-add" width="16" height="16">
                    Novo Orçamento
                </a>
                <!-- new_budget_button_end -->
            </div>
            <!-- create_new_budget_end -->
        </div>
        <!-- page_sum_end -->

        {{-- Show user budgets, if was --}}
        @if (isset($budgets))
            @foreach($budgets as $budget)
                <div class="row white-box go-to-budget py-2 px-2" data-budget-id="{{ $budget->id }}">
                    <!-- user_alias_wrapper_start -->
                    <div class="col-3">
                        <div style="background: #E9F1FB; color: #0F4589; height: 40px; border-radius: 100%; width: 40px; text-align: center; line-height: 40px; font-size: 1.2rem;">
                            <!-- user_alias -->
                            <span>{{ $budget->clientName[0] }}</span>
                        </div>
                    </div>
                    <!-- user_alias_wrapper_end -->

                    <!-- user_name_wrapper_start -->
                    <div class="col-9 d-flex flex-column poppins">
                        <!-- client_name -->
                        <span class="font-medium font-small text-dark-blue">{{ $budget->clientName }}</span>

                        <!-- budget_date_time -->
                        <span class="font-medium font-small text-secondary-gray">Criado {{ $budget->diff }} - {{ $budget->createdAt }}</span>
                    </div>
                    <!-- user_name_wrapper_end -->
                </div>
            @endforeach
        @endif
    </div>
    <!-- content_wrapper_end -->
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(() => {
            $(".go-to-budget").click(function (e) {
                e.preventDefault();

                // Redirect user to budget api
                window.location.href = `{{ url('/') }}/budget/${$(this).attr("data-budget-id")}`;
            });
        });
    </script>

    @if (Session::has('message'))
        <script type="text/javascript">
            alert("{{ Session::get('message') }}");
        </script>
    @endif
@endpush