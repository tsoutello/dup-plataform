@extends('layouts.guest.app')

@section('title', 'Criar conta')

@section('content')
   <!-- content_wrapper_start -->
    <div class="container">
        <!-- header_text_wrapper_start -->
        <div class="row">
            <div class="col-12">
                <!-- header_text_start -->
                <h1 class="header-text poppins font-semi-bold">
                    Criar Conta
                </h1>
                <!-- header_text_end -->
            </div>
        </div>
        <!-- header_text_wrapper_end -->

        <!-- register_info_message_wrapper_start -->
        <div class="row mt-1">
            <div class="col-12">
                <!-- register_message_info_start -->
                <p class="poppins font-medium text-dark-blue">
                    Insira seus dados para criar sua conta na DUP:
                </p>
                <!-- register_message_info_end -->
            </div>
        </div>
        <!-- register_info_message_wrapper_end -->

        <!-- form_wrapper_start -->
        <div class="row mt-1">
            <div class="col-12">
                <!-- form_start -->
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <!-- name_field_wrapper_start -->
                    <div class="form-group row">
                        <!-- field_label -->
                        <label for="name" class="col-md-4 col-form-label text-md-right poppins font-medium">Nome Completo:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-md-6">
                            <!-- input -->
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Digite seu nome" name="name" required autocomplete="name">

                            @error('name')
                                <!-- input_error_message_start -->
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <!-- input_error_message_end -->
                            @enderror
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- name_field_wrapper_start -->

                    <!-- email_field_wrapper_start -->
                    <div class="form-group row mt-4">
                        <!-- field_label -->
                        <label for="email" class="col-md-4 col-form-label text-md-right">E-mail de Acesso:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-md-6">
                            <!-- input -->
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Digite seu e-mail" required autocomplete="email">

                            @error('email')
                                <!-- input_error_message_start -->
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <!-- input_error_message_end -->
                            @enderror
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- email_field_wrapper_end -->

                    <!-- password_field_wrapper_start -->
                    <div class="form-group row mt-4">
                        <!-- field_label -->
                        <label for="password" class="col-md-4 col-form-label text-md-right">Senha de Acesso:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-md-6 d-flex">
                            <!-- input -->
                            <input id="password" type="password" class="form-control with-icon @error('password') is-invalid @enderror" name="password" placeholder="Digite sua senha" required autocomplete="new-password">
                            
                            <!-- icon_image -->
                            <img src="{{ asset('assets/icons/show-password.png') }}" alt="show-password" class="img-fluid form-control-icon password-toggler">

                            @error('password')
                                <!-- input_error_message_start -->
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <!-- input_error_message_end -->
                            @enderror
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- password_field_wrapper_end -->

                    <!-- company_field_wrapper_start -->
                    <div class="form-group row mt-4">
                        <!-- field_label -->
                        <label for="company" class="col-md-4 col-form-label text-md-right poppins font-medium">Nome da sua empresa:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-md-6">
                            <!-- input -->
                            <input id="company" type="text" class="form-control @error('company') is-invalid @enderror" placeholder="Digite o nome" name="company">

                            @error('company')
                                <!-- input_error_message_start -->
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <!-- input_error_message_end -->
                            @enderror
                        </div>
                        <!-- input_wrapper_end -->

                        <!-- field_subtitle_wrapper_start -->
                        <div class="col-md-4">
                            <!-- field_subtitle -->
                            <p class="form-control-subtitle">Se você não tem uma empresa, deixe vazio</p>
                        </div>
                        <!-- field_subtitle_wrapper_end -->
                    </div>
                    <!-- company_field_wrapper_end -->

                    <!-- form_submit_wrapper_start -->
                    <div class="form-group row mt-4">
                        <!-- button_wrapper_start -->
                        <div class="col-md-6 text-center">
                            <!-- submit_button -->
                            <button type="submit" class="btn btn-primary">Criar conta</button>
                        </div>
                        <!-- button_wrapper_end -->
                    </div>
                    <!-- form_submit_wrapper_end -->
                </form>
                <!-- form_end -->
                
            </div>
        </div>
        <!-- form_wrapper_end -->
        
        <!-- page_footer_wrapper_start -->
        <div class="row mt-4">
            <div class="col-12">
                <!-- page_footer_text_start -->
                <p class="page-footer-text poppins font-medium text-center">
                    Já possui conta? <a href="{{ route('login') }}" class="font-regular">Fazer Login</a>
                </p>
                <!-- page_footer_text_start -->
            </div>
        </div>
        <!-- page_footer_wrapper_end -->
    </div>
    <!-- content_wrapper_end -->
@endsection
