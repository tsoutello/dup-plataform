
@extends('layouts.guest.app')

@section('title', 'Criar Nova Senha')

@section('content')
   <!-- content_wrapper_start -->
    <div class="container">
        <!-- header_text_wrapper_start -->
        <div class="row">
            <div class="col-12">
                <!-- header_text_start -->
                <h1 class="header-text poppins font-semi-bold">
                    Criar Nova Senha
                </h1>
                <!-- header_text_end -->
            </div>
        </div>
        <!-- header_text_wrapper_end -->

        <!-- current-email_info_message_wrapper_start -->
        <div class="row mt-1">
            <div class="col-12">
                <!-- reset_message_info_start -->
                <p class="poppins font-medium text-dark-blue">
                    Digite seu e-mail ATUAL:
                </p>
                <!-- reset_message_info_end -->
            </div>
        </div>
        <!-- current-email_info_message_wrapper_end -->

        <!-- form_start -->
        <form method="POST" action="{{ route('password.update') }}">
            <!-- form_wrapper_start -->
            <div class="row mt-1">
                <div class="col-12">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <!-- email_field_wrapper_start -->
                        <div class="form-group row mt-2">
                            <!-- field_label -->
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-mail de Acesso Atual:</label>

                            <!-- input_wrapper_start -->
                            <div class="col-md-6">
                                <!-- input -->
                                <input id="email" type="email" class="form-control reverse-border @error('email') is-invalid @enderror" name="email" placeholder="Digite seu e-mail" required autocomplete="email">

                                @error('email')
                                    <!-- input_error_message_start -->
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    <!-- input_error_message_end -->
                                @enderror
                            </div>
                            <!-- input_wrapper_end -->
                        </div>
                        <!-- email_field_wrapper_end -->

                    </div>
                </div>
                <!-- form_wrapper_end -->

                <!-- new-password_info_message_wrapper_start -->
                <div class="row mt-4">
                    <div class="col-12">
                        <!-- reset_message_info_start -->
                        <p class="poppins font-medium text-dark-blue">
                            Cadastre sua NOVA senha:
                        </p>
                        <!-- reset_message_info_end -->
                    </div>
                </div>
                <!-- new-password_info_message_wrapper_end -->

                <!-- password_field_wrapper_start -->
                <div class="form-group row mt-3">
                    <!-- field_label -->
                    <label for="password" class="col-md-4 col-form-label text-md-right">Nova Senha de Acesso:</label>

                    <!-- input_wrapper_start -->
                    <div class="col-md-6 d-flex">
                        <!-- input -->
                        <input id="password" type="password" class="form-control with-icon @error('password') is-invalid @enderror" name="password" placeholder="Digite sua senha" required autocomplete="new-password">
                        
                        <!-- icon_image -->
                        <img src="{{ asset('assets/icons/show-password.png') }}" alt="show-password" class="img-fluid form-control-icon password-toggler">

                        @error('password')
                            <!-- input_error_message_start -->
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            <!-- input_error_message_end -->
                        @enderror
                    </div>
                    <!-- input_wrapper_end -->
                </div>
                <!-- password_field_wrapper_end -->

                <!-- password-confirm_field_wrapper_start -->
                <div class="form-group row mt-3">
                    <!-- field_label -->
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar Nova Senha:</label>

                    <!-- input_wrapper_start -->
                    <div class="col-md-6 d-flex">
                        <!-- input -->
                        <input id="password-confirm" type="password" class="form-control with-icon @error('password-confirm') is-invalid @enderror" name="password_confirmation" placeholder="Digite sua senha" required autocomplete="new-password">
                        
                        <!-- icon_image -->
                        <img src="{{ asset('assets/icons/show-password.png') }}" alt="show-password" class="img-fluid form-control-icon password-toggler">

                        @error('password-confirm')
                            <!-- input_error_message_start -->
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            <!-- input_error_message_end -->
                        @enderror
                    </div>
                    <!-- input_wrapper_end -->
                </div>
                <!-- password-confirm_field_wrapper_end -->
                
                <!-- form_submit_wrapper_start -->
                <div class="form-group row mt-5">
                    <!-- button_wrapper_start -->
                    <div class="col-md-6 text-right">
                        <!-- submit_button -->
                        <button type="submit" class="btn btn-primary text-uppercase">Salvar Senha</button>
                    </div>
                    <!-- button_wrapper_end -->
                </div>
                <!-- form_submit_wrapper_end -->
        </form>
        <!-- form_end -->
    </div>
    <!-- content_wrapper_end -->
@endsection

