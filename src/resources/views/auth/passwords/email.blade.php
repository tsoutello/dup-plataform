<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

@extends('layouts.guest.app')

@section('content')
   <!-- content_wrapper_start -->
    <div class="container">
        <!-- header_text_wrapper_start -->
        <div class="row">
            <div class="col-12">
                <!-- header_text_start -->
                <h1 class="header-text poppins font-semi-bold">
                    Recuperar Senha
                </h1>
                <!-- header_text_end -->
            </div>
        </div>
        <!-- header_text_wrapper_end -->

        <!-- reset_info_message_wrapper_start -->
        <div class="row mt-1">
            <div class="col-12">
                <!-- reset_message_info_start -->
                <p class="poppins font-medium text-dark-blue">
                    Digite seu email para a recuperação
                </p>
                <!-- reset_message_info_end -->
            </div>
        </div>
        <!-- reset_info_message_wrapper_end -->

        <!-- form_wrapper_start -->
        <div class="row mt-1">
            <div class="col-12">
                <!-- form_start -->
                <form method="POST" action="{{ route('password.email') }}" id="recovery-form">
                    @csrf

                    <!-- email_field_wrapper_start -->
                    <div class="form-group row mt-2">
                        <!-- field_label -->
                        <label for="email" class="col-md-4 col-form-label text-md-right">E-mail de Acesso:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-md-6">
                            <!-- input -->
                            <input id="email" type="email" class="form-control reverse-border @error('email') is-invalid @enderror" name="email" placeholder="Digite seu e-mail" required autocomplete="email">

                            @error('email')
                                <!-- input_error_message_start -->
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <!-- input_error_message_end -->
                            @enderror
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- email_field_wrapper_end -->

                    <!-- form_submit_wrapper_start -->
                    <div class="form-group row mt-5">
                        <!-- button_wrapper_start -->
                        <div class="col-md-6 text-center">
                            <!-- submit_button -->
                            <button type="submit" class="btn btn-primary text-uppercase">Enviar código</button>
                        </div>
                        <!-- button_wrapper_end -->
                    </div>
                    <!-- form_submit_wrapper_end -->
                </form>
                <!-- form_end -->
            </div>
        </div>
        <!-- form_wrapper_end -->
    </div>
    <!-- content_wrapper_end -->
@endsection
