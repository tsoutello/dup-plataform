@extends('layouts.guest.app')

@section('title', 'Login')

@section('content')
    <!-- content_wrapper_start -->
    <div class="container">
        <!-- header_text_wrapper_start -->
        <div class="row pl-2">
            <div class="col-12">
                <!-- header_text_start -->
                <h1 class="header-text poppins font-semi-bold">
                    Fazer Login
                </h1>
                <!-- header_text_end -->
            </div>
        </div>
        <!-- header_text_wrapper_end -->

        <!-- form_wrapper_start -->
        <div class="row mt-1">
            <div class="col-12">
                <!-- form_start -->
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <!-- email_field_wrapper_start -->
                    <div class="form-group row mt-4">
                        <!-- field_label -->
                        <label for="email" class="col-md-4 col-form-label text-md-right">E-mail de Acesso:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-md-6">
                            <!-- input -->
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Digite seu e-mail" required autocomplete="email">

                            @error('email')
                                <!-- input_error_message_start -->
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <!-- input_error_message_end -->
                            @enderror
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- email_field_wrapper_end -->

                    <!-- password_field_wrapper_start -->
                    <div class="form-group row mt-4">
                        <!-- field_label -->
                        <label for="password" class="col-md-4 col-form-label text-md-right">Senha de Acesso:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-md-6 d-flex">
                            <!-- input -->
                            <input id="password" type="password" class="form-control with-icon @error('password') is-invalid @enderror" name="password" placeholder="Digite sua senha" required autocomplete="new-password">
                            
                            <!-- icon_image -->
                            <img src="{{ asset('assets/icons/show-password.png') }}" alt="show-password" class="img-fluid form-control-icon password-toggler">

                            @error('password')
                                <!-- input_error_message_start -->
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <!-- input_error_message_end -->
                            @enderror
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- password_field_wrapper_end -->
                    
                    <!-- form_extras_wrapper_start -->
                    <div class="form-group row mt-2">
                        <div class="col-12 poppins">
                            <!-- checkbox_field -->
                            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} aria-label="Manter-me conectado">

                            <!-- checkbox-label -->
                            <label for="remember" class="pl-2 col-form-label">Manter-me conectado</label>
                            
                            <!-- recover_passwor_link -->
                            <a href="{{ route('password.request') }}" class="font-small font-regular text-right pl-4">Esqueci minha senha</a>
                        </div>
                    </div>
                    <!-- form_extras_wrapper_start -->

                    <!-- form_submit_wrapper_start -->
                    <div class="form-group row mt-4">
                        <!-- button_wrapper_start -->
                        <div class="col-md-6 text-center">
                            <!-- submit_button -->
                            <button type="submit" class="btn btn-primary">Entrar</button>
                        </div>
                        <!-- button_wrapper_end -->
                    </div>
                    <!-- form_submit_wrapper_end -->
                </form>
                <!-- form_end -->
                
            </div>
        </div>
        <!-- form_wrapper_end -->
        
        <!-- page_footer_wrapper_start -->
        <div class="row mt-4">
            <div class="col-12">
                <!-- page_footer_text_start -->
                <p class="page-footer-text poppins font-medium text-center">
                    Novo na DUP? <a href="{{ route('register') }}" class="font-regular">Crie sua conta</a>
                </p>
                <!-- page_footer_text_start -->
            </div>
        </div>
        <!-- page_footer_wrapper_end -->
    </div>
    <!-- content_wrapper_end -->
@endsection
