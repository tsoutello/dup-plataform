<body bgcolor="#e6e6e6" style="margin:0; padding:0; font-family: Arial, sans-serif; color: #4F4F4F; max-width: 700px; font-size: 1rem">
    <div style="margin: 2rem; padding: 2rem; border: 0; border-radius: .6rem; background-color: #ffffff">
        <h1>Novo Orçamento realizado</h1>
        <h4 style="font-weight: 400">Um novo orçamento foi criado. Abaixo você pode consultar os dados do mesmo, assim como o contato do cliente.<h4>

        <h3>Dados do cliente:</h3>
        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="150">Nome</th>
                    <th width="400">Email</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $clientData->user }}</td>
                    <td>{{ $clientData->email }}</td>
                </tr>
            </tbody>
        </table>

        <h3>Ambientes</h3>
        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="520">Nome</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($environments as $environment)
                    <tr>
                        <td>{{ $environment->target }}</td>
                        <td>{{ $environment->name }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <h3>Módulos</h3>
        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="90">Ambiente</th>
                    <th width="460">Nome</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($modules as $module)
                    <tr>
                        <td>{{ $module->target }}</td>
                        <td>{{ $module->envTarget }}</td>
                        <td>{{ $module->name }}</td>
                    </tr>
                @endforeach;
            </tbody>
        </table>

        <h3>Estrutura do Módulo</h3>

        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="80">Ambiente</th>
                    <th width="125">Módulo</th>
                    <th width="130">Acab. Esquerdo</th>
                    <th width="150">Acab. Direito</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($modulesData as $module)
                    <tr>
                        <td>{{ $module->target }}</td>
                        <td>{{ $module->envTarget }}</td>
                        <td>{{ $module->name }}</td>
                        <td>{{ $module->left }}</td>
                        <td>{{ $module->right }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <br>

        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="80">Ambiente</th>
                    <th width="125">Portas</th>
                    <th width="130">Espelho</th>
                    <th width="150">Cor</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($advancedCustomization as $customizationSet)
                    <tr>
                        <td>{{ $customizationSet->target }}</td>
                        <td>{{ $customizationSet->envTarget }}</td>
                        <td>{{ $customizationSet->doors }}</td>
                        <td>{{ $customizationSet->mirror }}</td>
                        <td>{{ $customizationSet->mirrorColor }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <br>

        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="80">Ambiente</th>
                    <th width="125">Altura (Módulo)</th>
                    <th width="130">Largura (Módulo)</th>
                    <th width="150">Profundidade (Módulo)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($moduleSizes as $moduleData)
                    <tr>
                        <td>{{ $moduleData->target }}</td>
                        <td>{{ $moduleData->envTarget }}</td>
                        <td>{{ $moduleData->height }}mm</td>
                        <td>{{ $moduleData->width }}mm</td>
                        <td>{{ $moduleData->depth }}mm</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <br>

        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="80">Ambiente</th>
                    <th width="125">Altura (Acab. Esq.)</th>
                    <th width="130">Largura (Acab. Esq)</th>
                    <th width="150">Profundidade (Acab. Esq)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($leftFinishings as $leftFinishing)
                    <tr>
                        <td>{{ $leftFinishing->target }}</td>
                        <td>{{ $leftFinishing->envTarget }}</td>
                        <td>{{ ($leftFinishing->height == "Não" ? $leftFinishing->height : $leftFinishing->height . "mm") }}</td>
                        <td>{{ ($leftFinishing->width == "Não" ? $leftFinishing->width : $leftFinishing->width . "mm") }}</td>
                        <td>{{ ($leftFinishing->depth == "Não" ? $leftFinishing->depth : $leftFinishing->depth . "mm") }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <br>

        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="80">Ambiente</th>
                    <th width="125">Altura (Acab. Dir.)</th>
                    <th width="130">Largura (Acab. Dir.)</th>
                    <th width="150">Profundidade (Acab. Dir.)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rightFinishings as $rightFinishing)
                    <tr>
                        <td>{{ $rightFinishing->target }}</td>
                        <td>{{ $rightFinishing->envTarget }}</td>
                        <td>{{ ($rightFinishing->height == "Não" ? $rightFinishing->height : $rightFinishing->height . "mm") }}</td>
                        <td>{{ ($rightFinishing->width == "Não" ? $rightFinishing->width : $rightFinishing->width . "mm") }}</td>
                        <td>{{ ($rightFinishing->depth == "Não" ? $rightFinishing->depth : $rightFinishing->depth . "mm") }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <br>

        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="80">Ambiente</th>
                    <th width="125">Espessura Interna</th>
                    <th width="130">Espessura Externa</th>
                    <th width="150">Espessura Prateleira</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($thicknessData as $thickness)
                    <tr>
                        <td>{{ $thickness->target }}</td>
                        <td>{{ $thickness->envTarget }}</td>
                        <td>{{ $thickness->internal }}mm</td>
                        <td>{{ $thickness->external }}mm</td>
                        <td>{{ $thickness->shelves }}mm</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <h3>Aparência do Módulo</h3>
        
        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="90">Ambiente</th>
                    <th width="200">Cor Interna (Módulo)</th>
                    <th width="200">Cor Externa (Módulo)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($moduleColors as $color)
                    <tr>
                        <td>{{ $color->target }}</td>
                        <td>{{ $color->envTarget }}</td>
                        <td>{{ $color->internalName }} {{ $color->internalCode }}</td>
                        <td>{{ $color->externalName }} {{ $color->externalCode }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <br>

        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="90">Ambiente</th>
                    <th width="200">Cor Interna (Fita)</th>
                    <th width="200">Cor Externa (Fita)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tapeColors as $color)
                    <tr>
                        <td>{{ $color->target }}</td>
                        <td>{{ $color->envTarget }}</td>
                        <td>{{ $color->internalName }} {{ $color->internalCode }}</td>
                        <td>{{ $color->externalName }} {{ $color->externalCode }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <br>

        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="90">Ambiente</th>
                    <th width="140">Puxador (Portas)</th>
                    <th width="160">Cor do Puxador</th>
                    <th width="90">Código</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($doorsPullers as $doorPuller)
                    <tr>
                        <td>{{ $doorPuller->target }}</td>
                        <td>{{ $doorPuller->envTarget }}</td>
                        <td>{{ $doorPuller->name }}</td>
                        <td>{{ $doorPuller->colorName }}</td>
                        <td>{{ $doorPuller->colorCode }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        
        <br>
        
        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="90">Ambiente</th>
                    <th width="140">Puxador (Gavetas)</th>
                    <th width="160">Cor do Puxador</th>
                    <th width="90">Código</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($drawersPullers as $drawerPuller)
                    <tr>
                        <td>{{ $drawerPuller->target }}</td>
                        <td>{{ $drawerPuller->envTarget }}</td>
                        <td>{{ $drawerPuller->name }}</td>
                        <td>{{ $drawerPuller->colorName }}</td>
                        <td>{{ $drawerPuller->colorCode }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <br>

        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th width="90">Ambiente</th>
                    <th width="200">Suporte das Prateleiras</th>
                    <th width="200">Corrediças</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($extras as $extra)
                    <tr>
                        <td>{{ $extra->target }}</td>
                        <td>{{ $extra->envTarget }}</td>
                        <td>{{ $extra->support }}</td>
                        <td>{{ $extra->slider }}</td>
                    </tr>
                @endforeach
            </tbody>
        <table>
    </div>
</body>