<body bgcolor="#e6e6e6" style="margin:0; padding:0; font-family: Arial, sans-serif; color: #4F4F4F; max-width: 700px; font-size: 1rem">
    <div style="margin: 2rem; padding: 2rem; border: 0; border-radius: .6rem; background-color: #ffffff">
        <h1>Novo Orçamento realizado</h1>
        <h4 style="font-weight: 400">Um novo orçamento foi criado. Abaixo você pode consultar os dados do mesmo, assim como o contato do cliente.</h4>

        <h3>Dados do cliente:</h3>
        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="150">Nome</th>
                    <th width="400">Email</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $clientData->user }}</td>
                    <td>{{ $clientData->email }}</td>
                </tr>
            </tbody>
        </table>

        <br>

        <h3>Dados das ferragens:</h3>
        <table border="1" style="border-collapse: collapse; text-align: left; font-size: .9rem" cellpadding="5">
            <thead>
                <tr>
                    <th width="450">Nome</th>
                    <th width="100">Quantidade</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $name }}</td>
                    <td>{{ $quantity }}</td>
                </tr>
            </tbody>
        </table>

        @if (!is_null($description))
            <br>
            <h3>Descrição:</h3>
            <p>{{ $description }}</p>
        @endif
    </div>
</body>