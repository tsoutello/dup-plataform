@extends('layouts.budgets.app')

@section('title', 'Adicionar Cliente')
@section('navbar-title', 'Novo Orçamento')

@section('content')
    <!-- page_content_wrapper_start -->
    <div class="container">
        <!-- page_top_content_wrapper_start -->
        <div class="row">
            <div class="col-12">
                <!-- header_text_start -->
                <h1 class="poppins font-regular font-extra-large">
                    <!-- plus_icon -->
                    <img src="{{ asset('assets/icons/plus.png') }}" alt="novo-orcamento" width="24" height="24">

                    <!-- header_message -->
                    Novo Orçamento
                </h1>
                <!-- header_text_end -->
            </div>
        </div>
        <!-- page_top_content_wrapper_end -->

        <!-- page_instructions_start -->
        <div class="row mt-4">
            <div class="col-12 ">
                <!-- instructions_title_start -->
                <p class="poppins m-0">
                    Adicione os Dados do Cliente:
                </p>
                <!-- instructions_text_end -->

                <!-- instructions_subtitle_start -->
                <p class="poppins font-small text-primary-gray pt-1">
                    Preencha as informações para Gerar o Orçamento
                </p>
                <!-- instructions_subtitle_end -->
            </div>
        </div>
        <!-- page_instructions_end -->

        <!-- form_wrapper_start -->
        <div class="row mt-4">
            <div class="col-12">
                <!-- form_start -->
                <form method="POST" action="{{ route('new-budget-client') }}">
                    @csrf

                    <!-- client_name_field_wrapper_start -->
                    <div class="form-group row">
                        <!-- field_label -->
                        <label for="name" class="col-12 col-form-label text-md-right">Nome do Cliente:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-12 d-flex">
                            <!-- right_icon_image -->
                            <img src="{{ asset('assets/icons/client.png') }}" alt="cliente" class="img-fluid form-control-left-icon password-toggler">

                            <!-- input -->
                            <input id="name" type="text" class="form-control with-icon both @error('name') is-invalid @enderror" 
                                name="name" placeholder="Selecione ou Adicione um Novo" required
                                @if(Session::has('clientData')) value="{{ Session::get('clientData')[0] }}" @endif>
                            
                            <!-- right_icon_image -->
                            <img src="{{ asset('assets/icons/down-arrow.png') }}" alt="clientes" class="img-fluid form-control-icon password-toggler" id="select-client">

                            @error('password')
                                <!-- input_error_message_start -->
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <!-- input_error_message_end -->
                            @enderror
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- client_name_field_wrapper_end -->

                    <!-- email_field_wrapper_start -->
                    <div class="form-group row mt-3">
                        <!-- field_label -->
                        <label for="email" class="col-12 col-form-label text-md-right">E-mail:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-12 d-flex">
                            <!-- right_icon_image -->
                            <img src="{{ asset('assets/icons/mail.png') }}" alt="email" class="img-fluid form-control-left-icon password-toggler">

                            <!-- input -->
                            <input id="email" type="email" class="form-control with-icon left @error('email') is-invalid @enderror" 
                                name="email" placeholder="Digite o E-mail" required
                                @if(Session::has('clientData')) value="{{ Session::get('clientData')[1] }}" @endif>

                            @error('email')
                                <!-- input_error_message_start -->
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <!-- input_error_message_end -->
                            @enderror
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- email_field_wrapper_end -->

                    <!-- phone_field_wrapper_start -->
                    <div class="form-group row mt-3">
                        <!-- field_label -->
                        <label for="phone" class="col-12 col-form-label text-md-right">Telefone: <span>Opcional*</span></label>

                        <!-- input_wrapper_start -->
                        <div class="col-12 d-flex">
                            <!-- right_icon_image -->
                            <img src="{{ asset('assets/icons/phone-alt.png') }}" alt="telefone" class="img-fluid form-control-left-icon password-toggler">

                            <!-- input -->
                            <input id="phone" type="text" class="form-control with-icon left @error('phone') is-invalid @enderror" 
                                name="phone" placeholder="Digite o Telefone"
                                @if(Session::has('clientData')) value="{{ Session::get('clientData')[2] }}" @endif>

                            @error('phone')
                                <!-- input_error_message_start -->
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <!-- input_error_message_end -->
                            @enderror
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- phone_field_wrapper_end -->

                    <!-- address_field_wrapper_start -->
                    <div class="form-group row mt-3">
                        <!-- field_label -->
                        <label for="address" class="col-12 col-form-label text-md-right">Endereço: <span>Opcional*</span></label>

                        <!-- input_wrapper_start -->
                        <div class="col-12 d-flex">
                            <!-- input -->
                            <textarea id="address" type="text" class="form-control @error('address') is-invalid @enderror" 
                                name="address" placeholder="Digite o Endereço"
                                @if(Session::has('clientData')) value="{{ Session::get('clientData')[3] }}" @endif>
                            </textarea>

                            @error('address')
                                <!-- input_error_message_start -->
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                <!-- input_error_message_end -->
                            @enderror
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- address_field_wrapper_end -->

                    <!-- form_submit_wrapper_start -->
                    <div class="form-group row mt-5">
                        <!-- button_wrapper_start -->
                        <div class="col-md-12 text-right">
                            <!-- submit_button -->
                            <button type="submit" class="btn btn-primary">Continuar <img src="{{ asset('assets/icons/right-arrow.png') }}" alt="" class="pl-2"></button>
                        </div>
                        <!-- button_wrapper_end -->
                    </div>
                    <!-- form_submit_wrapper_end -->
                </form>
                <!-- form_end -->
            </div>
        </div>
        <!-- form_wrapper_end -->
    </div>
    <!-- page_content_wrapper_end -->

    <!-- user_clients_modal_start -->
    <div class="modal fade big-modal" id="clients-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!-- modal_content_wrapper_start -->
            <div class="modal-content">
                <!-- modal_header_start -->
                <div class="modal-header">
                    <h5 class="modal-title">Seus clientes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- modal_header_end -->

                <!-- modal_content_start -->
                <div class="modal-body">
                    <!-- clients_table_start -->
                    <table id="available-clients-table" class="table">
                        <!-- table_columns_top_start -->
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Ação</th>
                            </tr>
                        </thead>
                        <!-- table_columns_top_end -->

                        <!-- table_rows_start -->
                        <tbody></tbody>
                        <!-- table_rows_end -->

                        <!-- table_columns_bottom_start -->
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Ação</th>
                            </tr>
                        </thead>
                        <!-- table_columns_bottom_end -->
                    </table>
                    <!-- clients_table_end -->
                </div>
                <!-- modal_content_end -->
            </div>
            <!-- modal_content_wrapper_end -->
        </div>
    </div>
    <!-- user_clients_modal_end -->
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(() => {

            /**
             * Add a event listener to handle clicks on Select button, in clietns table.
             * 
             * @return {void}
             */
            function addClientSelectButtonEventListener() {
                $(".select-client-button").on("click", function () {
                    // Get the client data
                    let $clientName = $($(this).closest('tr').find("td")[0]).text();
                    let $clientEmail = $($(this).closest('tr').find("td")[1]).text();

                    // Fill the inputs with the client data
                    $("#name").val($clientName);
                    $("#email").val($clientEmail);

                    // Open the clients modal
                    $("#clients-modal").modal('toggle');
                });
            }

            /**
             * Handle select-client button clicks.
             *
             * @return {void}
             */
            $("#select-client").click(() => {
                // Stores the target modal
                let modal = $("#select-client-modal");

                // Ajax request to search by the clients
                $.ajax({
                    url: "{{ route('search-clients') }}",
                    type: "POST",
                    data: {
                        _token: $("input[name='_token']").val()
                    },

                    success: ($response) => {
                        // Parse JSON response
                        let $data = JSON.parse($response);

                        // Fill clients modal table with $data content
                        table("#available-clients-table", $data, {"select-client-button" : ["primary", "Selecionar"]});

                        // Open the clients modal
                        $("#clients-modal").modal('toggle');

                        // Add event listener to the select button in clients table
                        addClientSelectButtonEventListener();
                    }
                });
            });
        });
    </script>
@endpush