@extends('layouts.budgets.app')

@section('title', 'Selecionar Ambiente')
@section('navbar-title', 'Novo Orçamento')

@push('css')
    <style type="text/css">
        /* Environment wrappers */
        .environment {
            margin: .25rem 0.0rem;
            background: #fff;
            border: 1.5px solid #E7E7E7;
            box-sizing: border-box;
            box-shadow: 0px 3px 12px rgba(0, 0, 0, 0.1);
            border-radius: 4px;
        }

        /* Disabled Environment wrappers */
        .environment.disabled {
            border: 0;
            box-shadow: none;
            background: transparent;
        }

        /* Environments checkbox wrappers */
        .environment .col-sm-2 {
            padding: 1rem 1rem;
                padding-bottom: 0;
                padding-right: 0;
        }

        .environment .col-sm-2 input {
            height: 1rem;
            width: 1rem;
        }

        /* Environments Titles */
        .environment .environment-title h2 {
            display: flex;
            flex-direction: column;
            justify-content: center;
            height: 100%;
            font-family: "Poppins", sans-serif;
            font-weight: 400;
            font-size: 1rem;
            text-align: center;
            text-transform: uppercase;
        }

        /* Disabled Environments In-dev message */
        .environment .environment-title span {
            position: relative;
            right: 50%;
            top: 2rem;
            white-space: nowrap;
            color: #6F90B8;
            font-family: "Poppins", sans-serif;
            font-weight: 600;
            font-style: italic;
            font-size: .938rem;
            text-transform: uppercase;
        }

        .environment:not(.disabled) .environment-title span {
            display: none;
        }

        /* Disabled Environments Titles */
        .environment.disabled .environment-title h2 {
            color: var(--primary-gray);
        }

        /* Environments Image wrappers */
        .environment .environment-image{
            margin-left: auto !important;
            width: 42%;
            text-align: right;
        }

        /* Environments Images */
        .environment .environment-image img{
            margin: .5rem 0;
            border: 1px solid #E7E7E7;
            border-radius: 4px;
        }

        /* Disabled Environments Images */
        .environment.disabled .environment-image img {
            -webkit-filter: grayscale(50%);
            filter: grayscale(50%);
        }

        /* Other budget item */
        .other-budget {
            border-radius: 4px;
            box-shadow: 0px 3px 12px rgba(0, 0, 0, 0.1);
            background: #ffffff;
        }

        /* Other budget image */
        .other-budget img {
            margin: .5rem 0;
            border: 1px solid #D7D7D7;
            border-radius: 2px;
        }

        /* Environment send */
        .environment-send {
            margin: .25rem .28rem;
                margin-bottom: 1rem;
            border: 1px solid #E7E7E7;
            border-radius: 4px;
            background: #E7E7E7;
        }

        /* Environment name input */
        .environment-send input {
            height: 2rem;
            border-color: #2775D7 !important;
        }

        /* Environment name field icon */
        .environment-send img {
            border-color: #2775D7 !important;
        }

        /* Environemnt wrapper when checked */
        .environment.checked {
            border: 1.5px #2775D7 solid !important;
        }
    </style>
@endpush

@section('content')
    <!-- content_wrapper_start -->
    <div class="container">
        <!-- page_form_start -->
        <form action="{{ route('new-budget-initial') }}" method="POST">
            @csrf

            <!-- hidden_control_fields -->
            <input type="hidden" name="environment-target" value="{{ $environmentTarget }}">

            <!-- page_top_start -->
            <div class="row">
                <div class="col-12">
                    <!-- page_instruction_text -->
                    <h1 class="poppins font-size-default font-medium">Qual o Ambiente de Montagem?</h1>

                    <!-- page_instruction_sub-title_text -->
                    <p class="poppins font-small font-medium text-primary-gray">Selecione o Ambiente de Montagem do módulo</p>
                </div>
            </div>
            <!-- page_top_end -->

            <!-- ambients_start -->
            <div class="row mt-3">
                <div class="col-12 p-1 px-2">
                    @foreach ($environments as $environment)
                        <!-- environment_wrapper_start -->
                        <div class="row environment {{ $environment->available ? '' : 'disabled' }}" id="environment-{{ $environment->id }}">
                            <!-- environment_checkbox_wrapper_start -->
                            <div class="col-sm-2">
                                @if ($environment->available)
                                    <!-- ambient_checkbox -->
                                    <input type="checkbox" name="ambient-id" value="{{ $environment->id }}">
                                @endif
                            </div>
                            <!-- environment_checkbox_wrapper_end -->

                            <!-- environment_name_wrapper_start -->
                            <div class="col-sm-3 environment-title">
                                <!-- environment_name_text -->
                                <h2>{{ $environment->ambient_name }} <span>Em desenvolvimento</span></h2>
                            </div>
                            <!-- environment_name_wrapper_end -->

                            <!-- environment-image_wrapper_start -->
                            <div class="col-sm-6 environment-image">
                                <!-- environment_image -->
                                <img src="{{ asset($environment->image) }}" alt="environment-image">
                            </div>
                            <!-- environment-image_wrapper_end -->
                        </div>
                        <!-- environment_wrapper_end -->

                        <!-- environment_send_wrapper_start -->
                        <div class="row py-3 px-3  environment-send d-none" id="send-{{ $environment->id }}">
                            <!-- environment_name_field_label -->
                            <label for="environment-name-{{ $environment->id }}" class="pl-4 col-form-label">Nome do Ambiente:</label>

                            <!-- environment_name_wrapper_start -->
                            <div class="col-12 d-flex">
                                <!-- input -->
                                <input type="text" id="environment-name-{{ $environment->id }}" name="ambient-alias" class="form-control reverse-border with-icon" required value="{{ $environment->ambient_name }}">

                                <!-- input_icon -->
                                <img src="{{ asset('assets/icons/edit.png') }}" class="form-control-icon reverse-border">
                            </div>
                            <!-- environment_name_wrapper_end -->

                            <!-- send_button -->
                            <button class="btn btn-primary mt-3 ml-auto send-button" type="button" data-environment-id="{{ $environment->id }}">Continuar</button>
                        </div>
                        <!-- environment_send_wrapper_end -->
                    @endforeach
                </div>
            </div>
            <!-- ambients_end -->

            <!-- others_budgets_text_wrapper_start -->
            <div class="row mt-2 pt-3 px-2 border-top">
                <!-- row_title_start -->
                <div class="col-12">
                    <!-- others_budgets_text -->
                    <span class="poppins font-size-default font-medium">Outros Tipos de Orçamento:</span>
                </div>
                <!-- row_title_end -->

                <!-- row_subtitle_start -->
                <div class="col-12">
                    <!-- others_budgets_subtitle_text -->
                    <span class="poppins font-small text-primary-gray font-medium">Outras alternativas em casos de serviços avulsos</span>
                </div>
                <!-- row_subtitle_end -->
            </div>
            <!-- others_budgets_text_wrapper_end -->

            <!-- others_budgets_items_wrapper_start -->
            <div class="row mt-5">
                <!-- item_start -->
                <div class="row mx-2 other-budget" onclick="window.location.href=`{{ route('new-pieces-budgets') }}`">
                    <!-- item_texts_start -->
                    <div class="col-sm-8 d-flex flex-column">
                        <!-- item_title -->
                        <h2 class="poppins font-size-default font-medium text-primary-gray pl-3 pt-3">Peças Avulsas</h2>

                        <!-- item_sub-title -->
                        <span class="poppins font-small font-medium text-primary-gray pl-3">Base, Lateral, Teto, Fundo, Etc.</span>
                    </div>
                    <!-- item_texts_end -->

                    <!-- item_image_wrapper_start -->
                    <div class="col-sm-3">
                        <!-- item_image -->
                        <img src="{{ asset($individualPieces) }}" alt="pecas-avulsas">
                    </div>
                    <!-- item_image_wrapper_end -->
                </div>
                <!-- item_end -->

                <!-- item_start -->
                <div class="row mx-2 other-budget mt-3" onclick="window.location.href=`{{ route('new-ironwares-budgets') }}`">
                    <!-- item_texts_start -->
                    <div class="col-sm-8 d-flex flex-column">
                        <!-- item_title -->
                        <h2 class="poppins font-size-default font-medium text-primary-gray pl-3 pt-3">Ferragens</h2>

                        <!-- item_sub-title -->
                        <span class="poppins font-small font-medium text-primary-gray pl-3">Corrediça, Dobradiça, Puxador, Etc.</span>
                    </div>
                    <!-- item_texts_end -->

                    <!-- item_image_wrapper_start -->
                    <div class="col-sm-3">
                        <!-- item_image -->
                        <img src="{{ asset($ironwares) }}" alt="ferragens">
                    </div>
                    <!-- item_image_wrapper_end -->
                </div>
                <!-- item_end -->
            </div>
            <!-- others_budgets_items_wrapper_end -->
        </form>
    </div>
    <!-- content_wrapper_end -->
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(() => {
            /**
             * Uncheck all checkboxes.
             * 
             * @return {void}
             */
             $("input[type='checkbox']").each(function () {
                 // Uncheck the current element
                 $(this).prop('checked', false);
             });

            /**
             * Handle changes in the selected environment.
             *
             * @return {void}
             */
            $("input[type='checkbox']").on("change", function () {
                // Gets the target environment id and his state
                let targetId    = $(this).val();
                let targetState = $(this).is(':checked');

                // Verifies the target state
                if (targetState === true) {
                    // Set the environment target to checked
                    $(`#environment-${targetId}`).addClass('checked');

                    // Show the corresponding sender
                    $(`#send-${targetId}`).removeClass('d-none');

                    // Loop each checkbox (except the changed)
                    $("input[type='checkbox']").each(function () {
                        // Get the environment id of the current element
                        let currentId = $(this).val();

                        // Verifies if the current element is the clicked element
                        if (currentId == targetId) {
                            // Ends the function
                            return;
                        }

                        // Uncheck the element
                        $(this).prop('checked', false);

                        // Remove checked class from current checkbox target
                        $(`#environment-${currentId}`).removeClass("checked");

                        // Hide the senders to others environments
                        $(`#send-${currentId}`).addClass('d-none');
                    });
                } else {
                    // Loop each checkbox
                    $("input[type='checkbox']").each(function () {
                        // Get the environment id of the current element
                        let currentId = $(this).val();

                        console.log(currentId)

                        // Uncheck the element
                        $(this).prop('checked', false);

                        // Remove checked class from current checkbox target
                        $(`#environment-${currentId}`).removeClass("checked");

                        // Hide the senders to others environments
                        $(`#send-${currentId}`).addClass('d-none');
                    });
                }
            });

            /**
             * Handle clicks in send button.
             *
             * @return {void}
             */
            $(".send-button").click(function (e) {
                // Prevent the click action
                e.preventDefault();

                // Get the environment id
                let targetId = $(this).attr("data-environment-id");

                // Loop all ambient name inputs
                $("input[name='ambient-alias']").each(function () {
                    // Get the current element id
                    let currentId = $(this).attr('id');

                    // Verifies if current element is the selected by user
                    if (currentId == `environment-name-${targetId}`) {
                        // Skips to the next loop executation
                        return;
                    }

                    // Delete current element
                    $(this).remove();
                });

                // Submit the form
                $("form").submit();
            });
        });
    </script>
@endpush