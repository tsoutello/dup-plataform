@extends('layouts.budgets.app')

@section('title', 'Estrutura do Módulo')
@section('navbar-title', 'Novo Orçamento')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/structure.css') }}">
    <link rel="stylesheet" href="{{ asset('css/components/modals.css')}}">
@endpush

@section('content')
    <!-- content_wrapper_start -->
    <div class="container px-3">
        <!-- page_name_wrapper_start -->
        <div class="row">
            <div class="col-12">
                <!-- page_name_text_start -->
                <h1 class="poppins font-extra-large text-uppercase">
                    <!-- text_pre_icon -->
                    <img src="{{ asset('assets/icons/project.png') }}" alt="projeto" width="24" height="24" class="mr-1">

                    <!-- text -->
                    Estrutura
                </h1>
                <!-- page_name_text_end -->
            </div>
        </div>
        <!-- page_name_wrapper_end -->

        <!-- page_form_start -->
        <form action="{{ route('new-budget-structure') }}" method="POST">
            @csrf

            <!-- hidden_control_inputs_start -->
            <input type="hidden" name="environment-target" value="{{ $environmentTarget }}" autocomplete="off">
            <input type="hidden" name="furniture-target" value="{{ $furnitureTarget }}" autocomplete="off">
            <input type="hidden" name="model-id" value="{{ $moduleData->id }}" autocomplete="off">
            <input type="hidden" name="mirror-doors[]" @if(isset($mirrorLeft)) value="1" @endif id="left-mirror-door" autocomplete="off">
            <input type="hidden" name="mirror-doors[]" @if(isset($mirrorMiddle)) value="2" @endif id="center-mirror-door" autocomplete="off">
            <input type="hidden" name="mirror-doors[]" @if(isset($mirrorRight)) value="3" @endif id="right-mirror-door" autocomplete="off">
            <input type="hidden" name="mirror-doors-color" autocomplete="off" 
                @if(Session::has("newBudget.environment.{$environmentTarget}.furnitures.{$furnitureTarget}.data.advancedCustomization")) 
                value='{{ Session::get("newBudget.environment.{$environmentTarget}.furnitures.{$furnitureTarget}.data.advancedCustomization.mirrorName") }}' @endif>
            <input type="hidden" name="mirror-doors-color-code" autocomplete="off"
                @if(Session::has("newBudget.environment.{$environmentTarget}.furnitures.{$furnitureTarget}.data.advancedCustomization")
                    && !is_null(Session::get("newBudget.environment.{$environmentTarget}.furnitures.{$furnitureTarget}.data.advancedCustomization.mirrorColor"))) 
                value='{{ Session::get("newBudget.environment.{$environmentTarget}.furnitures.{$furnitureTarget}.data.advancedCustomization.mirrorColor") }}' @endif>
            <input type="hidden" name="external-thickness" value="{{ $externalThickness }}" autocomplete="off">
            <input type="hidden" name="internal-thickness" value="{{ $internalThickness }}" autocomplete="off">
            <input type="hidden" name="shelves-thickness" value="{{ $shelvesThickness }}" autocomplete="off">
            <!-- hidden_control_inputs_end -->

            <!-- page_title_wrapper_start -->
            <div class="row mt-3">
                <div class="col-12 d-flex flex-column">
                    <!-- page_main_title_text -->
                    <h2 class="poppins font-size-default">Defina a Estrutura do Módulo:</h2>

                    <!-- page_sub_title_text -->
                    <h2 class="poppins font-small text-primary-gray font-medium pb-4 border-bottom">As escolhas determinarão a montagem e o acabamento do módulo.</h2>
                </div>
            </div>
            <!-- page_title_wrapper_end -->

            <!-- doors_selection_start -->
            <div class="row mt-4">
                <!-- selection_title_start -->
                <div class="col-12">
                    <!-- selection_title -->
                    <h2 class="selection-title">Qual o TIPO das PORTAS?</h2>
                </div>
                <!-- selection_title_end -->

                <div class="col-12">
                    @foreach($structures as $structure)
                        <!-- selection_wrapper_start -->
                        <div class="row selection doors {{ $structure->available ? '' : 'disabled' }}">
                            <!-- selection_checkbox_wrapper_start -->
                            <div class="col-sm-1">
                                @if ($structure->available)
                                    <!-- structure_checkbox -->
                                    <input type="checkbox" name="structure-id" value="{{ $structure->id }}" checked="true">
                                @endif
                            </div>
                            <!-- selection_checkbox_wrapper_end -->

                            <!-- selection_name_wrapper_start -->
                            <div class="col-sm-5 selection-title">
                                <!-- structure_name_text -->
                                <h2>{{ $structure->name }} <span>Em desenvolvimento</span></h2>
                            </div>
                            <!-- selection_name_wrapper_end -->

                            <!-- selection-image_wrapper_start -->
                            <div class="col-sm-6 selection-image">
                                <!-- structure_image -->
                                <img src="{{ asset($structure->image) }}" alt="structure-image">
                            </div>
                            <!-- selection-image_wrapper_end -->
                        </div>
                        <!-- selection_wrapper_end -->
                    @endforeach
                </div>
            </div>
            <!-- doors_selection_end -->

            <!-- model_selection_start -->
            <div class="row mt-2">
                <!-- selection_title_start -->
                <div class="col-12 pt-2">
                    <!-- selection_title -->
                    <h2 class="selection-title border-top pt-4 pb-2">Escolha o MODELO do Módulo:</h2>
                </div>
                <!-- selection_title_end -->

                <!-- model_info_start -->
                <div class="col-12 poppins">
                    <div class="row white-box select-model">
                        <!-- model_data_wrapper_start -->
                        <div class="col-sm-6 text-center d-flex flex-column justify-content-between h-100 py-3">
                            <!-- model_fixed_info_start -->
                            <div>
                                <!-- doors_amount_start -->
                                <h2 class="font-medium font-size-default text-uppercase text-primary-gray" id="model-doors">
                                    {{ $moduleData->doors }} portas
                                </h2>
                                <!-- doors_amount_end -->
                                
                                <!-- model_name_start -->
                                <h3 class="font-medium font-small text-uppercase text-secondary-gray" id="model-name">
                                    {{ $moduleData->name }}
                                </h3>
                                <!-- model_name_end -->
                            </div>
                            <!-- model_fixed_info_end -->

                            <!-- change_model_link -->
                            <a href="#" class="change-model-link">Clique para alterar</a>
                        </div>
                        <!-- model_data_wrapper_end -->

                        <!-- model_image_wrapper_start -->
                        <div class="col-sm-3 ml-3 pt-2">
                            <!-- model_image -->
                            <img src="{{ asset($moduleData->image) }}" alt="{{ $moduleData->name }}" id="model-image">
                        </div>
                        <!-- model_image_wrapper_end -->
                    </div>
                </div>
                <!-- model_info_end -->
            </div>
            <!-- model_selection_end -->

            <!-- finishing_selection_wrapper_start -->
            <div class="row mt-2">
                <!-- selection_title_start -->
                <div class="col-12 pt-2">
                    <!-- selection_title -->
                    <h2 class="selection-title border-top pt-4 pb-0">O Módulo se ENCOSTA em qual parede?</h2>

                    <!-- selection_sub_title_start -->
                    <p class="selection-sub-title">
                        Selecione a parede em que o módulo se encosta. Um Acabamento é necessário se houver paredes.
                    </p>
                    <!-- selection_sub_title_end -->
                </div>
                <!-- selection_title_end -->
            </div>
            <!-- finishing_selection_wrapper_end -->

            <!-- fishing_options_wrapper_start -->
            <div class="row">
                <div class="col-12 poppins">
                    <div class="d-flex flex-column white-box py-1 pl-3">
                        <!-- no_finishing_wrapper_start -->
                        <div class="row mt-2">
                            <!-- radio_wrapper_start -->
                            <div class="col-1">
                                <!-- radio -->
                                <input type="radio" name="finishing" id="no-finishing" value="no-finishing" autocomplete="off" {{ isset($noFinishingChecked) ? 'checked' : '' }}>
                            </div>
                            <!-- radio_wrapper_start -->

                            <!-- label_wrapper_start -->
                            <div class="col-9">
                                <!-- radio_label -->
                                <label for="no-finishing" class="finishing-label">SEM Parede dos lados</label>
                            </div>
                            <!-- label_wrapper_end -->
                        </div>
                        <!-- no_finishing_wrapper_end -->

                        <!-- right_finishing_wrapper_start -->
                        <div class="row mt-2">
                            <!-- radio_wrapper_start -->
                            <div class="col-1">
                                <!-- radio -->
                                <input type="radio" name="finishing" id="right-finishing" value="right-finishing" autocomplete="off" {{ isset($rightFinishingChecked) ? 'checked' : '' }}>
                            </div>
                            <!-- radio_wrapper_start -->

                            <!-- label_wrapper_start -->
                            <div class="col-9">
                                <!-- radio_label -->
                                <label for="right-finishing" class="finishing-label">Parede do Lado DIREITO</label>
                            </div>
                            <!-- label_wrapper_end -->
                        </div>
                        <!-- right_finishing_wrapper_end -->

                        <!-- left_finishing_wrapper_start -->
                        <div class="row mt-2">
                            <!-- radio_wrapper_start -->
                            <div class="col-1">
                                <!-- radio -->
                                <input type="radio" name="finishing" id="left-finishing" value="left-finishing" autocomplete="off" {{ isset($leftFinishingChecked) ? 'checked' : '' }}>
                            </div>
                            <!-- radio_wrapper_start -->

                            <!-- label_wrapper_start -->
                            <div class="col-9">
                                <!-- radio_label -->
                                <label for="left-finishing" class="finishing-label">Parede do Lado ESQUERDO</label>
                            </div>
                            <!-- label_wrapper_end -->
                        </div>
                        <!-- left_finishing_wrapper_end -->

                        <!-- both_finishing_wrapper_start -->
                        <div class="row mt-2">
                            <!-- radio_wrapper_start -->
                            <div class="col-1">
                                <!-- radio -->
                                <input type="radio" name="finishing" id="both-finishing" value="both-finishing" autocomplete="off" {{ isset($bothFinishingChecked) ? 'checked' : '' }}>
                            </div>
                            <!-- radio_wrapper_start -->

                            <!-- label_wrapper_start -->
                            <div class="col-9">
                                <!-- radio_label -->
                                <label for="both-finishing" class="finishing-label">Parede do Lado ESQUERDO e DIREITO</label>
                            </div>
                            <!-- label_wrapper_end -->
                        </div>
                        <!-- both_finishing_wrapper_end -->
                    </div>
                </div>
                <!-- fishing_options_wrapper_end -->
            </div>
            <!-- finishing_configuration_end -->

            <!-- module_sizes_selection_wrapper_start -->
            <div class="row mt-2">
                <!-- selection_title_start -->
                <div class="col-12 pt-2">
                    <!-- selection_title -->
                    <h2 class="selection-title border-top pt-4 pb-0">Confirme as DIMENSÕES do Módulo:</h2>
                </div>
                <!-- selection_title_end -->
            </div>
            <!-- module_sizes_selection_wrapper_end -->

            <!-- module_sizes_wrapper_start -->
            <div class="row">
                <div class="col-12 poppins p-2">
                    <div class="d-flex flex-column white-box py-3 pl-3">
                        <!-- box_title_wrapper_start -->
                        <div class="row">
                            <div class="col-12">
                                <!-- box_title -->
                                <h3 class="font-medium font-size-default">Medidas do Guarda-Roupas: <span class="text-secondary-gray">(mm)</h3>
                            </div>
                        </div>
                        <!-- box_title_wrapper_end -->

                        <!-- module_sizes_start -->
                        <div class="row pl-3 mt-3" style="height: 300px" id="module-sizes-overview">
                            <!-- left_finishing_start -->
                            <div class="left-finishing-view">
                                <!-- finishing_viewer -->
                                <div class="viewer">⠀⠀⠀</div>
                                
                                <!-- finishing_measure_start -->
                                <div class="measure">
                                    <!-- measeure_value -->
                                    <span id="left-finishing-view-measure">{{ $finishingWidth }}</span>
                                </div>
                                <!-- finishing_measure_end -->
                            </div>
                            <!-- left_finishing_end -->

                            <!-- module_width_start -->
                            <div class="module-size-view">
                                <!-- module_image_wrapper -->
                                <img src="{{ asset($moduleData->image) }}" alt="{{ $moduleData->name }}">

                                <!-- module_measures_start -->
                                <div class="measure">
                                    <!-- module_name -->
                                    <span id="module-width-measure" data-width-measure="{{ floatval($moduleData->moduleWidth) * 1000 }}">{{ $furnitureName }}</span>
                                </div>
                                <!-- module_measures_end -->
                            </div>
                            <!-- module_width_end -->

                            <!-- right_finishing_start -->
                            <div class="right-finishing-view">
                                <!-- finishing_viewer -->
                                <div class="viewer">⠀⠀⠀</div>
                                
                                <!-- finishing_measure_start -->
                                <div class="measure">
                                    <!-- measeure_value -->
                                    <span id="right-finishing-view-measure">{{ $finishingWidth }}</span>
                                </div>
                                <!-- finishing_measure_end -->
                            </div>
                            <!-- right_finishing_end -->

                            <!-- module_height_start -->
                            <div class="module-height-view">
                                <!-- module_height_viewer -->
                                <span id="module-height-viewer">{{ floatval($moduleData->moduleHeight) * 1000 }} CHÃO AO TETO</span>
                            </div>
                            <!-- module_height_end -->
                        </div>
                        <!-- module_sizes_end -->

                        <!-- module_total_width_start -->
                        <div class="row pl-3 poppins" id="module-total-width">
                            <div class="w-100">
                                <!-- width_viewer_start -->
                                <span class="module-total-width-viewer">
                                    Largura Total:

                                    <!-- module_width -->
                                    <span id="module-total-width-value">{{ (floatval($moduleData->moduleWidth) * 1000) + ($finishingWidth * 2) }}</span>
                                </span>
                                <!-- width_viewer_end -->
                            </div>
                        </div>
                        <!-- module_total_width_end -->
                    </div>
                    <!-- module_total_width_end -->
                </div>
            </div>
            <!-- module_sizes_wrapper_end -->

            <!-- module_sizes_controls_wrapper_start -->
            <div class="row size-controls mt-4">
                <!-- size_control_box_start -->
                <div class="size-box blue-box">
                    <!-- box_top_start -->
                    <div class="box-top">
                        <!-- box_title -->
                        <h3>Dimensão do Guarda-Roupas:</h3>

                        <!-- module_image -->
                        <img src="{{ asset($moduleData->image) }}" alt="module">
                    </div>
                    <!-- box_top_end -->

                    <!-- box_body_start -->
                    <div class="box-body">
                        <!-- height_control_wrapper_start -->
                        <div class="form-group row">
                            <div class="d-flex col-12">
                                <!-- input_icon_wrapper_start -->
                                <div>
                                    <img src="{{ asset('assets/icons/height.png') }}" alt="module-height" class="form-control-icon form-control-left-icon reverse-border">
                                </div>
                                <!-- input_icon_wrapper_end -->

                                <!-- input -->
                                <input type="text" class="form-control with-icon left reverse-border" name="module-height" value="{{ floatval($moduleData->moduleHeight) * 1000 }}" autocomplete="off" required>
                            </div>
                        </div>
                        <!-- height_control_wrapper_end -->

                        <!-- width_control_wrapper_start -->
                        <div class="form-group row">
                            <div class="d-flex col-12">
                                <!-- input_icon_wrapper_start -->
                                <div>
                                    <img src="{{ asset('assets/icons/width.png') }}" style="margin-top: 3px;" alt="module-width" class="form-control-icon form-control-left-icon reverse-border">
                                </div>
                                <!-- input_icon_wrapper_end -->

                                <!-- input -->
                                <input type="text" class="form-control with-icon left reverse-border" name="module-width" value="{{ floatval($moduleData->moduleWidth) * 1000 }}" autocomplete="off" required>
                            </div>
                        </div>
                        <!-- width_control_wrapper_end -->

                        <!-- depth_control_wrapper_start -->
                        <div class="form-group row">
                            <div class="d-flex col-12">
                                <!-- input_icon_wrapper_start -->
                                <div>
                                    <img src="{{ asset('assets/icons/depth.png') }}" style="margin-top: 6px;" alt="module-depth" class="form-control-icon form-control-left-icon reverse-border">
                                </div>
                                <!-- input_icon_wrapper_end -->

                                <!-- input -->
                                <input type="text" class="form-control with-icon left reverse-border" name="module-depth" value="{{ floatval($moduleData->moduleDepth) * 1000 }}" autocomplete="off" required>
                            </div>
                        </div>
                        <!-- depth_control_wrapper_start -->
                    </div>
                    <!-- box_body_end -->

                    <!-- box_footer_start -->
                    <div class="box-footer">
                        <!-- footer_text -->
                        <span>Clique no campo p/ Alterar a medida.</span>
                    </div>
                    <!-- box_footer_end -->
                </div>
                <!-- size_control_box_end -->

                <!-- size_control_box_start -->
                <div class="size-box green-box" id="left-finishing-control">
                    <!-- box_top_start -->
                    <div class="box-top">
                        <!-- box_title -->
                        <h3 class="w-100">Dimensão Acabamento ESQUERDO</h3>
                    </div>
                    <!-- box_top_end -->

                    <!-- box_body_start -->
                    <div class="box-body">
                        <!-- height_control_wrapper_start -->
                        <div class="form-group row">
                            <div class="d-flex col-12">
                                <!-- input_icon_wrapper_start -->
                                <div>
                                    <img src="{{ asset('assets/icons/height.png') }}" alt="module-height" class="form-control-icon form-control-left-icon reverse-border">
                                </div>
                                <!-- input_icon_wrapper_end -->

                                <!-- input -->
                                <input type="text" class="form-control with-icon left reverse-border" name="left-finishing-height" value="{{ floatval($moduleData->moduleHeight) * 1000 }}" autocomplete="off" required>
                            </div>
                        </div>
                        <!-- height_control_wrapper_end -->

                        <!-- width_control_wrapper_start -->
                        <div class="form-group row">
                            <div class="d-flex col-12">
                                <!-- input_icon_wrapper_start -->
                                <div>
                                    <img src="{{ asset('assets/icons/width.png') }}" style="margin-top: 3px;" alt="module-width" class="form-control-icon form-control-left-icon reverse-border">
                                </div>
                                <!-- input_icon_wrapper_end -->

                                <!-- input -->
                                <input type="text" class="form-control with-icon left reverse-border" name="left-finishing-width" value="{{ $finishingWidth }}" autocomplete="off" required>
                            </div>
                        </div>
                        <!-- width_control_wrapper_end -->

                        <!-- depth_control_wrapper_start -->
                        <div class="form-group row">
                            <div class="d-flex col-12">
                                <!-- input_icon_wrapper_start -->
                                <div>
                                    <img src="{{ asset('assets/icons/depth.png') }}" style="margin-top: 6px;" alt="module-depth" class="form-control-icon form-control-left-icon reverse-border">
                                </div>
                                <!-- input_icon_wrapper_end -->

                                <!-- input -->
                                <input type="text" class="form-control with-icon left reverse-border" name="left-finishing-depth" value="{{ floatval($moduleData->moduleDepth) * 1000 }}" autocomplete="off" required>
                            </div>
                        </div>
                        <!-- depth_control_wrapper_start -->
                    </div>
                    <!-- box_body_end -->

                    <!-- box_footer_start -->
                    <div class="box-footer">
                        <!-- footer_text -->
                        <span>Clique no campo p/ Alterar a medida.</span>
                    </div>
                    <!-- box_footer_end -->
                </div>
                <!-- size_control_box_end -->

                <!-- size_control_box_start -->
                <div class="size-box pink-box" id="right-finishing-control">
                    <!-- box_top_start -->
                    <div class="box-top">
                        <!-- box_title -->
                        <h3 class="w-100">Dimensão Acabamento DIREITO</h3>
                    </div>
                    <!-- box_top_end -->

                    <!-- box_body_start -->
                    <div class="box-body">
                        <!-- height_control_wrapper_start -->
                        <div class="form-group row">
                            <div class="d-flex col-12">
                                <!-- input_icon_wrapper_start -->
                                <div>
                                    <img src="{{ asset('assets/icons/height.png') }}" alt="module-height" class="form-control-icon form-control-left-icon reverse-border">
                                </div>
                                <!-- input_icon_wrapper_end -->

                                <!-- input -->
                                <input type="text" class="form-control with-icon left reverse-border" name="right-finishing-height" value="{{ floatval($moduleData->moduleHeight) * 1000 }}" autocomplete="off" required>
                            </div>
                        </div>
                        <!-- height_control_wrapper_end -->

                        <!-- width_control_wrapper_start -->
                        <div class="form-group row">
                            <div class="d-flex col-12">
                                <!-- input_icon_wrapper_start -->
                                <div>
                                    <img src="{{ asset('assets/icons/width.png') }}" style="margin-top: 3px;" alt="module-width" class="form-control-icon form-control-left-icon reverse-border">
                                </div>
                                <!-- input_icon_wrapper_end -->

                                <!-- input -->
                                <input type="text" class="form-control with-icon left reverse-border" name="right-finishing-width" value="{{ $finishingWidth }}" autocomplete="off" required>
                            </div>
                        </div>
                        <!-- width_control_wrapper_end -->

                        <!-- depth_control_wrapper_start -->
                        <div class="form-group row">
                            <div class="d-flex col-12">
                                <!-- input_icon_wrapper_start -->
                                <div>
                                    <img src="{{ asset('assets/icons/depth.png') }}" style="margin-top: 6px;" alt="module-depth" class="form-control-icon form-control-left-icon reverse-border">
                                </div>
                                <!-- input_icon_wrapper_end -->

                                <!-- input -->
                                <input type="text" class="form-control with-icon left reverse-border" name="right-finishing-depth" value="{{ floatval($moduleData->moduleDepth) * 1000 }}" autocomplete="off" required>
                            </div>
                        </div>
                        <!-- depth_control_wrapper_start -->
                    </div>
                    <!-- box_body_end -->

                    <!-- box_footer_start -->
                    <div class="box-footer">
                        <!-- footer_text -->
                        <span>Clique no campo p/ Alterar a medida.</span>
                    </div>
                    <!-- box_footer_end -->
                </div>
                <!-- size_control_box_end -->
            </div>
            <!-- module_sizes_controls_wrapper_end -->

            <!-- advanced_customization_start -->
            <div class="row white-box mt-4 py-2 cursor-pointer" id="advanced-customization-toggler">
                <!-- item_image_wrapper_start -->
                <div class="col-3">
                    <!-- item_image -->
                    <img src="{{ asset($advancedCustomization) }}" alt="advanced-customization" height="62" width="72" class="border rounded">
                </div>
                <!-- item_image_wrapper_end -->

                <!-- item_title_start -->
                <div class="col-9">
                    <h2 class="poppins font-size-default font-medium text-primary-gray">Customização Avançada</h2>
                    <span class="poppins font-small font-medium text-secondary-gray">
                        Espessura do MDF, Desnível de Teto, Tamponamento, Lateral Engrossada...
                    </span>
                </div>
                <!-- item_title_end -->
            </div>
            <!-- advanced_customization_end -->

            <!-- coupled_suitcase_start -->
            <div class="row white-box mt-4 py-2 cursor-pointer in-dev-white-box">
                <!-- item_image_wrapper_start -->
                <div class="col-3">
                    <!-- item_image -->
                    <img src="{{ asset($coupledSuitcase) }}" alt="coupled-suitcase" height="62" width="72" class="border rounded">
                </div>
                <!-- item_image_wrapper_end -->

                <!-- item_title_start -->
                <div class="col-9">
                    <h2 class="poppins font-size-default font-medium text-primary-gray">Maleiro Acoplado</h2>
                    <span class="individual-in-dev">
                        Em Desenvolvimento
                    </span>
                </div>
                <!-- item_title_end -->
            </div>
            <!-- coupled_suitcase_end -->

            <!-- send_button_wrapper_start -->
            <div class="row">
                <div class="col-12 d-flex flex-row justify-content-end mt-4">
                    <button type="submit" class="btn btn-primary">Continuar</button>
                </div>
            </div>
            <!-- send_button_wrapper_end -->
        </form>
        <!-- page_form_end -->
    </div>
    <!-- content_wrapper_end -->

    <!-- module_models_modal_start -->
    <div class="modal modal-fullscreen fade" tabindex="-1" role="dialog" id="module-models-modal">
        <div class="modal-dialog" role="document">
            <!-- modal_content_wrapper_start -->
            <div class="modal-content">
                <!-- modal_header_start -->
                <div class="modal-header">
                    <!-- modal_exit_button_start -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{ asset('assets/icons/white-left-arrow.png') }}" width="24" height="24">
                    </button>
                    <!-- modal_exit_button_end -->

                    <!-- modal_title_text -->
                    <h5 class="modal-title">Escolha o Modelo</h5>

                    <!-- three_dots_wrapper_start -->
                    <div class="dropdown">
                        <!-- three_dots_icon -->
                        <img src="{{ asset('assets/icons/three-dots.png') }}" alt="context-menu" width="10" height="24" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        
                        <!-- three_dots_menu_wrapper_start -->
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="navbar-dropdown">
                            <!-- whatsapp_link_start -->
                            <a class="dropdown-item poppins font-medium text-primary-gray font-small" href="https://api.whatsapp.com/send?phone={{ env('WHATSAPP_CONTACT') }}" target="_blank">
                                <!-- phone_icon -->
                                <img src="{{ asset('assets/icons/phone.png') }}" alt="whatsapp" width="24" height="24" class="mr-2">
                                Entrar em contato
                            </a>
                            <!-- whatsapp_link_end -->
                        </div>
                        <!-- three_dots_menu_wrapper_end -->
                    </div>
                    <!-- three_dots_wrapper_end -->
                </div>
                <!-- modal_header_end -->

                <!-- modal_body_start -->
                <div class="modal-body">
                    <!-- modal_breadcrumb_start -->
                    <div class="modal-breadcrump">
                        <!-- breadcrumb_title -->
                        <h3>Escolha o MODELO do Módulo</h3>
                        
                        <!-- breadcrumb_sub_title -->
                        <span class="poppins font-small font-medium text-secondary-gray pl-3 pb-2 border-bottom w-100">Clique na imagem do modelo para selecionar</span>
                    </div>
                    <!-- modal_breadcrumb_end -->

                    <!-- modal_body_content_wrapper_start -->
                    <div class="container pb-4 mt-2" id="module-models-body">
                    </div>
                    <!-- modal_body_content_wrapper_end -->
                </div>
                <!-- modal_body_end -->
            </div>
            <!-- modal_content_wrapper_end -->
        </div>
      </div>
    <!-- module_models_modal_end -->

    <!-- advanced_customizations_modal_start -->
    <div class="modal modal-fullscreen fade" tabindex="-1" role="dialog" id="advanced-customization-modal">
        <div class="modal-dialog" role="document">
            <!-- modal_content_wrapper_start -->
            <div class="modal-content">
                <!-- modal_header_start -->
                <div class="modal-header">
                    <!-- modal_exit_button_start -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{ asset('assets/icons/white-left-arrow.png') }}" width="24" height="24">
                    </button>
                    <!-- modal_exit_button_end -->

                    <!-- modal_title_text -->
                    <h5 class="modal-title">Escolha o Modelo</h5>

                    <!-- three_dots_wrapper_start -->
                    <div class="dropdown">
                        <!-- three_dots_icon -->
                        <img src="{{ asset('assets/icons/three-dots.png') }}" alt="context-menu" width="10" height="24" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        
                        <!-- three_dots_menu_wrapper_start -->
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="navbar-dropdown">
                            <!-- whatsapp_link_start -->
                            <a class="dropdown-item poppins font-medium text-primary-gray font-small" href="https://api.whatsapp.com/send?phone={{ env('WHATSAPP_CONTACT') }}" target="_blank">
                                <!-- phone_icon -->
                                <img src="{{ asset('assets/icons/phone.png') }}" alt="whatsapp" width="24" height="24" class="mr-2">
                                Entrar em contato
                            </a>
                            <!-- whatsapp_link_end -->
                        </div>
                        <!-- three_dots_menu_wrapper_end -->
                    </div>
                    <!-- three_dots_wrapper_end -->
                </div>
                <!-- modal_header_end -->

                <!-- modal_body_start -->
                <div class="modal-body">
                    <!-- modal_breadcrumb_start -->
                    <div class="modal-breadcrump">
                        <!-- breadcrumb_title -->
                        <h3>Selecione as EXPESSURAS do MDF:</h3>
                    </div>
                    <!-- modal_breadcrumb_end -->

                    <!-- modal_body_content_wrapper_start -->
                    <div class="container">
                        <!-- external_thickness_box_start -->
                        <div class="row py-3 pl-1 pr-3 mt-3 white-box">
                            <div class="col-12 d-flex flex-column">
                                <!-- box_title -->
                                <h3 class="poppins font-medium font-size-default">Espessura EXTERNA</h3>

                                <!-- options_wrapper_start -->
                                <div class="d-inline-flex flex-row justify-content-between poppins font-small mt-3">
                                    <!-- first_option_wrapper_start -->
                                    <div>
                                        <!-- option_input -->
                                        <input type="radio" class="external-thickness-radio" name="unsigned-external-thickness" autocomplete="off" @if(isset($defaultExternalThickness)) checked @endif value="15">
                                        
                                        <!-- option_label -->
                                        <label for="external-thickness-first" style="position: relative; bottom: 6px; left: 4px">15mm</label>
                                    </div>
                                    <!-- first_option_wrapper_end -->

                                    <!-- second_option_wrapper_start -->
                                    <div>
                                        <!-- option_input -->
                                        <input type="radio" class="external-thickness-radio" name="unsigned-external-thickness" autocomplete="off" @if(isset($mediumExternalThickness)) checked @endif value="18">
                                        
                                        <!-- option_label -->
                                        <label for="external-thickness-second" style="position: relative; bottom: 6px; left: 4px">18mm</label>
                                    </div>
                                    <!-- second_option_wrapper_end -->

                                    <!-- third_option_wrapper_start -->
                                    <div>
                                        <!-- option_input -->
                                        <input type="radio" class="external-thickness-radio" name="unsigned-external-thickness" autocomplete="off" @if(isset($largeExternalThickness)) checked @endif value="25">
                                        
                                        <!-- option_label -->
                                        <label for="external-thickness-third" style="position: relative; bottom: 6px; left: 4px">25mm</label>
                                    </div>
                                    <!-- second_option_wrapper_end -->
                                </div>
                                <!-- options_wrapper_end -->
                            </div>
                        </div>
                        <!-- external_thickness_box_end -->

                        <!-- internal_thickness_box_start -->
                        <div class="row py-3 pl-1 pr-3 mt-3 white-box">
                            <div class="col-12 d-flex flex-column">
                                <!-- box_title -->
                                <h3 class="poppins font-medium font-size-default">Espessura INTERNA</h3>

                                <!-- options_wrapper_start -->
                                <div class="d-inline-flex flex-row justify-content-between poppins font-small mt-3">
                                    <!-- first_option_wrapper_start -->
                                    <div>
                                        <!-- option_input -->
                                        <input type="radio" class="internal-thickness-radio" name="unsigned-internal-thickness" autocomplete="off" @if(isset($defaultInternalThickness)) checked @endif value="15">
                                        
                                        <!-- option_label -->
                                        <label style="position: relative; bottom: 6px; left: 4px">15mm</label>
                                    </div>
                                    <!-- first_option_wrapper_end -->

                                    <!-- second_option_wrapper_start -->
                                    <div>
                                        <!-- option_input -->
                                        <input type="radio" class="internal-thickness-radio" name="unsigned-internal-thickness" autocomplete="off" @if(isset($mediumInternalThickness)) checked @endif value="18">
                                        
                                        <!-- option_label -->
                                        <label style="position: relative; bottom: 6px; left: 4px">18mm</label>
                                    </div>
                                    <!-- second_option_wrapper_end -->

                                    <!-- third_option_wrapper_start -->
                                    <div>
                                        <!-- option_input -->
                                        <input type="radio" class="internal-thickness-radio" name="unsigned-internal-thickness" autocomplete="off" @if(isset($largeInternalThickness)) checked @endif value="25">
                                        
                                        <!-- option_label -->
                                        <label style="position: relative; bottom: 6px; left: 4px">25mm</label>
                                    </div>
                                    <!-- second_option_wrapper_end -->
                                </div>
                                <!-- options_wrapper_end -->
                            </div>
                        </div>
                        <!-- internal_thickness_box_end -->
                        
                        <!-- shelves_thickness_box_start -->
                        <div class="row py-3 pl-1 pr-3 mt-3 white-box">
                            <div class="col-12 d-flex flex-column">
                                <!-- box_title -->
                                <h3 class="poppins font-medium font-size-default">Espessura das PRATELEIRAS</h3>

                                <!-- options_wrapper_start -->
                                <div class="d-inline-flex flex-row justify-content-between poppins font-small mt-3">
                                    <!-- first_option_wrapper_start -->
                                    <div>
                                        <!-- option_input -->
                                        <input type="radio" class="shelves-thickness-radio" name="unsigned-shelves-thickness" autocomplete="off" @if(isset($defaultShelvesThickness)) checked @endif value="15">
                                        <!-- option_label -->
                                        <label for="shelves-thickness-first" style="position: relative; bottom: 6px; left: 4px">15mm</label>
                                    </div>
                                    <!-- first_option_wrapper_end -->

                                    <!-- second_option_wrapper_start -->
                                    <div>
                                        <!-- option_input -->
                                        <input type="radio" class="shelves-thickness-radio" name="unsigned-shelves-thickness" autocomplete="off" @if(isset($mediumShelvesThickness)) checked @endif value="18">
                                        
                                        <!-- option_label -->
                                        <label for="shelves-thickness-second" style="position: relative; bottom: 6px; left: 4px">18mm</label>
                                    </div>
                                    <!-- second_option_wrapper_end -->

                                    <!-- third_option_wrapper_start -->
                                    <div>
                                        <!-- option_input -->
                                        <input type="radio" class="shelves-thickness-radio" name="unsigned-shelves-thickness" autocomplete="off" @if(isset($largeShelvesThickness)) checked @endif value="25">
                                        
                                        <!-- option_label -->
                                        <label for="shelves-thickness-third" style="position: relative; bottom: 6px; left: 4px">25mm</label>
                                    </div>
                                    <!-- second_option_wrapper_end -->
                                </div>
                                <!-- options_wrapper_end -->
                            </div>
                        </div>
                        <!-- shelves_thickness_box_start -->

                        <!-- separator -->
                        <hr class="mt-3">

                        <!-- mirror_doors_box_start -->
                        <div class="row white-box mt-4 border-top" id="mirror-doors-toggler">
                            <div class="col-3 py-2">
                                <img src="{{ asset('uploads/advanced-customization/mirror-doors.png') }}" alt="mirror-doors" height="66" width="66" class="border rounded">
                            </div>
                            <div class="col-9 py-4 pl-4">
                                <h3 class="poppins text-primary-gray font-size-default font-medium">
                                    <img src="{{ asset('assets/icons/gray-plus.png') }}" alt="add" height="20" width="20" class="mr-2">

                                    Portas de Espelho
                                </h3>
                            </div>
                        </div>
                        <!-- mirror_doors_box_end -->

                        @foreach($advancedCustomizationDisabled as $item)
                            <!-- disabled_box_start -->
                            <div class="row white-box disabled-customization mt-3">
                                <div class="col-3 py-2">
                                    <img src="{{ asset($item['img']) }}" alt="disabled-item" height="66" width="66" class="border rounded">
                                </div>
                                <div class="col-9 py-4 pl-4">
                                    <h3 class="poppins text-primary-gray font-size-default font-medium">
                                        <img src="{{ asset('assets/icons/gray-plus.png') }}" alt="add" height="20" width="20" class="mr-2">

                                        {{ $item["text"] }}<br>
                                        <span class="individual-in-dev">
                                            Em Desenvolvimento
                                        </span>
                                    </h3>
                                </div>
                            </div>
                            <!-- disablrs_box_end -->
                        @endforeach

                        <!-- complete_settings_button_wrapper_start -->
                        <div class="row mt-5 pb-4">
                            <div class="col-12 d-flex justify-content-end">
                                <button class="btn btn-primary" id="complete-advanced-customization">Continuar</button>
                            </div>
                        </div>
                        <!-- complete_settings_button_wrapper_end -->
                    </div>
                    <!-- modal_body_content_wrapper_end -->
                </div>
                <!-- modal_body_end -->
            </div>
            <!-- modal_content_wrapper_end -->
        </div>
    </div>
    <!-- advanced_customizations_modal_end -->

    <!-- add_mirror_doors_modal_start -->
    <div class="modal modal-fullscreen fade" tabindex="-1" role="dialog" id="add-mirror-doors-modal">
        <div class="modal-dialog" role="document">
            <!-- modal_content_wrapper_start -->
            <div class="modal-content">
                <!-- modal_header_start -->
                <div class="modal-header">
                    <!-- modal_exit_button_start -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{ asset('assets/icons/white-left-arrow.png') }}" width="24" height="24">
                    </button>
                    <!-- modal_exit_button_end -->

                    <!-- modal_title_text -->
                    <h5 class="modal-title">Escolha o Modelo</h5>

                    <!-- three_dots_wrapper_start -->
                    <div class="dropdown">
                        <!-- three_dots_icon -->
                        <img src="{{ asset('assets/icons/three-dots.png') }}" alt="context-menu" width="10" height="24" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        
                        <!-- three_dots_menu_wrapper_start -->
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="navbar-dropdown">
                            <!-- whatsapp_link_start -->
                            <a class="dropdown-item poppins font-medium text-primary-gray font-small" href="#"href="https://api.whatsapp.com/send?phone={{ env('WHATSAPP_CONTACT') }}" target="_blank">
                                <!-- phone_icon -->
                                <img src="{{ asset('assets/icons/phone.png') }}" alt="whatsapp" width="24" height="24" class="mr-2">
                                Entrar em contato
                            </a>
                            <!-- whatsapp_link_end -->
                        </div>
                        <!-- three_dots_menu_wrapper_end -->
                    </div>
                    <!-- three_dots_wrapper_end -->
                </div>
                <!-- modal_header_end -->

                <!-- modal_body_start -->
                <div class="modal-body">
                    <!-- modal_breadcrumb_start -->
                    <div class="modal-breadcrump">
                        <!-- breadcrumb_title -->
                        <h3>Escolha em Qual Porta vai o Espelho:</h3>
                    </div>
                    <!-- modal_breadcrumb_end -->

                    <!-- modal_body_content_wrapper_start -->
                    <div class="container mt-3">
                        <div class="row white-box py-3">
                            <div class="col-12 d-flex flex-row justify-content-between poppins font-small text-uppercase">
                                <div>
                                    <input type="checkbox" class="mirror-door-selection" value="1" autocomplete="off">
                                    <label for="">Esquerda</label>
                                </div>
                                <div>
                                    <input type="checkbox" class="mirror-door-selection" value="2" autocomplete="off">
                                    <label for="">Centro</label>
                                </div>
                                <div>
                                    <input type="checkbox" class="mirror-door-selection" value="3" autocomplete="off">
                                    <label for="">Direita</label>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-5">
                            <div class="col-12">
                                <h3 class="poppins font-size-default font-medium">Escolha a COR do Espelho:</h3>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="ml-3 col-6 white-box py-2 px-3 d-flex flex-column" id="mirror-set">
                                <h4 class="poppins font-size-default text-primary-gray text-center" id="mirror-doors-name">BRONZE</h4>
                                <img src="{{ asset('uploads/generic/bronze-mirror.png') }}" alt="mirrors" id="mirror-doors-img">
                                <a href="" class="font-small poppins text-center mt-2" id="mirror-color-toggler">Clique para alterar</a>
                            </div>
                        </div>
                    </div>
                    <!-- modal_body_content_wrapper_end -->
                </div>
                <!-- modal_body_end -->
            </div>
            <!-- modal_content_wrapper_end -->
        </div>
    </div>
    <!-- add_mirror_doors_modal_end -->

    <!-- mirror_color_modal_start -->
    <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="mirror-color-modal">
        <div class="modal-dialog" role="document">
            <!-- modal_content_wrapper_start -->
            <div class="modal-content">
                <!-- modal_body_start -->
                <div class="modal-body">
                    <!-- modal_breadcrumb_start -->
                    <div class="modal-breadcrump">
                        <!-- breadcrumb_title -->
                        <h3>Selecione a COR do Espelho:</h3>
                    </div>
                    <!-- modal_breadcrumb_end -->

                    <!-- modal_body_content_wrapper_start -->
                    <div class="container">
                        <div class="row">
                            @foreach($mirrors as $mirror)
                                <div class="ml-2 white-box d-flex flex-column mirror-color-select" style="width: 47% !important">
                                    <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $mirror->name }}</h3>
                                    <img src="/{{ $mirror->image }}" height="104" width="131" class="mx-2 rounded border">
                                    <span class="font-small mt-2 text-secondary-gray pl-2 pb-2">{{ $mirror->color_name }}</span>
                                </div>
                            @endforeach
                        </div>

                        <div class="row mt-4 white-box">
                            <div class="col-12 d-flex flex-column pl-3 pb-3 pt-2 poppins">
                                <h3 class="font-size-default font-medium text-primary-gray pl-2">Outro Espelho (Coringa)</h3>

                                <div class="d-flex flex-row row">
                                    <div class="col-5">
                                        <label for="#mirror-custom-name" class="font-small font-light">Nome da COR</label>
                                    </div>
                                    <div class="col-5">
                                        <label for="#mirror-custom-code" class="font-small font-light">Código da COR</label>
                                    </div>
                                </div>

                                <div class="d-flex flex-row">
                                    <div class="col-5 px-0">
                                        <input type="text" class="form-control" id="mirror-custom-name" placeholder="Digite o Nome" autocomplete="off">
                                    </div>
                                    <div class="col-5 pr-0 pl-1">
                                        <input type="text" class="form-control" id="mirror-custom-code" placeholder="Ex: 122F6E" autocomplete="off">
                                    </div>
                                    <div class="col-2 pr-0 pl-1">
                                        <button class="btn btn-primary" id="custom-mirror-color-save" style="width: auto; height: auto">IR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal_body_content_wrapper_end -->
                </div>
                <!-- modal_body_end -->
            </div>
            <!-- modal_content_wrapper_end -->
        </div>
    </div>
    <!-- mirror_color_modal_end -->
@endsection

@push('js')
    <script src="{{ asset('js/structures.js') }}"></script>

    <script>
        // Set event listeners
        $(document).ready(() => {
            $('input[name="structure-id"]').click();
            // Handle finishing initial set
            getFinishingViewConfiguration();

            // Automatically adjust finishing view when finishing settings change
            $("input[name='finishing']").on("change", getFinishingViewConfiguration);

            // Link all height fields
            $("input[name='module-height']").keyup(changeModelHeight);
            $("input[name='left-finishing-height']").keyup(changeModelHeight);
            $("input[name='right-finishing-height']").keyup(changeModelHeight);

            // Link all depth fields
            $("input[name='module-depth']").keyup(changeModelDepth);
            $("input[name='left-finishing-depth']").keyup(changeModelDepth);
            $("input[name='right-finishing-depth']").keyup(changeModelDepth);

            // Link left finishing input with his viewer
            $("input[name='left-finishing-width']").keyup(leftFinishingViewerLink);

            // Link right finishing input with his viewer
            $("input[name='right-finishing-width']").keyup(rightFinishingViewerLink);

            // Link module width control with his viewer
            $("input[name='module-width']").keyup(moduleWidhtViewerLink);

            // Toggle module models selection modal
            $(".change-model-link").click(openModelsSelection);

            // Open advanced customization when user click in his toggler
            $("#advanced-customization-toggler").click(() => { 
                $("#advanced-customization-modal").modal('toggle');
            });

            // Open add mirror doors modal when user click in his toggler
            $("#mirror-doors-toggler").click(() => {
                $("#add-mirror-doors-modal").modal('toggle');
            });

            // Open mirror color configuration screne
            $("#mirror-color-toggler").click((e) => {
                e.preventDefault();
                $("#mirror-color-modal").modal('toggle');
            });

            $("#custom-mirror-color-save").click(() => {
                // Get custom mirror properties values
                let $colorName = $("#mirror-custom-name").val();
                let $colorCode = $("#mirror-custom-code").val();

                // Verifies if fields are filled
                if ($colorName == undefined || $colorName == undefined || $colorName == null || $colorName == null ||  $colorName.length == 0 || $colorName.length == 0 ) {
                    // Alert the user to fill the fields
                    alert('Preencha corretamente os campos!');

                    // Ends the function
                    return;
                }

                // Get checkboxes values
                let $leftDoor   = $(".mirror-door-selection")[0];
                let $middleDoor = $(".mirror-door-selection")[1];
                let $rightDoor  = $(".mirror-door-selection")[2];

                // Verifies which doors are checked
                if ($($leftDoor).is(':checked') == true) {
                    // Adds the left door input to page form
                    $("#left-mirror-door").val("1");
                } else {
                    // Remove this configuration from page
                    $("#left-mirror-door").removeAttr('value');
                }
                
                if ($($middleDoor).is(':checked') == true) {
                    // Adds the middle door input to page form
                    $("#center-mirror-door").val("2");
                } else {
                    // Remove this configuration from page
                    $("#center-mirror-door").removeAttr('value');
                }
                
                if ($($rightDoor).is(':checked') == true) {
                    // Adds the right door input to page form
                    $("#right-mirror-door").val("3");
                }else {
                    // Remove this configuration from page
                    $("#right-mirror-door").removeAttr('value');
                }

                // Verifies if any checkbox option was checked
                if ($($leftDoor).is(':checked') === false && $($middleDoor).is(':checked') == false && $($rightDoor).is(':checked') === false) {
                    // Alert the user to select one door
                    alert("Por favor, selecione ao menos uma porta para o espelho");

                    // Ends the function
                    return;
                }

                // Send the data to page form
                $("input[name='mirror-doors-color']").val($colorName);
                $("input[name='mirror-doors-color-code']").val($colorCode);

                // Change the mirror name
                $("#mirror-doors-name").text(`${$colorName} - Personalizada`);

                // Add blue-box class to #mirror-set and #mirror-doors-toggler
                $("#mirror-set").addClass("blue-box");
                $("#mirror-doors-toggler").addClass("blue-box");

                // Dismiss the mirror color selection modal
                $("#mirror-color-modal").modal('toggle');
                $("#add-mirror-doors-modal").modal('toggle');
            });

            $(".mirror-color-select").click(function () {
                // Get mirror name and image
                let $colorName = $(this).find("h3").text();
                let $mirrorImg = $(this).find("img").attr("src");

                // Get checkboxes values
                let $leftDoor   = $(".mirror-door-selection")[0];
                let $middleDoor = $(".mirror-door-selection")[1];
                let $rightDoor  = $(".mirror-door-selection")[2];

                // Verifies which doors are checked
                if ($($leftDoor).is(':checked') == true) {
                    // Adds the left door input to page form
                    $("#left-mirror-door").val("1");
                } else {
                    // Remove this configuration from page
                    $("#left-mirror-door").removeAttr('value');
                }
                
                if ($($middleDoor).is(':checked') == true) {
                    // Adds the middle door input to page form
                    $("#center-mirror-door").val("2");
                } else {
                    // Remove this configuration from page
                    $("#center-mirror-door").removeAttr('value');
                }
                
                if ($($rightDoor).is(':checked') == true) {
                    // Adds the right door input to page form
                    $("#right-mirror-door").val("3");
                }else {
                    // Remove this configuration from page
                    $("#right-mirror-door").removeAttr('value');
                }

                // Verifies if any checkbox option was checked
                if ($($leftDoor).is(':checked') === false && $($middleDoor).is(':checked') == false && $($rightDoor).is(':checked') === false) {
                    // Alert the user to select one door
                    alert("Por favor, selecione ao menos uma porta para o espelho");

                    // Ends the function
                    return;
                }

                // Send the data to page form
                $("input[name='mirror-doors-color']").val($colorName);

                // Change the mirror name and image
                $("#mirror-doors-name").text($colorName);
                $("#mirror-doors-img").attr("src", $mirrorImg);

                // Add blue-box class to #mirror-set and #mirror-doors-toggler
                $("#mirror-set").addClass("blue-box");
                $("#mirror-doors-toggler").addClass("blue-box");

                // Dismiss the mirror color selection modal
                $("#mirror-color-modal").modal('toggle');
                $("#add-mirror-doors-modal").modal('toggle');
                $("#advanced-customization-modal").modal('toggle');
            });

            $(".shelves-thickness-radio").on("change", function () {
                // Get the value of checked button
                let $thicknessValue = $(this).val();

                // Add the value to page form
                $("input[name='shelves-thickness']").val($thicknessValue);
            });

            $(".internal-thickness-radio").on("change", function () {
                // Get the value of checked button
                let $thicknessValue = $(this).val();

                // Add the value to page form
                $("input[name='internal-thickness']").val($thicknessValue);
            });

            $(".external-thickness-radio").on("change", function () {
                // Get the value of checked button
                let $thicknessValue = $(this).val();

                // Add the value to page form
                $("input[name='external-thickness']").val($thicknessValue);
            });

            $("button[type='submit']").click((e) => {
                // Prevent the submit
                e.preventDefault();

                // Control form validation
                let $validForm = true;

                // Get the finishing setting
                let $finishingType = $('input[name="finishing"]:checked').val();

                // Switch in $finishingType
                switch ($finishingType) {
                    // No finishing
                    case 'no-finishing':
                        // Delete the sizes controls to finishings
                        $("input[name='right-finishing-height']").remove();
                        $("input[name='left-finishing-height']").remove();
                        $('input[name="right-finishing-width"]').remove();
                        $('input[name="left-finishing-width"]').remove();
                        $('input[name="right-finishing-depth"]').remove();
                        $('input[name="left-finishing-depth"]').remove();
                        
                        // Ends case
                        break;

                    // Right Finishing
                    case 'right-finishing':
                        // Verifies if the right finishing width was filled
                        if ($('input[name="right-finishing-width"]').val() === undefined 
                            || $('input[name="right-finishing-width"]').val() === null 
                            || $('input[name="right-finishing-width"]').val().length == 0
                            || $('input[name="right-finishing-width"]').val() == "0")
                        {
                            // Set form validation controller to false
                            $validForm = false;
                        }
                        else {                        
                            // Delete the sizes controls to left finishing, and height and depth to right
                            $("input[name='right-finishing-height']").remove();
                            $("input[name='left-finishing-height']").remove();
                            $('input[name="left-finishing-width"]').remove();
                            $('input[name="right-finishing-depth"]').remove();
                            $('input[name="left-finishing-depth"]').remove();
                        }

                        // Ends case
                        break;

                    // Left Finishing
                    case 'left-finishing':
                        // Verifies if the left finishing width was filled
                        if ($('input[name="left-finishing-width"]').val() === undefined 
                            || $('input[name="left-finishing-width"]').val() === null 
                            || $('input[name="left-finishing-width"]').val().length == 0
                            || $('input[name="left-finishing-width"]').val() == "0")
                        {
                            // Set form validation controller to false
                            $validForm = false;
                        }
                        else {                        
                            // Delete the sizes controls to left finishing, and height and depth to right
                            $("input[name='left-finishing-height']").remove();
                            $('input[name="left-finishing-depth"]').remove();
                            $("input[name='right-finishing-height']").remove();
                            $('input[name="right-finishing-width"]').remove();
                            $('input[name="right-finishing-depth"]').remove();
                        }

                        // Ends case
                        break;

                    // Both finishings
                    case 'both-finishing':
                        // Verifies if the both finishings width was filled
                        if ($('input[name="left-finishing-width"]').val() === undefined 
                            || $('input[name="left-finishing-width"]').val() === null 
                            || $('input[name="left-finishing-width"]').val().length == 0
                            || $('input[name="left-finishing-width"]').val() == "0"
                            || $('input[name="right-finishing-width"]').val() === undefined 
                            || $('input[name="right-finishing-width"]').val() === null 
                            || $('input[name="right-finishing-width"]').val().length == 0
                            || $('input[name="right-finishing-width"]').val() == "0")
                        {
                            // Set form validation controller to false
                            $validForm = false;
                        }
                        else {
                            $("input[name='left-finishing-height']").remove();
                            $('input[name="left-finishing-depth"]').remove();
                            $("input[name='right-finishing-height']").remove();
                            $('input[name="right-finishing-depth"]').remove();
                        }

                        // Ends case
                        break;
                }

                // Verifies if the module sizes was filled
                if ($('input[name="module-height"]').val() === undefined
                    || $('input[name="module-height"]').val() === null
                    || $('input[name="module-height"]').val().length == 0
                    || $('input[name="module-height"]').val() == "0"
                    || $('input[name="module-width"]').val() === undefined
                    || $('input[name="module-width"]').val() === null
                    || $('input[name="module-width"]').val().length == 0
                    || $('input[name="module-width"]').val() == "0"
                    || $('input[name="module-depth"]').val() === undefined
                    || $('input[name="module-depth"]').val() === null
                    || $('input[name="module-depth"]').val().length == 0
                    || $('input[name="module-depth"]').val() == "0")
                {
                    // Set form validation controller to false
                    $validForm = false;
                }

                // Verifies if the doors type has be checked
                if ($("input[name='structure-id']").is(':checked') == false) {
                    // Set form validation controller to false
                    $validForm = false;
                }

                // Verifies if the form validation was altered
                if ($validForm === false) {  // Doesn't submit data
                    // Alert the user to properly fill data
                    alert("Por favor, preencha corretamente os dados!");
                } else {
                    // Submit the form
                    $("form").submit();
                }
            })
        });

    </script>
@endpush