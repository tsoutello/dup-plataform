@extends('layouts.budgets.app')

@section('title', 'Resumo do Orçamento')
@section('navbar-title', 'Novo Orçamento')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/appearance.css') }}">
    <link rel="stylesheet" href="{{ asset('css/components/modals.css')}}">
    <style>
        .page-divider-text {
            width: 100%;
            padding: 1.5rem;
            color: #0f4589;
            font-family: "Poppins", sans-serif;
            font-weight: 500;
            font-size: 1.5rem;
            text-transform: uppercase;
        }

        /* Left finishing view wrapper */
        .left-finishing-view {
            display: flex;
            flex-direction: column;
            height: 100%;
            width: 4%;
            max-width: 4%;
        }

        /* Left finishing viewer */
        .left-finishing-view .viewer {
            height: 85%;
            width: 100%;
            border: 1px solid #33BF1D;
            border-radius: 0;
            background: url('{{ asset("assets/backgrounds/structure-measure-viewer.png") }}') repeat center 100%;
        }

        .left-finishing-view .viewer::before {
            content: "ACABAMENTO ESQUERDO";
            position: relative;
            top: 65%;
            right: 7px;
            display: inline-flex;
            width: 0%;
            transform: rotate(270deg);
            color: #1F880E;
            font-size: .5rem;
            white-space: nowrap;
        }

        #left-finishing-view-measure {
            padding-left: 2px;
            color: #1F880E;
            font-weight: 500;
            font-size: .5rem;
        }

        #left-finishing-view-measure::after {
            content: "";
            display: block;
            width: 100%;
            padding-top: .3rem;
            border: 1px solid #33BF1D;
                border-top: 0;
        }

        .module-size-view {
            display: flex;
            flex-direction: column;
            height: 100%;
            width: 75%;
            max-width: 75%;
            padding-left: .5rem;
        }

        .module-size-view img {
            height: 85%;
            width: 100%;
            border: 1.04296px solid #2775D7;
        }

        .module-size-view .measure {
            display: flex;
            justify-content: center;
            height: 15%;
            width: 100%;
            padding-top: .5rem;
        }

        .module-size-view .measure span {
            width: 100%;
            padding-left: 2px;
            color: #6F90B8;
            font-weight: 500;
            font-size: .5rem;
            text-align: center;
            text-transform: uppercase;
        }

        .module-size-view .measure span::before {
            content: attr(data-width-measure);
            display: block;
            width: 100%;
            margin-bottom: .3rem;
            border: 1px solid #6F90B8;
                border-top: 0;
            font-size: .5rem;
        }

        .module-height-view {
            height: 85%;
            width: 8%;
        }

        .module-height-view #module-height-viewer{
            display: flex;
            flex-direction: column;
            justify-content: center;
            height: 100%;
            width: 100%;
            margin-left: .5rem;
            color:#6F90B8;
            font-size: .5rem;
            text-align: center;
        }

        .module-height-view #module-height-viewer::before,
        .module-height-view #module-height-viewer::after {
            content: "";
            height: 45%;
            width: 30%;
            margin-left: .375rem;
            border: 1px solid #6F90B8;
                border-left: 0;
        }

        .module-height-view #module-height-viewer::before {
            border-bottom: 0;
        }

        .module-height-view #module-height-viewer::after {
            border-top: 0
        }

        /* Left finishing view wrapper */
        .right-finishing-view {
            display: flex;
            flex-direction: column;
            height: 100%;
            width: 4%;
            max-width: 4%;
            margin-left: .5rem;
        }

        /* Left finishing viewer */
        .right-finishing-view .viewer {
            height: 85%;
            width: 100%;
            border: 1px solid #A723E6;
            border-radius: 0;
            background: url('{{ asset("assets/backgrounds/structure-measure-viewer.png") }}') repeat center 100%;
        }

        .right-finishing-view .viewer::before {
            content: "ACABAMENTO DIREITO";
            position: relative;
            top: 65%;
            left: 18px;
            display: inline-flex;
            width: 0%;
            transform: rotate(270deg);
            color: #831AB5;
            font-size: .5rem;
            white-space: nowrap;
        }

        #right-finishing-view-measure {
            padding-left: 2px;
            color: #831AB5;
            font-weight: 500;
            font-size: .5rem;
        }

        #right-finishing-view-measure::after {
            content: "";
            display: block;
            width: 100%;
            padding-top: .3rem;
            border: 1px solid #A723E6;
                border-top: 0;
        }

        .module-total-width-viewer::before {
            content: "";
            display: block;
            width: 100%;
            margin-bottom: .25rem;
            padding-top: .563rem;
            border: 1px solid #9E9E9E;
                border-top: 0;
        }

        .module-total-width-viewer {
            display: block;
            width: 100%;
            color: var(--primary-gray);
            font-weight: 500;
            font-size: .5rem;
            text-align: center;
            text-transform: uppercase;
        }

        .viewers {
            box-shadow: none !important;
            border: none !important;
            border-bottom: #D7D7D7 solid 1.4px !important;
            border-bottom-left-radius: 0 !important;
            border-bottom-right-radius: 0 !important;
        }

        ul.data-list {
            list-style: none;
        }

        ul.data-list li::before {
            content: "\2022";
            color: var(--secondary-gray);
            font-weight: bold;
            display: inline-block;
            width: 1em;
            margin-left: -1em;
        }
    </style>
@endpush

@section('content')
    @csrf

    <!-- content_wrapper_start -->
    <div class="container">
        <!-- page_name_wrapper_start -->
        <div class="row">
            <div class="col-12">
                <!-- page_name_text_start -->
                <h1 class="poppins font-extra-large text-uppercase">
                    <!-- text -->
                    Conclusão
                </h1>
                <!-- page_name_text_end -->
            </div>
        </div>
        <!-- page_name_wrapper_end -->

        <!-- page_sub_text_wrapper_start -->
        <div class="row mt-3">
            <div class="col-12 d-flex flex-column">
                <!-- page_sub_title -->
                <h3 class="poppins font-size-default font-medium">Confirme as Informações:</h3>

                <!-- page_subtitle_start -->
                <p class="poppins text-primary-gray font-medium font-small">
                    Confirme as Informações do Ambiente, podendo ADICIONAR, REMOVER ou EDITAR os Itens.
                </p>
                <!-- page_subtitle_end -->
            </div>
        </div>
        <!-- page_sub_text_wrapper_end -->

        <!-- overview_section_divider_wrapper_start -->
        <div class="row">
            <!-- colors_section -->
            <h2 class="page-divider-text">Resumo</h2>
        </div>
        <!-- overview_section_divider_wrapper_end -->

        @foreach($environment as $envTarget => $environment)
            <!-- environment_wrapper_start -->
            <div class="row white-box">
                <!-- environment_name_wrapper_start -->
                <div class="w-100 pt-2 pl-3">
                    <!-- environment_name -->
                    <span class="font-size-default font-regular text-dark-blue">{{ $environment["environmentName"] }}</span>
                </div>
                <!-- environment_name_wrapper_end -->

                {{-- environment furnitures loop start --}}
                @foreach($environment["furnitures"] as $furnitureTarget => $furniture)
                    <div class="w-100 mt-2">
                        <!-- furniture_name -->
                        <h3 class="poppins font-extra-large pl-3 py-1" style="background: #F5F5F5;">{{ $furniture["furniture"]["furnitureAlias"] }}:</h3>
                        <a class="poppins font-size-default text-uppercase font-medium pl-3" 
                            role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Editar
                        </a>

                        <div class="dropdown-menu ml-5" aria-labelledby="dropdownMenuLink">
                            <!-- change_structure_start -->
                            <a class="dropdown-item edit-furniture-structure text-primary-gray font-small" 
                                data-furniture-target="{{ $furnitureTarget }}" data-environment-target="{{ $envTarget }}">
                                Alterar ESTRUTURA
                            </a>
                            <!-- change_structure_end -->

                            <!-- change_structure_start -->
                            <a class="dropdown-item edit-furniture-appearance text-primary-gray font-small" 
                                data-furniture-target="{{ $furnitureTarget }}" data-environment-target="{{ $envTarget }}">
                                Alterar APARÊNCIA
                            </a>
                            <!-- change_structure_end -->

                            <!-- change_module_name_start -->
                            <a class="dropdown-item edit-furniture-name text-primary-gray font-small" 
                                data-furniture-target="{{ $furnitureTarget }}" data-environment-target="{{ $envTarget }}">
                                Nome do Módulo
                            </a>
                            <!-- change_module_name_end -->

                            <!-- change_module_name_start -->
                            <a class="dropdown-item edit-environment-name text-primary-gray font-small" data-environment-target="{{ $envTarget }}">
                                Nome do Ambiente
                            </a>
                            <!-- change_module_name_start -->
                        </div>

                        <!-- module_sizes_wrapper_start -->
                        <div class="row">
                            <div class="col-12 poppins">
                                <div class="d-flex flex-column white-box pb-3 pt-2 viewers pl-3">
                                    <!-- module_sizes_start -->
                                    <div class="row pl-3 mt-2" style="height: 300px" id="module-sizes-overview">
                                        @if($furniture["data"]["finishing"]["type"] == "left-finishing" || $furniture["data"]["finishing"]["type"] == "both-finishing")
                                            <!-- left_finishing_start -->
                                            <div class="left-finishing-view">
                                                <!-- finishing_viewer -->
                                                <div class="viewer">⠀⠀⠀</div>
                                                
                                                <!-- finishing_measure_start -->
                                                <div class="measure">
                                                    <!-- measeure_value -->
                                                    <span id="left-finishing-view-measure">{{ $furniture["data"]["finishing"]["width"] }}</span>
                                                </div>
                                                <!-- finishing_measure_end -->
                                            </div>
                                            <!-- left_finishing_end -->
                                        @else
                                            <!-- left_finishing_start -->
                                            <div class="left-finishing-view d-none">
                                                <!-- finishing_viewer -->
                                                <div class="viewer">⠀⠀⠀</div>
                                                
                                                <!-- finishing_measure_start -->
                                                <div class="measure">
                                                    <!-- measeure_value -->
                                                    <span id="left-finishing-view-measure"></span>
                                                </div>
                                                <!-- finishing_measure_end -->
                                            </div>
                                            <!-- left_finishing_end -->
                                        @endif

                                        <!-- module_width_start -->
                                        <div class="module-size-view">
                                            <!-- module_image_wrapper -->
                                            <img src="" class="furniture-image" data-model-id="{{ $furniture["data"]["model"]["modelID"] }}">

                                            <!-- module_measures_start -->
                                            <div class="measure">
                                                <!-- module_name -->
                                                <span id="module-width-measure" data-width-measure="{{ floatval($furniture["data"]["model"]["modelWidth"]) * 1000 }}">{{ $furniture["furniture"]["furnitureAlias"] }}</span>
                                            </div>
                                            <!-- module_measures_end -->
                                        </div>
                                        <!-- module_width_end -->

                                        @if($furniture["data"]["finishing"]["type"] == "right-finishing" || $furniture["data"]["finishing"]["type"] == "both-finishing")
                                            <!-- right_finishing_start -->
                                            <div class="right-finishing-view">
                                                <!-- finishing_viewer -->
                                                <div class="viewer">⠀⠀⠀</div>
                                            
                                                <!-- finishing_measure_start -->
                                                <div class="measure">
                                                    <!-- measeure_value -->
                                                    <span id="right-finishing-view-measure">{{ $furniture["data"]["finishing"]["width"] }} </span>
                                                </div>
                                                <!-- finishing_measure_end -->
                                            </div>
                                            <!-- right_finishing_end -->
                                        @else
                                            <!-- right_finishing_start -->
                                            <div class="right-finishing-view d-none">
                                                <!-- finishing_viewer -->
                                                <div class="viewer">⠀⠀⠀</div>
                                            
                                                <!-- finishing_measure_start -->
                                                <div class="measure">
                                                    <!-- measeure_value -->
                                                    <span id="right-finishing-view-measure"></span>
                                                </div>
                                                <!-- finishing_measure_end -->
                                            </div>
                                            <!-- right_finishing_end -->
                                        @endif

                                        <!-- module_height_start -->
                                        <div class="module-height-view">
                                            <!-- module_height_viewer -->
                                            <span id="module-height-viewer">{{ floatval($furniture["data"]["model"]["modelHeight"]) * 1000 }} CHÃO AO TETO</span>
                                        </div>
                                        <!-- module_height_end -->
                                    </div>
                                    <!-- module_sizes_end -->

                                    <!-- module_total_width_start -->
                                    <div class="row pl-3 poppins module-total-width">
                                        <div class="w-100">
                                            <!-- width_viewer_start -->
                                            <span class="module-total-width-viewer">
                                                Largura Total:

                                                <!-- module_width -->
                                                <span id="module-total-width-value">
                                                    @if($furniture["data"]["finishing"]["type"] == "no-finshing")
                                                        {{ floatval($furniture["data"]["model"]["modelWidth"]) * 1000 }}
                                                    @elseif($furniture["data"]["finishing"]["type"] == "both-finishing")
                                                        {{ (floatval($furniture["data"]["model"]["modelWidth"]) * 1000) + (intval($furniture["data"]["finishing"]["width"]) * 2) }}
                                                    @else
                                                        {{ (floatval($furniture["data"]["model"]["modelWidth"]) * 1000) + intval($furniture["data"]["finishing"]["width"]) }}
                                                    @endif
                                                </span>
                                            </span>
                                            <!-- width_viewer_end -->
                                        </div>
                                    </div>
                                    <!-- module_total_width_end -->
                                </div>
                                <!-- module_total_width_end -->
                            </div>
                        </div>
                        <!-- module_sizes_wrapper_end -->

                        <!-- module_colors_start -->
                        <div class="row pt-3">
                            <div class="col-12 poppins d-flex flex-column">
                                <!-- info_title -->
                                <h5 class="text-uppercase pl-3 font-small font-medium" style="color: #2775D7">Cor</h5>

                                <!-- color_boxes_start -->
                                <div class="mt-3 d-flex flex-row">
                                    <!-- external_color_viewer -->
                                    <span class="border rounded ml-3" style="height: 34px; width: 34px; background: {{ $furniture["data"]["moduleColor"]["externalCode"] }}"> </span>
                                    
                                    <!-- external_color_info_wrapper_start -->
                                    <div class="d-flex flex-column ml-1">
                                        <h6 class="font-small">
                                            @switch($furniture["data"]["moduleColor"]["external"])
                                                @case('Cinza Sagra...')
                                                    Cinza Sagrado
                                                    @break
                                                @default
                                                    {{ $furniture["data"]["moduleColor"]["external"] }}
                                                    @break
                                            @endswitch
                                        </h6>
                                                
                                        <h6 class="font-small text-secondary-gray">(Cor EXTERNA)</h6>
                                    </div>
                                    <!-- external_color_info_wrapper_end -->

                                    <!-- internal_color_viewer -->
                                    <span class="border rounded ml-5" style="height: 34px; width: 34px; background: {{ $furniture["data"]["moduleColor"]["internalCode"] }}"> </span>
                                    
                                    <!-- internal_color_info_wrapper_start -->
                                    <div class="d-flex flex-column ml-1">
                                        <h6 class="font-small">
                                            @switch($furniture["data"]["moduleColor"]["internal"])
                                                @case('Cinza Sagra...')
                                                    Cinza Sagrado
                                                    @break
                                                @default
                                                    {{ $furniture["data"]["moduleColor"]["internal"] }}
                                                    @break
                                            @endswitch
                                        </h6>
                                                
                                        <h6 class="font-small text-secondary-gray">(Cor INTERNA)</h6>
                                    </div>
                                    <!-- internal_color_info_wrapper_end -->
                                </div>
                                <!-- color_boxes_end -->
                            </div>
                        </div>
                        <!-- module_colors_end -->

                        <!-- module_structure_start -->
                        <div class="row pt-3">
                            <div class="col-12 poppins d-flex flex-column">
                                <!-- info_title -->
                                <h5 class="text-uppercase pl-3 font-small font-medium" style="color: #2775D7">Estrutura</h5>

                                <!-- info_data_start -->
                                <div class="d-flex flex-column">
                                    <ul class="data-list">
                                        <li class="font-small mt-2 doors-amount"></li>
                                        <li class="font-small mt-2">
                                            {{ $furniture["data"]["thickness"]["internal"] }}mm (INTERNA) /
                                            {{ $furniture["data"]["thickness"]["external"] }}mm (EXTERNA) /
                                            {{ $furniture["data"]["thickness"]["shelves"] }}mm (PRATELEIRAS)
                                        </li>
                                    </ul>
                                </div>
                                <!-- info_data_end -->
                            </div>
                        </div>
                        <!-- module_structure_end -->

                        <!-- module_ironwares_start -->
                        <div class="row pt-3">
                            <div class="col-12 poppins d-flex flex-column pb-3">
                                <!-- info_title -->
                                <h5 class="text-uppercase pl-3 font-small font-medium" style="color: #2775D7">Ferragens</h5>

                                <!-- doors_pullers_wrapper_start -->
                                <div class="pl-3">
                                    <!-- data_title -->
                                    <span class="font-small text-secondary-gray">Portas: </span>

                                    @if($furniture["data"]["doorsPuller"]["noPuller"] == true)
                                        <span class="font-small">Sem Puxador</span>
                                    @else
                                        <!-- puller_info_start -->
                                        <span class="font-small">
                                            {{ $furniture["data"]["doorsPuller"]["pullerName"] }} 
                                            - 
                                            {{ $furniture["data"]["doorsPuller"]["colors"]["colorName"] }}
                                        </span>
                                        <!-- puller_info_end -->
                                    @endif
                                </div>
                                <!-- doors_pullers_wrapper_end -->

                                <!-- drawers_pullers_wrapper_start -->
                                <div class="pl-3">
                                    <!-- data_title -->
                                    <span class="font-small text-secondary-gray">Gavetas: </span>

                                    @if($furniture["data"]["drawersPuller"]["noPuller"] == true)
                                        <span class="font-small">Sem Puxador</span>
                                    @else
                                        <!-- puller_info_start -->
                                        <span class="font-small">
                                            {{ $furniture["data"]["drawersPuller"]["drawerName"] }} 
                                            - 
                                            {{ $furniture["data"]["drawersPuller"]["colors"]["colorName"] }}
                                        </span>
                                        <!-- puller_info_end -->
                                    @endif
                                </div>
                                <!-- drawers_pullers_wrapper_end -->

                                <!-- sliders_wrapper_start -->
                                <div class="pl-3">
                                    <!-- data_title -->
                                    <span class="font-small text-secondary-gray">Corrediças: </span>

                                    <span class="slider-name font-small" data-slider-id="{{ $furniture["data"]["sliderId"] }}"></span>
                                </div>
                                <!-- sliders_wrapper_start -->
                            </div>
                        </div>
                        <!-- module_ironwares_end -->
                    </div>
                @endforeach
                {{-- environment furnitures loop end --}}
            </div>
            <!-- environment_wrapper_end -->
            
            <!-- add_module_start -->
            <div class="row white-box add-module" data-environment-target="{{ $envTarget }}">
                <!-- image_wrapper_start -->
                <div class="col-3 pl-2 py-1">
                    <img src="{{ asset('uploads/generic/add-module.png') }}" alt="" height="56" width="64" class="border rounded">
                </div>
                <!-- image_wrapper_end -->
    
                <!-- text_wrapper_start -->
                <div class="col pt-3">
                    <span class="font-size-default text-primary-gray poppins">Adicionar MÓDULO</span>
                </div>
                <!-- text_wrapper_end -->
            </div>
            <!-- add_module_end -->
        @endforeach

        <!-- add_environment_start -->
        <div class="row white-box add-env mt-3">
            <!-- image_wrapper_start -->
            <div class="col-3 pl-2 py-1">
                <img src="{{ asset('uploads/generic/add-env.png') }}" alt="" height="56" width="64" class="border rounded">
            </div>
            <!-- image_wrapper_end -->

            <!-- text_wrapper_start -->
            <div class="col pt-3">
                <span class="font-size-default text-primary-gray poppins">Adicionar AMBIENTE</span>
            </div>
            <!-- text_wrapper_end -->
        </div>
        <!-- add_environment_end -->

        <!-- continue_wrapper_start -->
        <div class="row mt-5">
            <div class="col-12 d-flex flex-row justify-content-end">
                <button onclick="window.location.href='{{ route('new-budget-create') }}';" class="btn btn-primary">Continuar</button>
            </div>
        </div>
        <!-- continue_wrapper_end -->
    </div>
    <!-- content_wrapper_end -->

    <!-- module_change_name_modal_start -->
    <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="furniture-name-modal">
        <div class="modal-dialog" role="document">
            <!-- modal_content_wrapper_start -->
            <div class="modal-content">
                <!-- modal_body_start -->
                <div class="modal-body">
                    <input type="text" class="form-control" id="furniture-name" placeholder="Nome para o módulo">
                    <button class="btn btn-primary mt-3" style="height: auto; width: auto" id="change-furniture-name">OK</button>
                </div>
                <!-- modal_body_end -->
            </div>
            <!-- modal_content_wrapper_end -->
        </div>
    </div>
    <!-- module_change_name_modal_end -->
@endsection

@push('js')
    <script type="text/javascript">
        function getModuleTotalWidth() {
            $(".right-finishing-view").each((index, el) => {
                console.log(index);
                // Get module viewers width
                let $leftFinishingWidth  = $($(".left-finishing-view")[index]).width();
                let $rightFinishingWidth = $($(".right-finishing-view")[index]).width();
                let $moduleHeightWidth   = $($(".module-height-view")[index]).width();
                let $moduleImageWidth    = $($(".module-size-view")[index]).width();
                let $computedWidth       = $leftFinishingWidth + $rightFinishingWidth + $moduleHeightWidth + $moduleImageWidth;
    
                // Set new width to the target
                $($(".module-total-width")[index]).css({
                    width: `${$computedWidth}px`
                });
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(() => {
            // Handle module widths set
            getModuleTotalWidth();

            // Loads models dynamic data
            $(".furniture-image").each(function () {
                // Get the model id
                let $modelId = $(this).attr("data-model-id");

                // Get csrf token
                let $csrf = $("input[name='_token']").val();

                // Ajax data to request model image
                let $ajaxData = {
                    _token : $csrf,
                    id     : $modelId
                }

                // Stores this element to get in ajax callback
                let $el = this;

                // Request by the model id
                $.ajax({
                    method: "POST",
                    url: "/models/get",
                    data: $ajaxData,

                    // Request callback
                    success: ($response) => {
                        // Parse the JSON of response
                        let $data = JSON.parse($response);

                        // Put the image-src into current looped element
                        $($el).attr("src", `/${$data.image}`);

                        // Get current model parent wrapper
                        let $master = $($el).parent().parent().parent().parent().parent().parent().parent();
                        
                        // Set the model doors amount
                        $($master).find('.doors-amount').text(`${$data.doors} Portas (de Correr)`);
                    }
                });
            });

            // Loads sliders dynamic data
            $(".slider-name").each(function () {
                // Get slider id
                let $sliderId = $(this).attr("data-slider-id");

                // Get CSRF token
                let $token = $("input[name='_token']").val();

                // Stores current element to get inside ajax callback
                let $el = this;

                // Stores ajax data to get slider name
                let $ajaxData = {
                    _token: $token,
                    slider: $sliderId
                }

                // Ajax request ot get slider name
                $.ajax({
                    method: "POST",
                    url: "/module/get-slider",
                    data: $ajaxData,

                    // Request callback
                    success: ($response) => {
                        // Parse response JSON data to Object
                        let $data = JSON.parse($response);

                        // Adds the recovered name to current looped elemente
                        $($el).text($data.name);
                    }
                })
            });

            // Prepare to alter furniture structure
            $(".edit-furniture-structure").click(function (e) {
                e.preventDefault();

                // Get environment and furniture target controls
                let $environmentTarget = $(this).attr("data-environment-target");
                let $furnitureTarget   = $(this).attr("data-furniture-target");

                // Gets CSRF token
                let $csrf = $("input[name='_token']").val();

                // Stores ajax data to prepare session ambient to change data
                $ajaxData = {
                    _token           : $csrf,
                    environmentTarget: $environmentTarget,
                    furnitureTarget  : $furnitureTarget
                };

                // Ajax request to prepare session ambient to change data
                $.ajax({
                    method: 'POST',
                    url: '/budget/prepare/structure',
                    data: $ajaxData,

                    // Request success callback
                    success: () => {
                        // Redirect the user to structure page
                        window.location.href = "{{ route('new-budget-structure') }}";
                    },

                    // Request error callback
                    error: ($xhr, $status, $error) => {
                        // Alert the user about the error
                        alert("Ocorreu um erro. Contate o administrador.");

                        // Debug the error
                        console.log({
                            xhrObject: $xhr,
                            status   : $status,
                            error    : $error
                        });
                    }
                });
            });

            // Prepare to alter furniture appearance
            $(".edit-furniture-appearance").click(function (e) {
                e.preventDefault();

                // Get environment and furniture target controls
                let $environmentTarget = $(this).attr("data-environment-target");
                let $furnitureTarget   = $(this).attr("data-furniture-target");

                // Gets CSRF token
                let $csrf = $("input[name='_token']").val();

                // Stores ajax data to prepare session ambient to change data
                $ajaxData = {
                    _token           : $csrf,
                    environmentTarget: $environmentTarget,
                    furnitureTarget  : $furnitureTarget
                };

                // Ajax request to prepare session ambient to change data
                $.ajax({
                    method: 'POST',
                    url: '/budget/prepare/appearance',
                    data: $ajaxData,

                    // Request success callback
                    success: () => {
                        // Redirect the user to structure page
                        window.location.href = "{{ route('new-budget-appearance') }}";
                    },

                    // Request error callback
                    error: ($xhr, $status, $error) => {
                        // Alert the user about the error
                        alert("Ocorreu um erro. Contate o administrador.");

                        // Debug the error
                        console.log({
                            xhrObject: $xhr,
                            status   : $status,
                            error    : $error
                        });
                    }
                });
            });

            // Open a modal to change furniture name
            $(".edit-furniture-name").click(function (e) {
                e.preventDefault();
                
                // Get targets info
                let $environmentTarget = $(this).attr("data-environment-target");
                let $furnitureTarget   = $(this).attr("data-furniture-target");

                // Open furniture name modal
                $("#furniture-name-modal").modal('toggle');

                // Set target attributes in modal OK button
                $("#change-furniture-name").attr("data-environment-target", $environmentTarget);
                $("#change-furniture-name").attr("data-furniture-target", $furnitureTarget);
            });

            // Change furniture name
            $("#change-furniture-name").click((e) => {
                e.preventDefault();

                // Stores element shortter
                let $this = "#change-furniture-name";

                // Get targets info
                let $environmentTarget = $($this).attr("data-environment-target");
                let $furnitureTarget   = $($this).attr("data-furniture-target");

                // Get new furniture name
                let $furnitureName = $("#furniture-name").val()

                // Verifies if the field was filled
                if ($furnitureName == undefined || $furnitureName.length == 0 || $furnitureName == null) {
                    // Alert the user to properly fill the field
                    alert("Por favor, informe um nome para o módulo");

                    // Ends the function
                    return;
                }

                // Gets CSRF token
                let $csrf = $("input[name='_token']").val();

                // Stores ajax data to update furniture name
                let $ajaxData = {
                    _token           : $csrf,
                    environmentTarget: $environmentTarget,
                    furnitureTarget  : $furnitureTarget,
                    furnitureName    : $furnitureName
                };

                // Ajax request to update furniture name
                $.ajax({
                    method: 'POST',
                    url: '/budget/update/furniture-name',
                    data: $ajaxData,

                    // Request success callback
                    success: () => {
                        // Change the furniture name loaded in page
                        $(`.edit-furniture-name[data-environment-target='${$environmentTarget}'][data-furniture-target='${$furnitureTarget}']`).parent().prev().prev().text($furnitureName + ":");

                        // Clear modal data
                        $($this).removeAttr("data-environment-target");
                        $($this).removeAttr("data-furniture-target");
                        $("#furniture-name").val("");

                        // Dismiss this modal
                        $("#furniture-name-modal").modal('toggle');
                    },

                    // Request error callback
                    error: ($xhr, $status, $error) => {
                        // Alert the user about the error
                        alert("Ocorreu um erro. Contate o administrador.");

                        // Debug the error
                        console.log({
                            xhrObject: $xhr,
                            status   : $status,
                            error    : $error
                        });
                    }
                });
            });

            // Prepare to add another furniture to environment
            $(".add-module").click(function (e) {
                e.preventDefault();

                // Get CSRF token
                let $csrf = $("input[name='_token']").val()

                // Get environment target
                let $environmentTarget = $(this).attr('data-environment-target');

                // Ajax data to get environments created
                let $ajaxEnvironmentsData = {
                    _token: $csrf
                }

                // Ajax request to get environments
                $.ajax({
                    method: 'POST',
                    url : '/budget/new/get',
                    data: $ajaxEnvironmentsData,

                    // Request callback
                    success: ($response) => {
                        // Parse JSON data in response
                        let $data = JSON.parse($response);

                        // Get the environment target new furniture index
                        let $nextFurniture = $data.environment[$environmentTarget].furnitures.length;

                        // Stores ajax data to prepare session ambient to add a furniture
                        let $ajaxPrepareFurnitureData = {
                            _token           : $csrf,
                            environmentTarget: $environmentTarget,
                            furnitureTarget  : $nextFurniture
                        };

                        // Ajax request to prepare session ambient to add a furniture
                        $.ajax({
                            method: 'POST',
                            url: '/budget/prepare/new-furniture',
                            data: $ajaxPrepareFurnitureData,

                            // Request success callback
                            success: () => {
                                // Redirect user to furniture screen
                                window.location.href = "{{ route('new-budget-furniture') }}";
                            },

                            // Request error callback
                            error: ($xhr, $status, $error) => {
                                // Alert the user about the error
                                alert("Ocorreu um erro. Contate o administrador.");

                                // Debug the error
                                console.log({
                                    xhrObject: $xhr,
                                    status   : $status,
                                    error    : $error
                                });
                            }
                        });
                    },

                    // Request error callback
                    error: ($xhr, $status, $error) => {
                        // Alert the user about the error
                        alert("Ocorreu um erro. Contate o administrador.");

                        // Debug the error
                        console.log({
                            xhrObject: $xhr,
                            status   : $status,
                            error    : $error
                        });
                    }
                });
            });

            // Prepare to add another furniture to environment
            $(".add-env").click((e) => {
                e.preventDefault();

                // Get CSRF token
                let $csrf = $("input[name='_token']").val();

                // Ajax data to get environments created
                let $ajaxEnvironmentsData = {
                    _token: $csrf
                }

                // Ajax request to get environments
                $.ajax({
                    method: 'POST',
                    url : '/budget/new/get',
                    data: $ajaxEnvironmentsData,

                    // Request callback
                    success: ($response) => {
                        // Parse JSON data in response
                        let $data = JSON.parse($response);

                        // Get next environment index
                        let $nextEnv = parseInt(Object.keys($data.environment)[Object.keys($data.environment).length - 1]) + 1;

                        // Ajax data to prepare session ambient to new environment
                        $ajaxPrepareEnvironmentData = {
                            _token: $csrf,
                            environmentTarget: $nextEnv
                        }

                        // Ajax request to prepare session ambient to new environment
                        $.ajax({
                            method: 'POST',
                            url: '/budget/prepare/new-environment',
                            data: $ajaxPrepareEnvironmentData,
                            
                            // Request callback
                            success: () => {
                                // Redirect user to environment setting screen
                                window.location.href = "{{ route('new-budget-initial') }}";
                            }
                        });
                    }
                });
            });
        });
    </script>
@endpush