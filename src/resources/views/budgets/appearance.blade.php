@extends('layouts.budgets.app')

@section('title', 'Aparência do Módulo')
@section('navbar-title', 'Novo Orçamento')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/appearance.css') }}">
    <link rel="stylesheet" href="{{ asset('css/components/modals.css')}}">
    <style>
        .page-divider-text {
            width: 100%;
            padding: 1.5rem;
            color: #0f4589;
            font-family: "Poppins", sans-serif;
            font-weight: 500;
            font-size: 1.5rem;
            text-transform: uppercase;
        }
    </style>
@endpush

@section('content')
    <!-- page_form_start -->
    <form action="{{ route('new-budget-appearance') }}" method="post">
        @csrf

        <!-- hidden_controls_start -->
        <input autocomplete="off" type="hidden" name="environment-target" value="{{ $environmentTarget }}">
        <input autocomplete="off" type="hidden" name="furniture-target" value="{{ $furnitureTarget }}">
        <input autocomplete="off" type="hidden" name="module-external-color" value="{{ $moduleExternalColor }}">
        <input autocomplete="off" type="hidden" name="module-external-color-code" value="{{ isset($moduleExternalCode) ? $moduleExternalCode : $moduleExternalColorCode }}">
        <input autocomplete="off" type="hidden" name="module-internal-color" value="{{ $moduleInternalColor }}">
        <input autocomplete="off" type="hidden" name="module-internal-color-code" value="{{ isset($moduleInternalCode) ? $moduleInternalCode : $moduleInternalColorCode }}">
        <input autocomplete="off" type="hidden" name="tape-external-color" value="{{ $tapeExternalColor }}">
        <input autocomplete="off" type="hidden" name="tape-external-color-code" value="{{ isset($tapeExternalCode) ? $tapeExternalCode : $tapeExternalColorCode }}">
        <input autocomplete="off" type="hidden" name="tape-internal-color" value="{{ $tapeInternalColor }}">
        <input autocomplete="off" type="hidden" name="tape-internal-color-code" value="{{ isset($tapeInternalCode) ? $tapeInternalCode : $tapeInternalColorCode }}">
        <input autocomplete="off" type="hidden" name="door-puller-name" value="{{ $doorPullerName }}">
        <input autocomplete="off" type="hidden" name="door-puller-color-name" value="{{ $doorPullerColorName }}">
        <input autocomplete="off" type="hidden" name="door-puller-color-code" @if(isset($doorPullerColorCode)) value="{{ $doorPullerColorCode }}" @endif>
        <input autocomplete="off" type="hidden" name="door-no-puller" @if(isset($doorNoPuller)) value="1" @endif>
        <input autocomplete="off" type="hidden" name="drawer-puller-name" value="{{ $drawerPullerName }}">
        <input autocomplete="off" type="hidden" name="drawer-puller-color-name" value="{{ $drawerPullerColorName }}">
        <input autocomplete="off" type="hidden" name="drawer-puller-color-code" @if(isset($drawerPullerColorCode)) value="{{ $drawerPullerColorCode }}" @endif>
        <input autocomplete="off" type="hidden" name="drawer-no-puller" @if(isset($drawerNoPuller)) value="1" @endif>
        <input autocomplete="off" type="hidden" name="slider-id" value="{{ $sliderId }}">
        <input autocomplete="off" type="hidden" name="shelve-support-id" value="{{ $supportId }}">
        <!-- hidden_controls_end -->

        <!-- content_wrapper_start -->
        <div class="container">
            <!-- page_name_wrapper_start -->
            <div class="row">
                <div class="col-12">
                    <!-- page_name_text_start -->
                    <h1 class="poppins font-extra-large text-uppercase">
                        <!-- text_pre_icon -->
                        <img src="{{ asset('assets/icons/project.png') }}" alt="projeto" width="24" height="24" class="mr-1">

                        <!-- text -->
                        Aparência
                    </h1>
                    <!-- page_name_text_end -->

                    <!-- page_name_sub_title_text -->
                    <span class="font-size-default poppins font-medium">Defina o VISUAL do Módulo:</span>
                </div>
            </div>
            <!-- page_name_wrapper_end -->

            <!-- colors_section_divider_wrapper_start -->
            <div class="row">
                <!-- colors_section -->
                <h2 class="page-divider-text">Cor</h2>
            </div>
            <!-- colors_section_divider_wrapper_end -->

            <!-- sub_section_header_start -->
            <div class="row">
                <div class="col-12 d-flex flex-column pl-3 poppins font-medium">
                    <!-- sub_section_header -->
                    <h3 class="font-size-default">Quais as CORES do Guarda-Roupas?</h3>

                    <!-- sub_section_subtitle -->
                    <span class="mt-1 text-muted font-small">Clique para selecionar as Cores dos Módulos.</span>
                </div>
            </div>
            <!-- sub_section_header_end -->

            <!-- module_color_selection_start -->
            <div class="row">
                <!-- module_internal_color_selection_start -->
                <div class="col p-2 white-box ml-2 text-center">
                    <!-- color_type -->
                    <h4 class="text-primary-gray poppins font-size-default font-medium">Cor EXTERIOR</h4>

                    <!-- color_viewer -->
                    <span class="color-viewer with-text" style="background: {{ isset($moduleExternalCode) ? $moduleExternalCode : $moduleExternalColorCode }}" data-color-name="{{ $moduleExternalColor }}"></span>

                    <!-- change_color_button -->
                    <a href="" class="change-link" id="change-external-color">Clique para alterar</a>
                </div>
                <!-- module_internal_color_selection_end -->

                <!-- module_internal_color_selection_start -->
                <div class="col p-2 white-box ml-2 text-center">
                    <!-- color_type -->
                    <h4 class="text-primary-gray poppins font-size-default font-medium">Cor INTERIOR</h4>

                    <!-- color_viewer -->
                    <span class="color-viewer with-text" style="background: {{ isset($moduleInternalCode) ? $moduleInternalCode : $moduleInternalColorCode }}" data-color-name="{{ $moduleInternalColor }}"></span>

                    <!-- change_color_button -->
                    <a href="" class="change-link" id="change-internal-color">Clique para alterar</a>
                </div>
                <!-- module_internal_color_selection_end -->
            </div>
            <!-- module_color_selection_end -->

            <!-- tape_color_selection_start -->
            <div class="row">
                <!-- tape_internal_color_selection_start -->
                <div class="col p-2 white-box ml-2 text-center">
                    <!-- color_type -->
                    <h4 class="text-primary-gray poppins font-size-default font-medium">Cor EXTERIOR da fita</h4>

                    <!-- color_viewer -->
                    <span class="color-viewer with-text" style="background: {{ isset($tapeExternalCode) ? $tapeExternalCode : $tapeExternalColorCode }}" data-color-name="{{ $tapeInternalColor }}"></span>

                    <!-- change_color_button -->
                    <a href="" class="change-link" id="change-tape-external-color">Clique para alterar</a>
                </div>
                <!-- tape_internal_color_selection_end -->

                <!-- tape_internal_color_selection_start -->
                <div class="col p-2 white-box ml-2 text-center">
                    <!-- color_type -->
                    <h4 class="text-primary-gray poppins font-size-default font-medium">Cor INTERIOR da fita</h4>

                    <!-- color_viewer -->
                    <span class="color-viewer with-text" style="background: {{ isset($tapeInternalCode) ? $tapeInternalCode : $tapeInternalColorCode }}" data-color-name="{{ $tapeInternalColor }}"></span>

                    <!-- change_color_button -->
                    <a href="" class="change-link" id="change-tape-internal-color">Clique para alterar</a>
                </div>
                <!-- tape_internal_color_selection_end -->
            </div>
            <!-- tape_color_selection_end -->

            <!-- ironwares_section_divider_wrapper_start -->
            <div class="row">
                <!-- colors_section -->
                <h2 class="page-divider-text">Ferragens</h2>
            </div>
            <!-- ironwares_section_divider_wrapper_end -->

            <!-- sub_section_header_start -->
            <div class="row">
                <div class="col-12 d-flex flex-column pl-3 poppins font-medium">
                    <!-- sub_section_header -->
                    <h3 class="font-size-default">Puxadores das PORTAS:</h3>
                </div>
            </div>
            <!-- sub_section_header_end -->

            <!-- doors_puller_type_start -->
            <div class="row white-box">
                <!-- data_wrapper_start -->
                <div class="col d-flex text-center py-3 flex-column justify-content-between">
                    <!-- puller_name -->
                    <span class="font-size-default poppins font-regular" id="door-puller-name">{{ $doorPullerName }}</span>

                    <!-- change_door_puller_button -->
                    <a href="" class="change-link" id="change-door-puller">Clique para alterar</a>
                </div>
                <!-- data_wrapper_end -->

                <!-- image_wrapper_start -->
                <div class="col py-1 d-flex flex-row justify-content-end">
                    <!-- puller_image -->
                    <img src="/{{ $doorPullerImage }}" alt="" class="border rounded" width="128" height="112" id="door-puller-image">
                </div>
                <!-- image_wrapper_end -->
            </div>
            <!-- doors_puller_type_end -->

            <!-- sub_section_header_start -->
            <div class="row mt-4">
                <div class="col-12 d-flex flex-column pl-3 poppins font-medium">
                    <!-- sub_section_header -->
                    <h3 class="font-size-default">COR do Puxador:</h3>
                </div>
            </div>
            <!-- sub_section_header_end -->

            <!-- doors_puller_color_start --->
            <div class="row white-box py-2">
                <!-- puller_image_wrapper_start -->
                <div class="col-3">
                    <!-- puller_image -->
                    <img src="{{ $doorPullerColorImage }}" alt="" width="72" height="56" class="border rounded" id="door-puller-color-image">
                </div>
                <!-- puller_image_wrapper_end -->

                <!-- puller_data_wrapper_start -->
                <div class="col d-flex flex-column pl-3">
                    <!-- puller_color_name -->
                    <span class="poppins font-size-default text-primary-gray font-medium" id="door-puller-color">{{ $doorPullerColorName }}</span>
                    
                    <!-- change_door_puller_button -->
                    <a href="" class="change-link" id="change-door-puller-color">Clique para alterar</a>
                </div>
                <!-- puller_data_wrapper_end -->
            </div>
            <!-- doors_puller_color_end --->

            <!-- sub_section_header_start -->
            <div class="row mt-3">
                <div class="col-12 d-flex flex-column pl-3 poppins font-medium">
                    <!-- sub_section_header -->
                    <h3 class="font-size-default pt-3 border-top">Puxadores das GAVETAS:</h3>
                </div>
            </div>
            <!-- sub_section_header_end -->

            <!-- drawers_puller_type_start -->
            <div class="row white-box">
                <!-- data_wrapper_start -->
                <div class="col d-flex text-center py-3 flex-column justify-content-between">
                    <!-- puller_name -->
                    <span class="font-size-default poppins font-regular" id="drawer-puller-name">{{ $drawerPullerName }}</span>

                    <!-- change_drawer_puller_button -->
                    <a href="" class="change-link" id="change-drawer-puller">Clique para alterar</a>
                </div>
                <!-- data_wrapper_end -->

                <!-- image_wrapper_start -->
                <div class="col py-1 d-flex flex-row justify-content-end">
                    <!-- puller_image -->
                    <img src="/{{ $drawerPullerImage }}" alt="" class="border rounded" width="128" height="112" id="drawer-puller-image">
                </div>
                <!-- image_wrapper_end -->
            </div>
            <!-- drawers_puller_type_end -->

            <!-- sub_section_header_start -->
            <div class="row mt-4">
                <div class="col-12 d-flex flex-column pl-3 poppins font-medium">
                    <!-- sub_section_header -->
                    <h3 class="font-size-default">COR do Puxador:</h3>
                </div>
            </div>
            <!-- sub_section_header_end -->

            <!-- drawers_puller_color_start --->
            <div class="row white-box py-2">
                <!-- puller_image_wrapper_start -->
                <div class="col-3">
                    <!-- puller_image -->
                    <img src="{{ $drawerPullerColorImage }}" alt="" width="72" height="56" class="border rounded" id="drawer-puller-color-image">
                </div>
                <!-- puller_image_wrapper_end -->

                <!-- puller_data_wrapper_start -->
                <div class="col d-flex flex-column pl-3">
                    <!-- puller_color_name -->
                    <span class="poppins font-size-default text-primary-gray font-medium" id="drawer-puller-color">{{ $drawerPullerColorName }}</span>
                    
                    <!-- change_drawer_puller_button -->
                    <a href="" class="change-link" id="change-drawer-puller-color">Clique para alterar</a>
                </div>
                <!-- puller_data_wrapper_end -->
            </div>
            <!-- drawers_puller_color_end --->

            <!-- sub_section_header_start -->
            <div class="row mt-3">
                <div class="col-12 d-flex flex-column pl-3 poppins font-medium">
                    <!-- sub_section_header -->
                    <h3 class="font-size-default pt-3 border-top">Escolha o Tipo de CORREDIÇA:</h3>
                </div>
            </div>
            <!-- sub_section_header_end -->

            <!-- slider_type_start -->
            <div class="row white-box mt-3">
                <!-- data_wrapper_start -->
                <div class="col d-flex text-center py-3 flex-column justify-content-between">
                    <!-- slider_name -->
                    <span class="font-size-default poppins font-regular" id="slider-name">{{ $sliderData->name }}</span>

                    <!-- change_slider_button -->
                    <a href="" class="change-link" id="change-slider">Clique para alterar</a>
                </div>
                <!-- data_wrapper_end -->

                <!-- image_wrapper_start -->
                <div class="col py-1 d-flex flex-row justify-content-end">
                    <!-- slider_image -->
                    <img src="/{{ $sliderData->image }}" alt="" class="border rounded" width="128" height="112" id="slider-image">
                </div>
                <!-- image_wrapper_end -->
            </div>
            <!-- slider_type_end -->

            <!-- sub_section_header_start -->
            <div class="row mt-3">
                <div class="col-12 d-flex flex-column pl-3 poppins font-medium">
                    <!-- sub_section_header -->
                    <h3 class="font-size-default pt-3 border-top">Escolha qual o SUPORTE das Prateleiras:</h3>
                </div>
            </div>
            <!-- sub_section_header_end -->

            <!-- support_type_start -->
            <div class="row white-box mt-3">
                <!-- data_wrapper_start -->
                <div class="col d-flex text-center py-3 flex-column justify-content-between">
                    <!-- support_name -->
                    <span class="font-size-default poppins font-regular" id="shelves-support-name">{{ $supportData->name }}</span>

                    <!-- change_support_button -->
                    <a href="" class="change-link" id="change-support">Clique para alterar</a>
                </div>
                <!-- data_wrapper_end -->

                <!-- image_wrapper_start -->
                <div class="col py-1 d-flex flex-row justify-content-end">
                    <!-- slider_image -->
                    <img src="/{{ $supportData->image }}" alt="" class="border rounded" width="128" height="112" id="support-image">
                </div>
                <!-- image_wrapper_end -->
            </div>
            <!-- support_type_end -->

            <!-- send_button_wrapper_start -->
            <div class="row mt-4">
                <div class="col-12 pr-3 d-flex flex-row justify-content-end pb-3">
                    <!-- send_button -->
                    <button class="btn btn-primary" type="submit" id="send-button">Continuar</button>
                </div>
            </div>
            <!-- send_button_wrapper_end -->
        </div>
        <!-- content_wrapper_end -->

        <!-- module_internal_color_modal_start -->
        <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="module-internal-color-modal">
            <div class="modal-dialog" role="document">
                <!-- modal_content_wrapper_start -->
                <div class="modal-content">
                    <!-- modal_body_start -->
                    <div class="modal-body">
                        <!-- modal_breadcrumb_start -->
                        <div class="modal-breadcrumb">
                            <!-- breadcrumb_title -->
                            <h3 class="font-size-default">Selecione a Cor INTERNA do Módulo:</h3>
                        </div>
                        <!-- modal_breadcrumb_end -->

                        <!-- modal_body_content_wrapper_start -->
                        <div class="container">
                            <div class="row">
                                @foreach($colors as $color)
                                    <div class="ml-2 white-box d-flex flex-column internal-module-color" style="width: 47% !important">
                                        <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $color->name }}</h3>
                                        <span class="color-viewer" style="background: {{ $color->code }}"></span>
                                    </div>
                                @endforeach
                            </div>

                            <div class="row mt-4 white-box">
                                <div class="col-12 d-flex flex-column pl-3 pb-3 pt-2 poppins">
                                    <h3 class="font-size-default font-medium text-primary-gray pl-2">Outra COR</h3>

                                    <div class="d-flex flex-row row">
                                        <div class="col-5">
                                            <label for="module-internal-custom-name" class="font-small font-light">Nome da COR</label>
                                        </div>
                                        <div class="col-5">
                                            <label for="module-internal-custom-code" class="font-small font-light">Código da COR</label>
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row">
                                        <div class="col-5 px-0">
                                            <input type="text" class="form-control" id="module-internal-custom-name" placeholder="Digite o Nome" autocomplete="off">
                                        </div>
                                        <div class="col-5 pr-0 pl-1">
                                            <input type="text" class="form-control" id="module-internal-custom-code" placeholder="Ex: 122F6E" autocomplete="off">
                                        </div>
                                        <div class="col-2 pr-0 pl-1">
                                            <button class="btn btn-primary" id="custom-module-internal-color-save" style="width: auto; height: auto">IR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal_body_content_wrapper_end -->
                    </div>
                    <!-- modal_body_end -->
                </div>
                <!-- modal_content_wrapper_end -->
            </div>
        </div>
        <!-- module_internal_color_modal_end -->

        <!-- module_external_color_modal_start -->
        <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="module-external-color-modal">
            <div class="modal-dialog" role="document">
                <!-- modal_content_wrapper_start -->
                <div class="modal-content">
                    <!-- modal_body_start -->
                    <div class="modal-body">
                        <!-- modal_breadcrumb_start -->
                        <div class="modal-breadcrumb">
                            <!-- breadcrumb_title -->
                            <h3 class="font-size-default">Selecione a Cor EXTERNA do Módulo:</h3>
                        </div>
                        <!-- modal_breadcrumb_end -->

                        <!-- modal_body_content_wrapper_start -->
                        <div class="container">
                            <div class="row">
                                @foreach($colors as $color)
                                    <div class="ml-2 white-box d-flex flex-column external-module-color" style="width: 47% !important">
                                        <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $color->name }}</h3>
                                        <span class="color-viewer" style="background: {{ $color->code }}"></span>
                                    </div>
                                @endforeach
                            </div>

                            <div class="row mt-4 white-box">
                                <div class="col-12 d-flex flex-column pl-3 pb-3 pt-2 poppins">
                                    <h3 class="font-size-default font-medium text-primary-gray pl-2">Outra COR</h3>

                                    <div class="d-flex flex-row row">
                                        <div class="col-5">
                                            <label for="module-external-custom-name" class="font-small font-light">Nome da COR</label>
                                        </div>
                                        <div class="col-5">
                                            <label for="module-external-custom-code" class="font-small font-light">Código da COR</label>
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row">
                                        <div class="col-5 px-0">
                                            <input type="text" class="form-control" id="module-external-custom-name" placeholder="Digite o Nome" autocomplete="off">
                                        </div>
                                        <div class="col-5 pr-0 pl-1">
                                            <input type="text" class="form-control" id="module-external-custom-code" placeholder="Ex: 122F6E" autocomplete="off">
                                        </div>
                                        <div class="col-2 pr-0 pl-1">
                                            <button class="btn btn-primary" id="custom-module-external-color-save" style="width: auto; height: auto">IR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal_body_content_wrapper_end -->
                    </div>
                    <!-- modal_body_end -->
                </div>
                <!-- modal_content_wrapper_end -->
            </div>
        </div>
        <!-- module_external_color_modal_end -->

        <!-- tape_external_color_modal_start -->
        <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="tape-external-color-modal">
            <div class="modal-dialog" role="document">
                <!-- modal_content_wrapper_start -->
                <div class="modal-content">
                    <!-- modal_body_start -->
                    <div class="modal-body">
                        <!-- modal_breadcrumb_start -->
                        <div class="modal-breadcrumb">
                            <!-- breadcrumb_title -->
                            <h3 class="font-size-default">Selecione a Cor EXTERNA das Fitas:</h3>
                        </div>
                        <!-- modal_breadcrumb_end -->

                        <!-- modal_body_content_wrapper_start -->
                        <div class="container">
                            <div class="row">
                                @foreach($colors as $color)
                                    <div class="ml-2 white-box d-flex flex-column external-tape-color" style="width: 47% !important">
                                        <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $color->name }}</h3>
                                        <span class="color-viewer" style="background: {{ $color->code }}"></span>
                                    </div>
                                @endforeach
                            </div>

                            <div class="row mt-4 white-box">
                                <div class="col-12 d-flex flex-column pl-3 pb-3 pt-2 poppins">
                                    <h3 class="font-size-default font-medium text-primary-gray pl-2">Outra COR</h3>

                                    <div class="d-flex flex-row row">
                                        <div class="col-5">
                                            <label for="tape-external-custom-name" class="font-small font-light">Nome da COR</label>
                                        </div>
                                        <div class="col-5">
                                            <label for="tape-external-custom-code" class="font-small font-light">Código da COR</label>
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row">
                                        <div class="col-5 px-0">
                                            <input type="text" class="form-control" id="tape-external-custom-name" placeholder="Digite o Nome" autocomplete="off">
                                        </div>
                                        <div class="col-5 pr-0 pl-1">
                                            <input type="text" class="form-control" id="tape-external-custom-code" placeholder="Ex: 122F6E" autocomplete="off">
                                        </div>
                                        <div class="col-2 pr-0 pl-1">
                                            <button class="btn btn-primary" id="custom-tape-external-color-save" style="width: auto; height: auto">IR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal_body_content_wrapper_end -->
                    </div>
                    <!-- modal_body_end -->
                </div>
                <!-- modal_content_wrapper_end -->
            </div>
        </div>
        <!-- tape_external_color_modal_end -->

        <!-- tape_external_color_modal_start -->
        <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="tape-internal-color-modal">
            <div class="modal-dialog" role="document">
                <!-- modal_content_wrapper_start -->
                <div class="modal-content">
                    <!-- modal_body_start -->
                    <div class="modal-body">
                        <!-- modal_breadcrumb_start -->
                        <div class="modal-breadcrumb">
                            <!-- breadcrumb_title -->
                            <h3 class="font-size-default">Selecione a Cor EXTERNA das Fitas:</h3>
                        </div>
                        <!-- modal_breadcrumb_end -->

                        <!-- modal_body_content_wrapper_start -->
                        <div class="container">
                            <div class="row">
                                @foreach($colors as $color)
                                    <div class="ml-2 white-box d-flex flex-column internal-tape-color" style="width: 47% !important">
                                        <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $color->name }}</h3>
                                        <span class="color-viewer" style="background: {{ $color->code }}"></span>
                                    </div>
                                @endforeach
                            </div>

                            <div class="row mt-4 white-box">
                                <div class="col-12 d-flex flex-column pl-3 pb-3 pt-2 poppins">
                                    <h3 class="font-size-default font-medium text-primary-gray pl-2">Outra COR</h3>

                                    <div class="d-flex flex-row row">
                                        <div class="col-5">
                                            <label for="tape-internal-custom-name" class="font-small font-light">Nome da COR</label>
                                        </div>
                                        <div class="col-5">
                                            <label for="tape-internal-custom-code" class="font-small font-light">Código da COR</label>
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row">
                                        <div class="col-5 px-0">
                                            <input type="text" class="form-control" id="tape-internal-custom-name" placeholder="Digite o Nome" autocomplete="off">
                                        </div>
                                        <div class="col-5 pr-0 pl-1">
                                            <input type="text" class="form-control" id="tape-internal-custom-code" placeholder="Ex: 122F6E" autocomplete="off">
                                        </div>
                                        <div class="col-2 pr-0 pl-1">
                                            <button class="btn btn-primary" id="custom-tape-internal-color-save" style="width: auto; height: auto">IR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal_body_content_wrapper_end -->
                    </div>
                    <!-- modal_body_end -->
                </div>
                <!-- modal_content_wrapper_end -->
            </div>
        </div>
        <!-- tape_external_color_modal_end -->

        <!-- door_puller_type_modal_start -->
        <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="door-puller-type-modal">
            <div class="modal-dialog" role="document">
                <!-- modal_content_wrapper_start -->
                <div class="modal-content">
                    <!-- modal_body_start -->
                    <div class="modal-body" style="max-height: 75vh; overflow-y: auto">
                        <!-- modal_breadcrumb_start -->
                        <div class="modal-breadcrumb">
                            <!-- breadcrumb_title -->
                            <h3 class="font-size-default">Selecione o Puxador para PORTAS:</h3>
                        </div>
                        <!-- modal_breadcrumb_end -->

                        <!-- modal_body_content_wrapper_start -->
                        <div class="container">
                            <div class="row">
                                <div class="ml-2 white-box d-flex flex-column door-no-puller" style="width: 47% !important">
                                    <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">Nenhum</h3>
                                    <img src="{{ asset('uploads/generic/no-puller.png') }}" height="104" width="131" class="mx-2 rounded border">
                                    <span class="font-small mt-2 text-secondary-gray pl-2 pb-2">Sem Puxador</span>
                                </div>
                                @foreach($pullers as $puller)
                                    <div class="ml-2 white-box d-flex flex-column door-puller-select" style="width: 47% !important">
                                        <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $puller->name }}</h3>
                                        <img src="/{{ $puller->image }}" height="104" width="131" class="mx-2 rounded border">
                                        <span class="font-small mt-2 text-secondary-gray pl-2 pb-2">{{ $puller->alias }}</span>
                                    </div>
                                @endforeach
                            </div>

                            <div class="row mt-4 white-box">
                                <div class="col-12 d-flex flex-column pl-3 pb-3 pt-2 poppins">
                                    <h3 class="font-size-default font-medium text-primary-gray pl-2">Outro Tipo de Puxador</h3>

                                    <div class="d-flex flex-row row">
                                        <div class="col-5">
                                            <label for="door-puller-type-custom-name" class="font-small font-light">Nome</label>
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row">
                                        <div class="col-5 px-0">
                                            <input type="text" class="form-control" id="door-puller-type-custom-name" placeholder="Digite o Nome" autocomplete="off">
                                        </div>
                                        <div class="col-2 pr-0 pl-1">
                                            <button class="btn btn-primary" id="custom-door-puller-type-save" style="width: auto; height: auto">IR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal_body_content_wrapper_end -->
                    </div>
                    <!-- modal_body_end -->
                </div>
                <!-- modal_content_wrapper_end -->
            </div>
        </div>
        <!-- door_puller_type_modal_end -->

        <!-- door_puller_color_modal_start -->
        <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="door-puller-color-modal">
            <div class="modal-dialog" role="document">
                <!-- modal_content_wrapper_start -->
                <div class="modal-content">
                    <!-- modal_body_start -->
                    <div class="modal-body" style="max-height: 75vh; overflow-y: auto">
                        <!-- modal_breadcrumb_start -->
                        <div class="modal-breadcrumb">
                            <!-- breadcrumb_title -->
                            <h3 class="font-size-default">Selecione a COR do Puxador:</h3>
                        </div>
                        <!-- modal_breadcrumb_end -->

                        <!-- modal_body_content_wrapper_start -->
                        <div class="container">
                            <div class="row">
                                @foreach($pullerColors as $puller)
                                    <div class="ml-2 white-box d-flex flex-column door-puller-color-select" style="width: 47% !important">
                                        <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $puller->name }}</h3>
                                        <img src="/{{ $puller->image }}" height="104" width="131" class="mx-2 rounded border">
                                    </div>
                                @endforeach
                            </div>

                            <div class="row mt-4 white-box">
                                <div class="col-12 d-flex flex-column pl-3 pb-3 pt-2 poppins">
                                    <h3 class="font-size-default font-medium text-primary-gray pl-2">Outra Cor</h3>

                                    <div class="d-flex flex-row row">
                                        <div class="col-5">
                                            <label for="#door-puller-color-custom-name" class="font-small font-light">Nome da COR</label>
                                        </div>
                                        <div class="col-5">
                                            <label for="#door-puller-color-custom-code" class="font-small font-light">Código da COR</label>
                                        </div>
                                    </div>
    
                                    <div class="d-flex flex-row">
                                        <div class="col-5 px-0">
                                            <input type="text" class="form-control" id="door-puller-color-custom-name" placeholder="Digite o Nome" autocomplete="off">
                                        </div>
                                        <div class="col-5 pr-0 pl-1">
                                            <input type="text" class="form-control" id="door-puller-color-custom-code" placeholder="Ex: 122F6E" autocomplete="off">
                                        </div>
                                        <div class="col-2 pr-0 pl-1">
                                            <button class="btn btn-primary" id="custom-door-puller-color-save" style="width: auto; height: auto">IR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal_body_content_wrapper_end -->
                    </div>
                    <!-- modal_body_end -->
                </div>
                <!-- modal_content_wrapper_end -->
            </div>
        </div>
        <!-- door_puller_color_modal_end -->

        <!-- door_puller_type_modal_start -->
        <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="drawer-puller-type-modal">
            <div class="modal-dialog" role="document">
                <!-- modal_content_wrapper_start -->
                <div class="modal-content">
                    <!-- modal_body_start -->
                    <div class="modal-body" style="max-height: 75vh; overflow-y: auto">
                        <!-- modal_breadcrumb_start -->
                        <div class="modal-breadcrumb">
                            <!-- breadcrumb_title -->
                            <h3 class="font-size-default">Selecione o Puxador para PORTAS:</h3>
                        </div>
                        <!-- modal_breadcrumb_end -->

                        <!-- modal_body_content_wrapper_start -->
                        <div class="container">
                            <div class="row">
                                <div class="ml-2 white-box d-flex flex-column drawer-no-puller" style="width: 47% !important">
                                    <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">Nenhum</h3>
                                    <img src="{{ asset('uploads/generic/no-puller.png') }}" height="104" width="131" class="mx-2 rounded border">
                                    <span class="font-small mt-2 text-secondary-gray pl-2 pb-2">Sem Puxador</span>
                                </div>
                                @foreach($pullers as $puller)
                                    <div class="ml-2 white-box d-flex flex-column drawer-puller-select" style="width: 47% !important">
                                        <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $puller->name }}</h3>
                                        <img src="/{{ $puller->image }}" height="104" width="131" class="mx-2 rounded border">
                                        <span class="font-small mt-2 text-secondary-gray pl-2 pb-2">{{ $puller->alias }}</span>
                                    </div>
                                @endforeach
                            </div>

                            <div class="row mt-4 white-box">
                                <div class="col-12 d-flex flex-column pl-3 pb-3 pt-2 poppins">
                                    <h3 class="font-size-default font-medium text-primary-gray pl-2">Outro Tipo de Puxador</h3>

                                    <div class="d-flex flex-row row">
                                        <div class="col-5">
                                            <label for="drawer-puller-type-custom-name" class="font-small font-light">Nome</label>
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row">
                                        <div class="col-5 px-0">
                                            <input type="text" class="form-control" id="drawer-puller-type-custom-name" placeholder="Digite o Nome" autocomplete="off">
                                        </div>
                                        <div class="col-2 pr-0 pl-1">
                                            <button class="btn btn-primary" id="custom-drawer-puller-type-save" style="width: auto; height: auto">IR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal_body_content_wrapper_end -->
                    </div>
                    <!-- modal_body_end -->
                </div>
                <!-- modal_content_wrapper_end -->
            </div>
        </div>
        <!-- drawer_puller_type_modal_end -->

        <!-- drawer_puller_color_modal_start -->
        <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="drawer-puller-color-modal">
            <div class="modal-dialog" role="document">
                <!-- modal_content_wrapper_start -->
                <div class="modal-content">
                    <!-- modal_body_start -->
                    <div class="modal-body" style="max-height: 75vh; overflow-y: auto">
                        <!-- modal_breadcrumb_start -->
                        <div class="modal-breadcrumb">
                            <!-- breadcrumb_title -->
                            <h3 class="font-size-default">Selecione a COR do Puxador:</h3>
                        </div>
                        <!-- modal_breadcrumb_end -->

                        <!-- modal_body_content_wrapper_start -->
                        <div class="container">
                            <div class="row">
                                @foreach($pullerColors as $puller)
                                    <div class="ml-2 white-box d-flex flex-column drawer-puller-color-select" style="width: 47% !important">
                                        <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $puller->name }}</h3>
                                        <img src="/{{ $puller->image }}" height="104" width="131" class="mx-2 rounded border">
                                    </div>
                                @endforeach
                            </div>

                            <div class="row mt-4 white-box">
                                <div class="col-12 d-flex flex-column pl-3 pb-3 pt-2 poppins">
                                    <h3 class="font-size-default font-medium text-primary-gray pl-2">Outra Cor</h3>

                                    <div class="d-flex flex-row row">
                                        <div class="col-5">
                                            <label for="#drawer-puller-color-custom-name" class="font-small font-light">Nome da COR</label>
                                        </div>
                                        <div class="col-5">
                                            <label for="#drawer-puller-color-custom-code" class="font-small font-light">Código da COR</label>
                                        </div>
                                    </div>
    
                                    <div class="d-flex flex-row">
                                        <div class="col-5 px-0">
                                            <input type="text" class="form-control" id="drawer-puller-color-custom-name" placeholder="Digite o Nome" autocomplete="off">
                                        </div>
                                        <div class="col-5 pr-0 pl-1">
                                            <input type="text" class="form-control" id="drawer-puller-color-custom-code" placeholder="Ex: 122F6E" autocomplete="off">
                                        </div>
                                        <div class="col-2 pr-0 pl-1">
                                            <button class="btn btn-primary" id="custom-drawer-puller-color-save" style="width: auto; height: auto">IR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal_body_content_wrapper_end -->
                    </div>
                    <!-- modal_body_end -->
                </div>
                <!-- modal_content_wrapper_end -->
            </div>
        </div>
        <!-- drawer_puller_color_modal_end -->

        <!-- sliders_modal_start -->
        <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="sliders-modal">
            <div class="modal-dialog" role="document">
                <!-- modal_content_wrapper_start -->
                <div class="modal-content">
                    <!-- modal_body_start -->
                    <div class="modal-body">
                        <!-- modal_breadcrumb_start -->
                        <div class="modal-breadcrumb">
                            <!-- breadcrumb_title -->
                            <h3 class="font-size-default">Selecione o Tipo de CORREDIÇA:</h3>
                        </div>
                        <!-- modal_breadcrumb_end -->

                        <!-- modal_body_content_wrapper_start -->
                        <div class="container">
                            <div class="row">
                                <!-- sliders_type_section_divider_wrapper_start -->
                                <div class="row w-100">
                                    <!-- sliders_type_section -->
                                    <h2 class="page-divider-text">Telescópica</h2>
                                </div>
                                <br>
                                <!-- sliders_type_section_divider_wrapper_end -->
                                @foreach($sliders["Telescópica"] as $slider)
                                    <div class="ml-2 white-box d-flex flex-column slider-selection" style="width: 47% !important" data-id="{{ $slider->id }}">
                                        <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $slider->name }}</h3>
                                        <img src="/{{ $slider->image }}" height="104" width="131" class="mx-2 rounded border">
                                        <span class="font-small mt-2 text-secondary-gray pl-2 pb-2">{{ $slider->alias }}</span>
                                    </div>
                                @endforeach

                                <!-- sliders_type_section_divider_wrapper_start -->
                                <div class="row w-100">
                                    <!-- sliders_type_section -->
                                    <h2 class="page-divider-text d-block">Invisível</h2>
                                </div>
  
                                <!-- sliders_type_section_divider_wrapper_end -->
                                @foreach($sliders["Invisível"] as $slider)
                                    <div class="ml-2 white-box d-flex flex-column slider-selection" style="width: 47% !important" data-id="{{ $slider->id }}">
                                        <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $slider->name }}</h3>
                                        <img src="/{{ $slider->image }}" height="104" width="131" class="mx-2 rounded border">
                                        <span class="font-small mt-2 text-secondary-gray pl-2 pb-2">{{ $slider->alias }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- modal_body_content_wrapper_end -->
                    </div>
                    <!-- modal_body_end -->
                </div>
                <!-- modal_content_wrapper_end -->
            </div>
        </div>
        <!-- sliders_modal_end -->

        <!-- support_modal_start -->
        <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="supports-modal">
            <div class="modal-dialog" role="document">
                <!-- modal_content_wrapper_start -->
                <div class="modal-content">
                    <!-- modal_body_start -->
                    <div class="modal-body">
                        <!-- modal_breadcrumb_start -->
                        <div class="modal-breadcrumb">
                            <!-- breadcrumb_title -->
                            <h3 class="font-size-default">Selecione o SUPORTE das Prateleiras:</h3>
                        </div>
                        <!-- modal_breadcrumb_end -->

                        <!-- modal_body_content_wrapper_start -->
                        <div class="container">
                            <div class="row">
                                @foreach($supports as $support)
                                    <div class="ml-2 white-box d-flex flex-column support-selection" style="width: 47% !important" data-id="{{ $support->id }}">
                                        <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $support->name }}</h3>
                                        <img src="/{{ $support->image }}" height="104" width="131" class="mx-2 rounded border">
                                    <span class="font-small mt-2 text-secondary-gray pl-2 pb-2">{{ $support->alias }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- modal_body_content_wrapper_end -->
                    </div>
                    <!-- modal_body_end -->
                </div>
                <!-- modal_content_wrapper_end -->
            </div>
        </div>
        <!-- support_modal_end -->
    </form>
@endsection

@push('js')
    <script type="text/javascript">
        function rgba2hex(rgb) {
            if (  rgb.search("rgb") == -1 ) {
                return rgb;
            }
            else if ( rgb == 'rgba(0, 0, 0, 0)' ) {
                return 'transparent';
            }
            else {
                rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
                function hex(x) {
                    return ("0" + parseInt(x).toString(16)).slice(-2);
                }
                return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]); 
            }
        }

        $(document).ready(() => {
            // Open module internal color selection
            $("#change-internal-color").click((e) => {
                e.preventDefault(); 
                $("#module-internal-color-modal").modal('toggle');
            });

            $(".internal-module-color").click(function (e) {
                e.preventDefault();

                // Get color data
                let $name  = $(this).find("h3").text();
                let $color = rgba2hex($(this).find("span").css("background-color"));

                console.log($color);

                // Adds the color name and code to his viewer
                $("#change-internal-color").prev().css({backgroundColor: $color});
                $("#change-internal-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='module-internal-color']").val($name);
                $("input[name='module-internal-color-code']").val($color);

                // Dispense this modal
                $("#module-internal-color-modal").modal('toggle');
            });

            // Save custom internal module color
            $("#custom-module-internal-color-save").click((e) => {
                e.preventDefault();

                // Get color values
                let $name  = $("#module-internal-custom-name").val();
                let $color = $("#module-internal-custom-code").val();

                // Validates the values
                if ($name == undefined || $name.length == 0 || $name == null || $color == undefined || $color.length == 0 || $color == null) {
                    // Alert the user to fill properly the fields
                    alert("Por favor, informe o nome e código da cor para adiciona-lá.");

                    // Ends function
                    return;
                }

                // Verifies if the color code has # char
                if ($color.indexOf("#") == -1) {
                    // Adds the # char to $color
                    $color = "#" + $color;
                }

                // Adds the color name and code to his viewer
                $("#change-internal-color").prev().css("background", $color);
                $("#change-internal-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='module-internal-color']").val($name);
                $("input[name='module-internal-color-code']").val($color);

                // Dispense this modal
                $("#module-internal-color-modal").modal('toggle');
            });

            // Open module external color selection
            $("#change-external-color").click((e) => {
                e.preventDefault(); 
                $("#module-external-color-modal").modal('toggle');
            });

            $(".external-module-color").click(function (e) {
                e.preventDefault();

                // Get color data
                let $name  = $(this).find("h3").text();
                let $color = rgba2hex($(this).find("span").css("background-color"));

                console.log($color);

                // Adds the color name and code to his viewer
                $("#change-external-color").prev().css({backgroundColor: $color});
                $("#change-external-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='module-external-color']").val($name);
                $("input[name='module-external-color-code']").val($color);

                // Dispense this modal
                $("#module-external-color-modal").modal('toggle');
            });

            // Save custom external module color
            $("#custom-module-external-color-save").click((e) => {
                e.preventDefault();

                // Get color values
                let $name  = $("#module-external-custom-name").val();
                let $color = $("#module-external-custom-code").val();

                // Validates the values
                if ($name == undefined || $name.length == 0 || $name == null || $color == undefined || $color.length == 0 || $color == null) {
                    // Alert the user to fill properly the fields
                    alert("Por favor, informe o nome e código da cor para adiciona-lá.");

                    // Ends function
                    return;
                }

                // Verifies if the color code has # char
                if ($color.indexOf("#") == -1) {
                    // Adds the # char to $color
                    $color = "#" + $color;
                }

                // Adds the color name and code to his viewer
                $("#change-external-color").prev().css("background", $color);
                $("#change-external-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='module-external-color']").val($name);
                $("input[name='module-external-color-code']").val($color);

                // Dispense this modal
                $("#module-external-color-modal").modal('toggle');
            });

            // Open tape external color selection
            $("#change-tape-external-color").click((e) => {
                e.preventDefault(); 
                $("#tape-external-color-modal").modal('toggle');
            });

            $(".external-tape-color").click(function (e) {
                e.preventDefault();

                // Get color data
                let $name  = $(this).find("h3").text();
                let $color = rgba2hex($(this).find("span").css("background-color"));

                console.log($color);

                // Adds the color name and code to his viewer
                $("#change-tape-external-color").prev().css({backgroundColor: $color});
                $("#change-tape-external-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='tape-external-color']").val($name);
                $("input[name='tape-external-color-code']").val($color);

                // Dispense this modal
                $("#tape-external-color-modal").modal('toggle');
            });

            // Save custom external tape color
            $("#custom-tape-external-color-save").click((e) => {
                e.preventDefault();

                // Get color values
                let $name  = $("#tape-external-custom-name").val();
                let $color = $("#tape-external-custom-code").val();

                // Validates the values
                if ($name == undefined || $name.length == 0 || $name == null || $color == undefined || $color.length == 0 || $color == null) {
                    // Alert the user to fill properly the fields
                    alert("Por favor, informe o nome e código da cor para adiciona-lá.");

                    // Ends function
                    return;
                }

                // Verifies if the color code has # char
                if ($color.indexOf("#") == -1) {
                    // Adds the # char to $color
                    $color = "#" + $color;
                }

                // Adds the color name and code to his viewer
                $("#change-tape-external-color").prev().css("background", $color);
                $("#change-tape-external-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='tape-external-color']").val($name);
                $("input[name='tape-external-color-code']").val($color);

                // Dispense this modal
                $("#tape-external-color-modal").modal('toggle');
            });

            // Open tape internal color selection
            $("#change-tape-internal-color").click((e) => {
                e.preventDefault(); 
                $("#tape-internal-color-modal").modal('toggle');
            });

            $(".internal-tape-color").click(function (e) {
                e.preventDefault();

                // Get color data
                let $name  = $(this).find("h3").text();
                let $color = rgba2hex($(this).find("span").css("background-color"));

                console.log($color);

                // Adds the color name and code to his viewer
                $("#change-tape-internal-color").prev().css({backgroundColor: $color});
                $("#change-tape-internal-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='tape-internal-color']").val($name);
                $("input[name='tape-internal-color-code']").val($color);

                // Dispense this modal
                $("#tape-internal-color-modal").modal('toggle');
            });

            // Save custom internal tape color
            $("#custom-tape-internal-color-save").click((e) => {
                e.preventDefault();

                // Get color values
                let $name  = $("#tape-internal-custom-name").val();
                let $color = $("#tape-internal-custom-code").val();

                // Validates the values
                if ($name == undefined || $name.length == 0 || $name == null || $color == undefined || $color.length == 0 || $color == null) {
                    // Alert the user to fill properly the fields
                    alert("Por favor, informe o nome e código da cor para adiciona-lá.");

                    // Ends function
                    return;
                }

                // Verifies if the color code has # char
                if ($color.indexOf("#") == -1) {
                    // Adds the # char to $color
                    $color = "#" + $color;
                }

                // Adds the color name and code to his viewer
                $("#change-tape-internal-color").prev().css("background", $color);
                $("#change-tape-internal-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='tape-internal-color']").val($name);
                $("input[name='tape-internal-color-code']").val($color);

                // Dispense this modal
                $("#tape-internal-color-modal").modal('toggle');
            });

            // Open sliders selection modal
            $("#change-slider").click((e) => {
                e.preventDefault();
                $("#sliders-modal").modal('toggle');
            });

            $(".slider-selection").click(function (e) {
                e.preventDefault();

                // Get slider data
                let $id    = $(this).attr("data-id");
                let $name  = $(this).find('h3').text();
                let $image = $(this).find('img').attr('src');

                // Adds slider data to form
                $("input[name='slider-id']").val($id);
                $("#slider-name").text($name);
                $("#slider-image").attr('src', $image);

                // Dismiss this modal
                $("#sliders-modal").modal('toggle');
            });

            // Open shelves supports selection modal
            $("#change-support").click((e) => {
                e.preventDefault();
                $("#supports-modal").modal('toggle');
            });

            // Select a new shelve support
            $(".support-selection").click(function (e) {
                e.preventDefault();

                // Get the support data
                let $id    = $(this).attr("data-id");
                let $name  = $(this).find("h3").text();
                let $image = $(this).find("img").attr("src");

                // Set the id on form
                $("input[name='shelve-support-id']").val($id);

                // Set support viewer configs
                $("#shelves-support-name").text($name);
                $("#support-image").attr("src", $image);

                // Dismiss this modal
                $("#supports-modal").modal('toggle');
            });

            // Open doors pullers type selection modal
            $("#change-door-puller").click((e) => {
                e.preventDefault();
                $("#door-puller-type-modal").modal('toggle');
            });

            $(".door-puller-select").click(function (e) {
                // Get puller data
                let $name  = $(this).find("h3").text();
                let $image = $(this).find("img").attr("src");

                // Pass the data to form
                $("input[name='door-puller-name']").val($name);
                $("#door-puller-name").text($name);
                $("#door-puller-image").attr("src", $image);

                // Dismiss this modal
                $("#door-puller-type-modal").modal('toggle');
            });

            $("#custom-door-puller-type-save").click((e) => {
                e.preventDefault();

                // Get puller name
                let $name = $("#door-puller-type-custom-name").val();

                // Verifies if the data was filled
                if ($name == undefined || $name.length == 0 || $name == null) {
                    alert("Preencha corretamente o nome do puxador");
                    return;
                }

                // Add data to page form
                $("input[name='door-puller-name']").val($name);
                $("#door-puller-name").text($name);
                $("#door-puller-image").attr("src", "");

                // Dismiss this modal
                $("#door-puller-type-modal").modal('toggle');
            });

            $(".door-no-puller").click(function (e) {
                e.preventDefault();

                // Get no puller image
                $image = $(this).find("img").attr("src");

                // Add data to form
                $("input[name='door-no-puller']").val("1");
                $("input[name='door-puller-name']").removeAttr("value");
                $("input[name='door-puller-color-name']").removeAttr("value");
                $("input[name='door-puller-color-code']").removeAttr("value");
                $("#door-puller-name").text("SEM Puxador");
                $("#door-puller-image").attr("src", $image);
                $("#door-puller-color").text("SEM Puxador");
                $("#door-puller-color-image").attr("src", $image);

                // Dismiss this modal
                $("#door-puller-type-modal").modal('toggle');
            })

            // Open doors pullers type selection modal
            $("#change-door-puller-color").click((e) => {
                e.preventDefault();
                $("#door-puller-color-modal").modal('toggle');
            });

            $(".door-puller-color-select").click(function (e) {
                e.preventDefault();

                // Get color data
                let $name  = $(this).find("h3").text();
                let $image = $(this).find("img").attr("src");

                // Add data to form
                $("input[name='door-puller-color-name']").val($name);
                $("#door-puller-color").text($name);
                $("#door-puller-color-image").attr("src", $image);

                // Dismiss this modal
                $("#door-puller-color-modal").modal('toggle');
            });

            $("#custom-door-puller-color-save").click((e) => {
                e.preventDefault();

                // Get color data
                let $name  = $("#door-puller-color-custom-name").val();
                let $color = $("#door-puller-color-custom-code").val();

                // Verifies if the data was filled properly
                if ($name == undefined || $name.length == 0 || $name == null || $color == undefined || $color.length == 0 || $color == null ) {
                    alert("Preencha corretamente a cor do puxador")
                    return;
                }

                // Verifies if color has #
                if ($color.indexOf("#") == -1) {
                    $color = "#" + $color;
                }

                // Add data to form
                $("input[name='door-puller-color-name']").val($name);
                $("input[name='door-puller-color-code']").val($color);
                $("#door-puller-color").text($name);
                $("#door-puller-color-image").attr("src", "");

                // Dismiss this modal
                $("#door-puller-color-modal").modal('toggle');
            });

            // Open doors pullers type selection modal
            $("#change-drawer-puller").click((e) => {
                e.preventDefault();
                $("#drawer-puller-type-modal").modal('toggle');
            });

            $(".drawer-puller-select").click(function (e) {
                // Get puller data
                let $name  = $(this).find("h3").text();
                let $image = $(this).find("img").attr("src");

                // Pass the data to form
                $("input[name='drawer-puller-name']").val($name);
                $("#drawer-puller-name").text($name);
                $("#drawer-puller-image").attr("src", $image);

                // Dismiss this modal
                $("#drawer-puller-type-modal").modal('toggle');
            });

            $("#custom-drawer-puller-type-save").click((e) => {
                e.preventDefault();

                // Get puller name
                let $name = $("#drawer-puller-type-custom-name").val();

                // Verifies if the data was filled
                if ($name == undefined || $name.length == 0 || $name == null) {
                    alert("Preencha corretamente o nome do puxador");
                    return;
                }

                // Add data to page form
                $("input[name='drawer-puller-name']").val($name);
                $("#drawer-puller-name").text($name);
                $("#drawer-puller-image").attr("src", "");

                // Dismiss this modal
                $("#drawer-puller-type-modal").modal('toggle');
            });

            $(".drawer-no-puller").click(function (e) {
                e.preventDefault();

                // Get no puller image
                $image = $(this).find("img").attr("src");

                // Add data to form
                $("input[name='drawer-no-puller']").val("1");
                $("input[name='drawer-puller-name']").removeAttr("value");
                $("input[name='drawer-puller-color-name']").removeAttr("value");
                $("input[name='drawer-puller-color-code']").removeAttr("value");
                $("#drawer-puller-name").text("SEM Puxador");
                $("#drawer-puller-image").attr("src", $image);
                $("#drawer-puller-color").text("SEM Puxador");
                $("#drawer-puller-color-image").attr("src", $image);

                // Dismiss this modal
                $("#drawer-puller-type-modal").modal('toggle');
            })

            // Open drawers pullers type selection modal
            $("#change-drawer-puller-color").click((e) => {
                e.preventDefault();
                $("#drawer-puller-color-modal").modal('toggle');
            });

            $(".drawer-puller-color-select").click(function (e) {
                e.preventDefault();

                // Get color data
                let $name  = $(this).find("h3").text();
                let $image = $(this).find("img").attr("src");

                // Add data to form
                $("input[name='drawer-puller-color-name']").val($name);
                $("#drawer-puller-color").text($name);
                $("#drawer-puller-color-image").attr("src", $image);

                // Dismiss this modal
                $("#drawer-puller-color-modal").modal('toggle');
            });

            $("#custom-drawer-puller-color-save").click((e) => {
                e.preventDefault();

                // Get color data
                let $name  = $("#drawer-puller-color-custom-name").val();
                let $color = $("#drawer-puller-color-custom-code").val();

                // Verifies if the data was filled properly
                if ($name == undefined || $name.length == 0 || $name == null || $color == undefined || $color.length == 0 || $color == null ) {
                    alert("Preencha corretamente a cor do puxador")
                    return;
                }

                // Verifies if color has #
                if ($color.indexOf("#") == -1) {
                    $color = "#" + $color;
                }

                // Add data to form
                $("input[name='drawer-puller-color-name']").val($name);
                $("input[name='drawer-puller-color-code']").val($color);
                $("#drawer-puller-color").text($name);
                $("#drawer-puller-color-image").attr("src", "");

                // Dismiss this modal
                $("#drawer-puller-color-modal").modal('toggle');
            });
        });
    </script>
@endpush