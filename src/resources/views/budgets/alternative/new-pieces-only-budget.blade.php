@extends('layouts.budgets.app')

@section('title', 'Orçamento de Ferragens')
@section('navbar-title', 'Adicionais')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/appearance.css') }}">
    <link rel="stylesheet" href="{{ asset('css/components/modals.css')}}">
@endpush

@section('content')
    <!-- content_wrapper_start -->
    <div class="container">
        <!-- page_top_start -->
        <div class="row">
            <div class="col-12">
                <!-- page_instruction_text -->
                <h1 class="poppins font-size-default font-medium">Adicionar Peça:</h1>

                <!-- page_instruction_sub-title_text -->
                <p class="poppins font-small font-medium text-primary-gray">Adicione Peças a mais no orçamento.</p>
            </div>
        </div>
        <!-- page_top_end -->

        <!-- page_form_start -->
        <form action="{{ route('new-pieces-budgets') }}" method="POST">
            @csrf

            <!-- hidden_controls_start -->
            <input autocomplete="off" type="hidden" name="piece-color" value="{{ $colors[0]->name }}">
            <input autocomplete="off" type="hidden" name="piece-color-code" value="{{ $colors[0]->code }}">
            <input autocomplete="off" type="hidden" name="tape-color" value="{{ $colors[0]->name }}">
            <input autocomplete="off" type="hidden" name="tape-color-code" value="{{ $colors[0]->code }}">
            <!-- hidden_controls_end -->

            <!-- name_field_wrapper_start -->
            <div class="form-group row mt-4">
                <!-- field_label -->
                <label for="name" class="col-md-12 col-form-label text-md-right">Nome da Peça:</label>

                <!-- input_wrapper_start -->
                <div class="col-md-12">
                    <!-- input -->
                    <input id="name" type="text" class="form-control" name="name" placeholder="Digite o nome" required autocomplete="off">
                </div>
                <!-- input_wrapper_end -->
            </div>
            <!-- name_field_wrapper_end -->

            <!-- quantity-thickness_wrapper_start -->
            <div class="row">
                <!-- thickness_wrapper_start -->
                <div class="col-7">
                    <!-- thickness_field_wrapper_start -->
                    <div class="form-group row mt-4">
                        <!-- field_label -->
                        <label for="thickness" class="col-md-12 col-form-label text-md-right">Espessura: <span>(mm)</span></label>

                        <!-- input_wrapper_start -->
                        <div class="col-md-12">
                            <!-- input -->
                            <input id="thickness" type="number" class="form-control" name="thickness" required autocomplete="off" value="15" min="15">
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- thickness_field_wrapper_end -->
                </div>
                <!-- thickness_wrapper_end -->

                <!-- quantity_wrapper_start -->
                <div class="col-5">
                    <!-- thickness_field_wrapper_start -->
                    <div class="form-group row mt-4">
                        <!-- field_label -->
                        <label for="quantity" class="col-md-12 col-form-label text-md-right">Quantidade:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-md-12">
                            <!-- input -->
                            <input id="quantity" type="number" class="form-control" name="quantity" required autocomplete="off" value="1" min="1">
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- thickness_field_wrapper_end -->
                </div>
                <!-- quantity_wrapper_end -->
            </div>
            <!-- quantity-thickness_wrapper_end -->

            <!-- sizes_section_header_wrapper_start -->
            <div class="row">
                <div class="col-12 pl-3 mt-4">
                    <!-- sizes_section_header_start -->
                    <h3 class="poppins font-size-default">
                        Dimensões do Módulo
                        <span class="text-secondary-gray">(mm)</span>
                    </h3>
                    <!-- sizes_section_header_end -->
                </div>
            </div>
            <!-- sizes_section_header_wrapper_end -->

            <!-- sizes_wrapper_start -->
            <div class="row">
                <!-- length_wrapper_start -->
                <div class="col-6">
                    <!-- length_field_wrapper_start -->
                    <div class="form-group row mt-4">
                        <!-- field_label -->
                        <label for="length" class="col-md-12 col-form-label text-md-right">Comprimento:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-md-12">
                            <!-- input -->
                            <input id="length" type="number" class="form-control" name="length" required autocomplete="off" value="15" min="15">
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- length_field_wrapper_end -->
                </div>
                <!-- length_wrapper_end -->

                <!-- width_wrapper_start -->
                <div class="col-6">
                    <!-- thickness_field_wrapper_start -->
                    <div class="form-group row mt-4">
                        <!-- field_label -->
                        <label for="width" class="col-md-12 col-form-label text-md-right">Largura:</label>

                        <!-- input_wrapper_start -->
                        <div class="col-md-12">
                            <!-- input -->
                            <input id="width" type="number" class="form-control" name="width" required autocomplete="off" value="1" min="1">
                        </div>
                        <!-- input_wrapper_end -->
                    </div>
                    <!-- width_field_wrapper_end -->
                </div>
                <!-- width_wrapper_end -->
            </div>
            <!-- sizes_wrapper_end -->

            <!-- tapes_wrapper_start -->
            <div class="row white-box py-2">
                <div class="col-12 d-flex flex-column poppins">
                    <!-- user_instruction -->
                    <h4 class="font-size-default">Selecione os lados da Fita de Borda:</h4>
                    
                    <!-- options_start -->
                    <div class="d-flex d-inline-flex pl-3 pt-2">
                        <!-- c1_option_start -->
                        <input type="checkbox" name="tape-c1" id="c1" value="1">
                        <label class="pl-1 pr-5" for="c1" style="position:relative; font-size: .75rem; top: 4px">C1</label>
                        <!-- c1_option_end -->

                        <!-- c2_option_start -->
                        <input type="checkbox" name="tape-c2" id="" value="1">
                        <label class="pl-1 pr-5" style="position:relative; font-size: .75rem; top: 4px">C2</label>
                        <!-- c2_option_end -->

                        <!-- l1_option_start -->
                        <input type="checkbox" name="tape-l1" id="" value="1">
                        <label class="pl-1 pr-5" style="position:relative; font-size: .75rem; top: 4px">L1</label>
                        <!-- l1_option_end -->

                        <!-- l2_option_start -->
                        <input type="checkbox" name="tape-l2" id="" value="1">
                        <label class="pl-1 pr-5" style="position:relative; font-size: .75rem; top: 4px">L2</label>
                        <!-- l2_option_end -->
                    </div>
                    <!-- options_end -->
                </div>
            </div>
            <!-- tapes_wrapper_end -->


            <!-- description_field_wrapper_start -->
            <div class="form-group row mt-4">
                <!-- field_label -->
                <label for="description" class="col-md-12 col-form-label text-md-right">Descrição: <span>(Opcional)</span></label>

                <!-- input_wrapper_start -->
                <div class="col-md-12">
                    <!-- input -->
                    <textarea id="description" class="form-control" name="description" placeholder="Descrição" autocomplete="off" min="1" value="1"></textarea>
                </div>
                <!-- input_wrapper_end -->
            </div>
            <!-- description_field_wrapper_end -->

            <!-- sub_section_header_start -->
            <div class="row mt-5">
                <div class="col-12 d-flex flex-column pl-3 poppins font-medium">
                    <!-- sub_section_header -->
                    <h3 class="font-size-default">Cores da Peça</h3>
                </div>
            </div>
            <!-- sub_section_header_end -->

            <!-- colors_selection_start -->
            <div class="row">
                <!-- main_color_selection_start -->
                <div class="col p-2 white-box ml-2 text-center">
                    <!-- color_type -->
                    <h4 class="text-primary-gray poppins font-size-default font-medium">Cor da Peça</h4>

                    <!-- color_viewer -->
                    <span class="color-viewer with-text" style="background: {{ $colors[0]->code }}" data-color-name="{{ $colors[0]->name }}"></span>

                    <!-- change_color_button -->
                    <a href="" class="change-link" id="change-piece-color">Clique para alterar</a>
                </div>
                <!-- main_color_selection_end -->

                <!-- module_internal_color_selection_start -->
                <div class="col p-2 white-box ml-2 text-center">
                    <!-- color_type -->
                    <h4 class="text-primary-gray poppins font-size-default font-medium">Cor das Fitas</h4>

                    <!-- color_viewer -->
                    <span class="color-viewer with-text" style="background: {{ $colors[0]->code }}" data-color-name="{{ $colors[0]->name }}"></span>

                    <!-- change_color_button -->
                    <a href="" class="change-link" id="change-tape-color">Clique para alterar</a>
                </div>
                <!-- module_internal_color_selection_end -->
            </div>
            <!-- colors_selection_end -->

            <!-- continue_wrapper_start -->
            <div class="row mt-5">
                <div class="col-12 d-flex flex-row justify-content-end">
                    <button class="btn btn-primary" type="submit">Continuar</button>
                </div>
            </div>
            <!-- continue_wrapper_end -->
        </form>
        <!-- page_form_end -->
    </div>
    <!-- content_wrapper_end -->

    <!-- module_internal_color_modal_start -->
    <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="piece-color-modal">
        <div class="modal-dialog" role="document">
            <!-- modal_content_wrapper_start -->
            <div class="modal-content">
                <!-- modal_body_start -->
                <div class="modal-body">
                    <!-- modal_breadcrumb_start -->
                    <div class="modal-breadcrumb">
                        <!-- breadcrumb_title -->
                        <h3 class="font-size-default">Selecione a Cor das Peças:</h3>
                    </div>
                    <!-- modal_breadcrumb_end -->

                    <!-- modal_body_content_wrapper_start -->
                    <div class="container">
                        <div class="row">
                            @foreach($colors as $color)
                                <div class="ml-2 white-box d-flex flex-column internal-module-color" style="width: 47% !important">
                                    <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $color->name }}</h3>
                                    <span class="color-viewer" style="background: {{ $color->code }}"></span>
                                </div>
                            @endforeach
                        </div>

                        <div class="row mt-4 white-box">
                            <div class="col-12 d-flex flex-column pl-3 pb-3 pt-2 poppins">
                                <h3 class="font-size-default font-medium text-primary-gray pl-2">Outra COR</h3>

                                <div class="d-flex flex-row row">
                                    <div class="col-5">
                                        <label for="module-internal-custom-name" class="font-small font-light">Nome da COR</label>
                                    </div>
                                    <div class="col-5">
                                        <label for="module-internal-custom-code" class="font-small font-light">Código da COR</label>
                                    </div>
                                </div>

                                <div class="d-flex flex-row">
                                    <div class="col-5 px-0">
                                        <input type="text" class="form-control" id="module-internal-custom-name" placeholder="Digite o Nome" autocomplete="off">
                                    </div>
                                    <div class="col-5 pr-0 pl-1">
                                        <input type="text" class="form-control" id="module-internal-custom-code" placeholder="Ex: 122F6E" autocomplete="off">
                                    </div>
                                    <div class="col-2 pr-0 pl-1">
                                        <button class="btn btn-primary" id="custom-module-internal-color-save" style="width: auto; height: auto">IR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal_body_content_wrapper_end -->
                </div>
                <!-- modal_body_end -->
            </div>
            <!-- modal_content_wrapper_end -->
        </div>
    </div>
    <!-- module_internal_color_modal_end -->

    <!-- module_external_color_modal_start -->
    <div class="modal sub-menu-modal fade" tabindex="-1" role="dialog" id="tape-color-modal">
        <div class="modal-dialog" role="document">
            <!-- modal_content_wrapper_start -->
            <div class="modal-content">
                <!-- modal_body_start -->
                <div class="modal-body">
                    <!-- modal_breadcrumb_start -->
                    <div class="modal-breadcrumb">
                        <!-- breadcrumb_title -->
                        <h3 class="font-size-default">Selecione a Cor das Fitas:</h3>
                    </div>
                    <!-- modal_breadcrumb_end -->

                    <!-- modal_body_content_wrapper_start -->
                    <div class="container">
                        <div class="row">
                            @foreach($colors as $color)
                                <div class="ml-2 white-box d-flex flex-column external-module-color" style="width: 47% !important">
                                    <h3 class="poppins font-size-default text-primary-gray text-right pr-3 pt-1 font-medium">{{ $color->name }}</h3>
                                    <span class="color-viewer" style="background: {{ $color->code }}"></span>
                                </div>
                            @endforeach
                        </div>

                        <div class="row mt-4 white-box">
                            <div class="col-12 d-flex flex-column pl-3 pb-3 pt-2 poppins">
                                <h3 class="font-size-default font-medium text-primary-gray pl-2">Outra COR</h3>

                                <div class="d-flex flex-row row">
                                    <div class="col-5">
                                        <label for="module-external-custom-name" class="font-small font-light">Nome da COR</label>
                                    </div>
                                    <div class="col-5">
                                        <label for="module-external-custom-code" class="font-small font-light">Código da COR</label>
                                    </div>
                                </div>

                                <div class="d-flex flex-row">
                                    <div class="col-5 px-0">
                                        <input type="text" class="form-control" id="module-external-custom-name" placeholder="Digite o Nome" autocomplete="off">
                                    </div>
                                    <div class="col-5 pr-0 pl-1">
                                        <input type="text" class="form-control" id="module-external-custom-code" placeholder="Ex: 122F6E" autocomplete="off">
                                    </div>
                                    <div class="col-2 pr-0 pl-1">
                                        <button class="btn btn-primary" id="custom-module-external-color-save" style="width: auto; height: auto">IR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal_body_content_wrapper_end -->
                </div>
                <!-- modal_body_end -->
            </div>
            <!-- modal_content_wrapper_end -->
        </div>
    </div>
    <!-- module_external_color_modal_end -->
@endsection

@push('js')
    <script type="text/javascript">
        function rgba2hex(rgb) {
            if (  rgb.search("rgb") == -1 ) {
                return rgb;
            }
            else if ( rgb == 'rgba(0, 0, 0, 0)' ) {
                return 'transparent';
            }
            else {
                rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
                function hex(x) {
                    return ("0" + parseInt(x).toString(16)).slice(-2);
                }
                return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]); 
            }
        }

        $(document).ready(() => {
            // Open module internal color selection
            $("#change-piece-color").click((e) => {
                e.preventDefault(); 
                $("#piece-color-modal").modal('toggle');
            });

            $(".internal-module-color").click(function (e) {
                e.preventDefault();

                // Get color data
                let $name  = $(this).find("h3").text();
                let $color = rgba2hex($(this).find("span").css("background-color"));

                console.log($color);

                // Adds the color name and code to his viewer
                $("#change-piece-color").prev().css({backgroundColor: $color});
                $("#change-piece-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='piece-color']").val($name);
                $("input[name='piece-color-code']").val($color);

                // Dispense this modal
                $("#piece-color-modal").modal('toggle');
            });

            // Save custom internal module color
            $("#custom-module-internal-color-save").click((e) => {
                e.preventDefault();

                // Get color values
                let $name  = $("#module-internal-custom-name").val();
                let $color = $("#module-internal-custom-code").val();

                // Validates the values
                if ($name == undefined || $name.length == 0 || $name == null || $color == undefined || $color.length == 0 || $color == null) {
                    // Alert the user to fill properly the fields
                    alert("Por favor, informe o nome e código da cor para adiciona-lá.");

                    // Ends function
                    return;
                }

                // Verifies if the color code has # char
                if ($color.indexOf("#") == -1) {
                    // Adds the # char to $color
                    $color = "#" + $color;
                }

                // Adds the color name and code to his viewer
                $("#change-piece-color").prev().css("background", $color);
                $("#change-piece-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='piece-color']").val($name);
                $("input[name='piece-color-code']").val($color);

                // Dispense this modal
                $("#piece-color-modal").modal('toggle');
            });

            // Open module external color selection
            $("#change-tape-color").click((e) => {
                e.preventDefault(); 
                $("#tape-color-modal").modal('toggle');
            });

            $(".external-module-color").click(function (e) {
                e.preventDefault();

                // Get color data
                let $name  = $(this).find("h3").text();
                let $color = rgba2hex($(this).find("span").css("background-color"));

                console.log($color);

                // Adds the color name and code to his viewer
                $("#change-tape-color").prev().css({backgroundColor: $color});
                $("#change-tape-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='tape-color']").val($name);
                $("input[name='tape-color-code']").val($color);

                // Dispense this modal
                $("#tape-color-modal").modal('toggle');
            });

            // Save custom external module color
            $("#custom-module-external-color-save").click((e) => {
                e.preventDefault();

                // Get color values
                let $name  = $("#module-external-custom-name").val();
                let $color = $("#module-external-custom-code").val();

                // Validates the values
                if ($name == undefined || $name.length == 0 || $name == null || $color == undefined || $color.length == 0 || $color == null) {
                    // Alert the user to fill properly the fields
                    alert("Por favor, informe o nome e código da cor para adiciona-lá.");

                    // Ends function
                    return;
                }

                // Verifies if the color code has # char
                if ($color.indexOf("#") == -1) {
                    // Adds the # char to $color
                    $color = "#" + $color;
                }

                // Adds the color name and code to his viewer
                $("#change-tape-color").prev().css("background", $color);
                $("#change-tape-color").prev().attr("data-color-name", $name);

                // Saves color settings to get in POST
                $("input[name='tape-color']").val($name);
                $("input[name='tape-color-code']").val($color);

                // Dispense this modal
                $("#tape-color-modal").modal('toggle');
            });
        })
    </script>
@endpush