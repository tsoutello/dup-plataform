@extends('layouts.budgets.app')

@section('title', 'Orçamento de Ferragens')
@section('navbar-title', 'Adicionais')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/appearance.css') }}">
@endpush

@section('content')
    <!-- content_wrapper_start -->
    <div class="container">
        <!-- page_top_start -->
        <div class="row">
            <div class="col-12">
                <!-- page_instruction_text -->
                <h1 class="poppins font-size-default font-medium">Adicionar Outro Item:</h1>

                <!-- page_instruction_sub-title_text -->
                <p class="poppins font-small font-medium text-primary-gray">Adicione um Item de sua necessidade.</p>
            </div>
        </div>
        <!-- page_top_end -->

        <!-- page_form_start -->
        <form action="{{ route('new-ironwares-budgets') }}" method="POST">
            @csrf
            
            <!-- name_field_wrapper_start -->
            <div class="form-group row mt-4">
                <!-- field_label -->
                <label for="name" class="col-md-12 col-form-label text-md-right">Digite o Nome do Item:</label>

                <!-- input_wrapper_start -->
                <div class="col-md-12">
                    <!-- input -->
                    <input id="name" type="text" class="form-control" name="name" placeholder="Digite o nome" required autocomplete="off">
                </div>
                <!-- input_wrapper_end -->
            </div>
            <!-- name_field_wrapper_end -->

            <!-- quantity_field_wrapper_start -->
            <div class="form-group row mt-4">
                <!-- field_label -->
                <label for="quantity" class="col-md-12 col-form-label text-md-right">Quantidade:</label>

                <!-- input_wrapper_start -->
                <div class="col-md-5">
                    <!-- input -->
                    <input id="quantity" type="number" class="form-control" name="quantity" autocomplete="off" min="1" value="1" required>
                </div>
                <!-- input_wrapper_end -->
            </div>
            <!-- quantity_field_wrapper_end -->

            <!-- description_field_wrapper_start -->
            <div class="form-group row mt-4">
                <!-- field_label -->
                <label for="description" class="col-md-12 col-form-label text-md-right">Descrição: <span>(Opcional)</span></label>

                <!-- input_wrapper_start -->
                <div class="col-md-12">
                    <!-- input -->
                    <textarea id="description" class="form-control" name="description" placeholder="Descrição" autocomplete="off" min="1" value="1"></textarea>
                </div>
                <!-- input_wrapper_end -->
            </div>
            <!-- description_field_wrapper_end -->

            <!-- continue_wrapper_start -->
            <div class="row mt-5">
                <div class="col-12 d-flex flex-row justify-content-end">
                    <button class="btn btn-primary" type="submit">Continuar</button>
                </div>
            </div>
            <!-- continue_wrapper_end -->
        </form>
        <!-- page_form_end -->
    </div>
    <!-- content_wrapper_end -->
@endsection

@push('js')
@endpush
