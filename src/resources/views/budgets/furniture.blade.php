@extends('layouts.budgets.app')

@section('title', 'Selecionar Módulo')
@section('navbar-title', 'Novo Orçamento')

@push('css')
    <style type="text/css">
        /* Furniture wrappers */
        .furniture {
            margin: .25rem 0.0rem;
            background: #fff;
            border: 1.5px solid #E7E7E7;
            box-sizing: border-box;
            box-shadow: 0px 3px 12px rgba(0, 0, 0, 0.1);
            border-radius: 4px;
        }

        /* Disabled Furniture wrappers */
        .furniture.disabled {
            border: 0;
            box-shadow: none;
            background: transparent;
        }

        /* Furnitures checkbox wrappers */
        .furniture .col-sm-2 {
            padding: 1rem 1rem;
                padding-bottom: 0;
                padding-right: 0;
        }

        .furniture .col-sm-2 input {
            height: 1rem;
            width: 1rem;
        }

        /* Furnitures Titles */
        .furniture .furniture-title h2 {
            display: flex;
            flex-direction: column;
            justify-content: center;
            height: 100%;
            font-family: "Poppins", sans-serif;
            font-weight: 400;
            font-size: 1rem !important;
            text-align: center;
        }

        /* Disabled Furnitures In-dev message */
        .furniture .furniture-title span {
            position: relative;
            right: 50%;
            top: 2rem;
            white-space: nowrap;
            color: #6F90B8;
            font-family: "Poppins", sans-serif;
            font-weight: 600;
            font-style: italic;
            font-size: .938rem;
            text-transform: uppercase;
        }

        .furniture:not(.disabled) .furniture-title span {
            display: none;
        }

        /* Disabled Furnitures Titles */
        .furniture.disabled .furniture-title h2 {
            color: var(--primary-gray);
        }

        /* Furnitures Image wrappers */
        .furniture .furniture-image{
            margin-left: auto !important;
            width: 42%;
            text-align: right;
        }

        /* Furnitures Images */
        .furniture .furniture-image img{
            margin: .5rem 0;
            border: 1px solid #E7E7E7;
            border-radius: 4px;
        }

        /* Disabled Furnitures Images */
        .furniture.disabled .furniture-image img {
            -webkit-filter: grayscale(50%);
            filter: grayscale(50%);
        }

        /* Furniture send */
        .furniture-send {
            margin: .25rem .28rem;
                margin-bottom: 1rem;
            border: 1px solid #E7E7E7;
            border-radius: 4px;
            background: #E7E7E7;
        }

        /* Furniture name input */
        .furniture-send input {
            height: 2rem;
            border-color: #2775D7 !important;
        }

        /* Furniture name field icon */
        .furniture-send img {
            border-color: #2775D7 !important;
        }

        /* Furniture wrapper when checked */
        .furniture.checked {
            border: 1.5px #2775D7 solid !important;
        }
    </style>
@endpush

@section('content')
    <!-- content_wrapper_start -->
    <div class="container">
        <!-- page_form_start -->
        <form action="{{ route('new-budget-furniture') }}" method="POST">
            @csrf

            <!-- hidden_control_fields_start -->
            <input type="hidden" name="environment-target" value="{{ $environmentTarget }}">
            <input type="hidden" name="furniture-target" value="{{ $furnitureTarget }}">
            <!-- hidden_control_fields_end -->

            <!-- page_top_start -->
            <div class="row">
                <div class="col-12">
                    <!-- page_instruction_text -->
                    <h1 class="poppins font-size-default font-medium">Escolha o tipo do MÓVEL:</h1>

                    <!-- page_instruction_sub-title_text -->
                    <p class="poppins font-small font-medium text-primary-gray">
                        Caso queira mais de um móvel no ambiente, você poderá adicionar no passo final.
                    </p>
                </div>
            </div>
            <!-- page_top_end -->

            <!-- furnitures_start -->
            <div class="row mt-3">
                <div class="col-12 p-1 px-2">
                    @foreach ($furnitures as $furniture)
                        <!-- furniture_wrapper_start -->
                        <div class="row furniture {{ $furniture->available ? '' : 'disabled' }}" id="furniture-{{ $furniture->id }}">
                            <!-- furniture_checkbox_wrapper_start -->
                            <div class="col-sm-2">
                                @if ($furniture->available)
                                    <!-- furniture_checkbox -->
                                    <input type="checkbox" name="furniture-id" value="{{ $furniture->id }}">
                                @endif
                            </div>
                            <!-- furniture_checkbox_wrapper_end -->

                            <!-- furniture_name_wrapper_start -->
                            <div class="col-sm-3 furniture-title">
                                <!-- furniture_name_text -->
                                <h2>{{ $furniture->name }} <span>Em desenvolvimento</span></h2>
                            </div>
                            <!-- furniture_name_wrapper_end -->

                            <!-- furniture-image_wrapper_start -->
                            <div class="col-sm-6 furniture-image">
                                <!-- furniture_image -->
                                <img src="{{ asset($furniture->image) }}" alt="furniture-image">
                            </div>
                            <!-- furniture-image_wrapper_end -->
                        </div>
                        <!-- furniture_wrapper_end -->

                        <!-- furniture_send_wrapper_start -->
                        <div class="row py-3 px-3  furniture-send d-none" id="send-{{ $furniture->id }}">
                            <!-- furniture_name_field_label -->
                            <label for="furniture-name-{{ $furniture->id }}" class="pl-4 col-form-label">Nome do Módulo:</label>

                            <!-- furniture_name_wrapper_start -->
                            <div class="col-12 d-flex">
                                <!-- input -->
                                <input type="text" id="furniture-name-{{ $furniture->id }}" name="furniture-alias" class="form-control reverse-border with-icon" required value="{{ $furniture->name }}">

                                <!-- input_icon -->
                                <img src="{{ asset('assets/icons/edit.png') }}" class="form-control-icon reverse-border">
                            </div>
                            <!-- environment_name_wrapper_end -->

                            <!-- send_button -->
                            <button class="btn btn-primary mt-3 ml-auto send-button" type="button" data-furniture-id="{{ $furniture->id }}">Continuar</button>
                        </div>
                        <!-- environment_send_wrapper_end -->
                    @endforeach
                </div>
            </div>
            <!-- ambients_end -->
        </form>
    </div>
    <!-- content_wrapper_end -->
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(() => {
            /**
             * Uncheck all checkboxes.
             * 
             * @return {void}
             */
             $("input[type='checkbox']").each(function () {
                 // Uncheck the current element
                 $(this).prop('checked', false);
             });

            /**
             * Handle changes in the selected furniture.
             *
             * @return {void}
             */
            $("input[type='checkbox']").on("change", function () {
                // Gets the target furniture id and his state
                let targetId    = $(this).val();
                let targetState = $(this).is(':checked');

                // Verifies the target state
                if (targetState === true) {
                    // Set the furniture target to checked
                    $(`#furniture-${targetId}`).addClass('checked');

                    // Show the corresponding sender
                    $(`#send-${targetId}`).removeClass('d-none');

                    // Loop each checkbox (except the changed)
                    $("input[type='checkbox']").each(function () {
                        // Get the environment id of the current element
                        let currentId = $(this).val();

                        // Verifies if the current element is the clicked element
                        if (currentId == targetId) {
                            // Ends the function
                            return;
                        }

                        // Uncheck the element
                        $(this).prop('checked', false);

                        // Remove checked class from current checkbox target
                        $(`#furniture-${currentId}`).removeClass("checked");

                        // Hide the senders to others environments
                        $(`#send-${currentId}`).addClass('d-none');
                    });
                } else {
                    // Loop each checkbox
                    $("input[type='checkbox']").each(function () {
                        // Get the environment id of the current element
                        let currentId = $(this).val();

                        console.log(currentId)

                        // Uncheck the element
                        $(this).prop('checked', false);

                        // Remove checked class from current checkbox target
                        $(`#furniture-${currentId}`).removeClass("checked");

                        // Hide the senders to others environments
                        $(`#send-${currentId}`).addClass('d-none');
                    });
                }
            });

            /**
             * Handle clicks in send button.
             *
             * @return {void}
             */
            $(".send-button").click(function (e) {
                // Prevent the click action
                e.preventDefault();

                // Get the furniture id
                let targetId = $(this).attr("data-furniture-id");

                // Loop all furniture name inputs
                $("input[name='furniture-alias']").each(function () {
                    // Get the current element id
                    let currentId = $(this).attr('id');

                    // Verifies if current element is the selected by user
                    if (currentId == `furniture-name-${targetId}`) {
                        // Skips to the next loop executation
                        return;
                    }

                    // Delete current element
                    $(this).remove();
                });

                // Submit the form
                $("form").submit();
            });
        });
    </script>
@endpush