<!-- footer_start -->
<footer>
    <!-- footer_content_wrapper_start -->
    <div class="container">
        <div class="row pt-2 pb-2">
            <!-- footer_content_wrapper_start -->
            <a class="col" href="#" disabled>
                <!-- entrie_icon -->
                <img src="{{ asset('assets/icons/footer/home.png') }}" alt="home" width="22" height="20">

                <!-- entrie_text -->
                <span class="footer-link">Início</span>
            </a>
            <!-- footer_content_wrapper_end -->

            <!-- footer_content_wrapper_start -->
            <a class="col" href="{{ route('home') }}">
                <!-- entrie_icon -->
                <img src="{{ asset('assets/icons/footer/budget.png') }}" alt="orcamentos" width="24" height="24">

                <!-- entrie_text -->
                <span class="footer-link active">Orçamentos</span>
            </a>
            <!-- footer_content_wrapper_end -->

            <!-- footer_content_wrapper_start -->
            <a class="col" href="#" disabled>
                <!-- entrie_icon -->
                <img src="{{ asset('assets/icons/footer/projects.png') }}" alt="projetos" width="24" height="24">

                <!-- entrie_text -->
                <span class="footer-link">Projetos</span>
            </a>
            <!-- footer_content_wrapper_end -->

            <!-- footer_content_wrapper_start -->
            <a class="col" href="#" disabled>
                <!-- entrie_icon -->
                <img src="{{ asset('assets/icons/footer/clients.png') }}" alt="clientes" width="24" height="24">

                <!-- entrie_text -->
                <span class="footer-link">Clientes</span>
            </a>
            <!-- footer_content_wrapper_end -->

            <!-- footer_content_wrapper_start -->
            <a class="col" href="#" disabled>
                <!-- entrie_icon -->
                <img src="{{ asset('assets/icons/footer/settings.png') }}" alt="ajustes" width="24" height="24">

                <!-- entrie_text -->
                <span class="footer-link">Ajustes</span>
            </a>
            <!-- footer_content_wrapper_end -->
        </div>
    </div>
    <!-- footer_content_wrapper_end -->
</footer>
<!-- footer_end -->