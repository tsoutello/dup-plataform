<!doctype html>
<html>
<head>
    <!-- Bootstrap meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Page title -->
    <title>{{ env('APP_NAME') }} - @yield('title')</title>
    
    <!-- Bootstrap CSS -->
    <link href="{{ asset('css/resources/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Theme colors -->
    <link rel="stylesheet" href="{{ asset('css/colors.css') }}">

    <!-- Components CSS -->
    <link rel="stylesheet" href="{{ asset('css/components/buttons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/components/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('css/components/forms.css') }}">

    <!-- App css -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @stack('css')