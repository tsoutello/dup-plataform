<!-- sidebar_toggler_checkbox -->
<input type="checkbox" id="sidebar-toggler" checked="false">

<!-- sidebar_start -->
<nav class="sidebar d-none">
    <!-- sidebar_content_wrapper_start -->
    <div class="container">
        <!-- sidebar_top_row_start -->
        <div class="row pt-4 pb-3 pl-3 border-bottom">
            <!-- user_icon_wrapper_start -->
            <div class="col-sm-2">
                <div>
                    <span class="sidebar-user-initial">
                        @if (auth()->user()->name)
                            {{ substr(auth()->user()->name, 0, 1) }}
                        @endif
                    </span>
                </div>
            </div>
            <!-- user_icon_wrapper_end -->

            <!-- user_info_wrapper_start -->
            <div class="col-sm-7 d-flex flex-column sidebar-user-info">
                <!-- username_start -->
                <div>
                    <!-- username_text_start -->
                    <span class="text-primary-gray poppins font-regular">
                        @if (auth()->user()->name)
                            {{ auth()->user()->name }}
                        @endif
                    </span>
                    <!-- username_text_end -->
                </div>
                <!-- username_end -->

                <!-- username_start -->
                <div>
                    <!-- username_text_start -->
                    <span class="text-secondary-gray poppins font-medium font-small">
                        @if (auth()->user()->company_name)
                            {{ auth()->user()->company_name }}
                        @endif
                    </span>
                    <!-- username_text_end -->
                </div>
                <!-- username_end -->
            </div>
            <!-- user_info_wrapper_end -->

            <div class="col-sm-3 d-flex justify-content-center">
                <!-- sidebar_toggler_field_label_start -->
                <label for="sidebar-toggler">
                    <!-- left_arrow_icon -->
                    <img src="{{ asset('assets/icons/left-arrow.png') }}" alt="toggle-sidebar" width="16" height="16" class="sidebar-toggler-left-arrow">

                    <!-- menu_icon -->
                    <img src="{{ asset('assets/icons/menu-toggled.png') }}" alt="toggle-sidebar" width="24" height="24" class="sidebar-toggler-menu">
                </label>
                <!-- sidebar_toggler_field_label_end -->
            </div>
        </div>
        <!-- sidebar_top_row_end -->

        <!-- menu_list_start -->
        <div class="row">
            <div class="col-12">
                <!-- menu_items_start -->
                <ul class="sidebar-nav">
                    <!-- navigation_section_start -->
                    <li class="sidebar-section">
                        <!-- section_title_start -->
                        <li class="sidebar-section-title">
                            <span>Navegação</span>
                        </li>
                        <!-- section_title_end -->

                        <!-- section_items_start -->
                        <li class="sidebar-section-content">
                            <li class="sidebar-item active">
                                <!-- menu_icon -->
                                <img src="{{ asset('assets/icons/sidebar/dup-academy.png') }}" alt="dup-academy">

                                <!-- menu_text -->
                                <a href="#" class="sidebar-link" disabled>DUP Academy</a>
                            </li>

                            <li class="sidebar-item disabled">
                                <!-- menu_icon -->
                                <img src="{{ asset('assets/icons/sidebar/help.png') }}" alt="faq">

                                <!-- menu_text -->
                                <a href="#" class="sidebar-link" disabled>F. A. Q.</a>
                                <!-- menu_text_end -->
                            </li>

                            <li class="sidebar-item disabled">
                                <!-- menu_icon -->
                                <img src="{{ asset('assets/icons/sidebar/galery.png') }}" alt="galeria-de-projetos">

                                <!-- menu_text -->
                                <a href="#" class="sidebar-link" disabled>Galeria de Projetos</a>
                            </li>

                            <li class="sidebar-item disabled">
                                <!-- menu_icon -->
                                <img src="{{ asset('assets/icons/sidebar/settings.png') }}" alt="ajustes">

                                <!-- menu_text -->
                                <a href="#" class="sidebar-link" disabled>Ajustes</a>
                            </li>
                        </li>
                        <!-- section_items_end -->
                        
                        <!-- profile_section_start -->
                        <li class="sidebar-section">
                            <!-- section_title_start -->
                            <li class="sidebar-section-title disabled">
                                <span>Perfil</span>
                            </li>
                            <!-- section_title_end -->
    
                            <!-- section_items_start -->
                            <li class="sidebar-section-content">
                                <li class="sidebar-item disabled">
                                    <!-- menu_icon -->
                                    <img src="{{ asset('assets/icons/sidebar/user-profile.png') }}" alt="meu-perfil">
    
                                    <!-- menu_text -->
                                    <a href="#" class="sidebar-link with-chevron" disabled>Meu Perfil</a>
                                </li>

                                <li class="sidebar-item disabled">
                                    <!-- menu_icon -->
                                    <img src="{{ asset('assets/icons/sidebar/money.png') }}" alt="financeiro">
    
                                    <!-- menu_text -->
                                    <a href="#" class="sidebar-link with-chevron" disabled>Financeiro</a>
                                </li>

                                <li class="sidebar-item">
                                    <!-- menu_icon -->
                                    <img src="{{ asset('assets/icons/sidebar/exit.png') }}" alt="sair">
    
                                    <!-- menu_text -->
                                    <a href="{{ route('sair') }}" class="sidebar-link">Sair</a>
                                </li>
                            </li>
                            <!-- section_items_end -->
                        </li>
                        <!-- profile_section_end -->
                    </ul>
                </ul>
                <!-- menu_items_end -->
            </div>
        </div>
        <!-- menu_list_end -->

        <!-- sidebar-in-dev_message_start -->
        <div class="sidebar-in-dev">
            <!-- in-dev_message -->
            <h2>Em Desenvolvimento</h2>
        </div>
        <!-- sidebar-in-dev_message_end -->
    </div>
    <!-- sidebar_content_wrapper_end -->
</nav>
<!-- sidebar_end -->