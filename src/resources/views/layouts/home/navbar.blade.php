</head>
<body>

    <!-- navbar_start -->
    <nav class="navbar">
        <!-- navbar_content_wrapper_start -->
        <div class="nav-row pt-2">
            <!-- sidebar-toggler_start -->
            <div>
                <!-- sidebar_toggler_field_label_start -->
                <label for="sidebar-toggler">
                    <!-- menu_icon -->
                    <img src="{{ asset('assets/icons/menu.png') }}" alt="toggle-sidebar" width="24" height="24">
                </label>
                <!-- sidebar_toggler_field_label_end -->
            </div>
            <!-- sidebar-toggler_end -->

            <!-- navbar_page_name_start -->
            <div>
                <!-- page_title_text -->
                <span class="navbar-page-title">Orçamentos</span>
            </div>
            <!-- navbar_page_name_end -->

            <!-- three_dots_wrapper_start -->
            <div class="dropdown">
                <!-- three_dots_icon -->
                <img src="{{ asset('assets/icons/three-dots.png') }}" alt="context-menu" width="10" height="24" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                
                <!-- three_dots_menu_wrapper_start -->
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="navbar-dropdown">
                    <!-- whatsapp_link_start -->
                    <a class="dropdown-item poppins font-medium text-primary-gray font-small" href="https://api.whatsapp.com/send?phone={{ env('WHATSAPP_CONTACT') }}" target="_blank">
                        <!-- phone_icon -->
                        <img src="{{ asset('assets/icons/phone.png') }}" alt="whatsapp" width="24" height="24" class="mr-2">
                        Entrar em contato
                    </a>
                    <!-- whatsapp_link_start -->
                </div>
                <!-- three_dots_menu_wrapper_end -->
            </div>
            <!-- three_dots_wrapper_start -->
        </div>
        <!-- navbar_content_wrapper_start -->
    </nav>
    <!-- navbar_end -->