@include('layouts.headers')
@include('layouts.home.headers')
@include('layouts.home.navbar')
@include('layouts.home.sidebar')

    <main class="py-4">
        @yield('content')
    </main>

    @push('js')
        <script src="{{ asset('js/app.js') }}"></script>
    @endpush

@include('layouts.footer')
@include('layouts.document-end')