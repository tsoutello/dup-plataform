<!-- Variables -->
<script type="text/javascript">
    const URL = "{{ url('/') }}";
</script>

<!-- JQuery -->
<script src="{{ asset('js/resources/jquery_3_6.min.js') }}"></script>
<!-- PopperJS -->
<script src="{{ asset('js/resources/popper_1_12.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('js/resources/bootstrap.min.js') }}"></script>

@stack('js')

</body>
</html>