</head>
<body>

<!-- navbar_start -->
<nav class="navbar bg-light">
    <!-- navbar_back_button_start -->
    <a href="{{ url()->previous() }}" class="navbar-brand mr-auto navbar-back-button">
        <img src="{{ asset('assets/icons/back-arrow.png') }}" alt="back-arrow">
    </a>
    <!-- navbar_back_button_end -->

    <!-- navbar_logo_start -->
    <a href="{{ url('/') }}" class="navbar-brand ml-auto navbar-dup-logo">
        <img src="{{ asset('assets/logo.png') }}" alt="dup">
    </a>
    <!-- navbar_logo_end -->
</nav>
<!-- navbar_end -->