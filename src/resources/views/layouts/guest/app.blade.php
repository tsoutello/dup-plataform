@include('layouts.headers')
@include('layouts.guest.headers')
@include('layouts.guest.navbar')

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    @push('js')
        <script src="{{ asset('js/auth.js') }}"></script>
    @endpush

@include('layouts.document-end')