@include('layouts.headers')
@include('layouts.budgets.headers')
@include('layouts.budgets.navbar')

    <main class="py-4">
        @yield('content')
    </main>

    @push('js')
        <script src="{{ asset('js/budget.js') }}"></script>
    @endpush

@include('layouts.footer')
@include('layouts.document-end')