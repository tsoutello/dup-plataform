</head>
<body>

    <!-- navbar_start -->
    <nav class="navbar">
        <!-- navbar_content_wrapper_start -->
        <div class="nav-row pt-2 pb-2">
            <!-- menu_icon_start -->
            <div>
                <!-- back_link_start -->
                <a href="{{ url()->previous() }}">
                    <!-- menu_icon -->
                    <img src="{{ asset('assets/icons/white-left-arrow.png') }}" alt="toggle-sidebar" width="24" height="24">
                <!-- back_link_start -->
            </div>
            <!-- menu_icon_start -->

            <!-- navbar_page_name_start -->
            <div>
                <!-- page_title_text -->
                <span class="navbar-page-title text-left">@yield('navbar-title')</span>
            </div>
            <!-- navbar_page_name_end -->

            <!-- three_dots_wrapper_start -->
            <div class="dropdown">
                <!-- three_dots_icon -->
                <img src="{{ asset('assets/icons/three-dots.png') }}" onclick="contact()" alt="context-menu" width="10" height="24" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                
                <!-- three_dots_menu_wrapper_start -->
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="navbar-dropdown-2">
                    <!-- whatsapp_link_start -->
                    <a class="dropdown-item poppins font-medium text-primary-gray font-small" href="https://api.whatsapp.com/send?phone={{ env('WHATSAPP_CONTACT') }}" target="_blank">
                        <!-- phone_icon -->
                        <img src="{{ asset('assets/icons/phone.png') }}" alt="whatsapp" width="24" height="24" class="mr-2">
                        Entrar em contato
                    </a>
                    <!-- whatsapp_link_start -->
                </div>
                <!-- three_dots_menu_wrapper_end -->
            </div>
            <!-- three_dots_wrapper_start -->
        </div>
        <!-- navbar_content_wrapper_end -->
    </nav>
    <!-- navbar_end -->

    @push('js')
        <script type="text/javascript">
            function contact() {
                $("#navbar-dropdown-2").css({
                    display: ($("#navbar-dropdown-2").css("display") == "none" ? "block" : "none"),
                    position: 'absolute',
                    left: "-180px"
                });

                console.log(($("#navbar-dropdown-2").css("display") == "none" ? "block" : "none"))
            }
        </script>
    @endpush