<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    /**
     * Point the target table to module_models
     * 
     * @var string $table
     */
    protected $table = "module_models";
}
