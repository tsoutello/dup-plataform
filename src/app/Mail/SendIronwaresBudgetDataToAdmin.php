<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendIronwaresBudgetDataToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     * 
     * @param array $data
     *
     * @return void
     */
    public function __construct(array $data)
    {
        // Set data propertie to get in build
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Loads message view
        return $this->subject("Novo Orçamento de Ferragens")
            ->view('emails.ironwares-budget')
            ->with($this->data);
    }
}
