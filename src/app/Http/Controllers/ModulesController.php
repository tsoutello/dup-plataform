<?php

namespace App\Http\Controllers;

use App\Module;
use App\Slider;
use Illuminate\Http\Request;

class ModulesController extends Controller
{
    /**
     * Responds a AJAX request, with all modules models.
     * 
     * @param Request $request
     * 
     * @return response
     */
    public function ajaxGetAll(Request $request)
    {
        // Get the modules
        $modules = Module::join("images", "module_models.image", "=", "images.id")
            ->select([
                "module_models.id",
                "module_models.name",
                "module_models.doors_amount as doors",
                "module_models.drawers_amount as drawers",
                "module_models.shelves_amount as shelves",
                "module_models.description",
                "images.src as image"
            ])
            ->orderBy('module_models.doors_amount', "asc")
            ->orderBy('module_models.id', 'asc')
            ->get();

        // Stores modules grouped by doors amount
        $groupedModules = array();

        // Loop controller
        $lastDoorsAmount = 0;

        // Loop in $modules, to group doors by amount
        foreach ($modules as $item => $module) {
            // Verifies if $lastDoorsAmount is minor than the current one
            if ($lastDoorsAmount < $module->doors) {
                // Append a new models group to $groupedModules
                array_push( $groupedModules, array(
                    "doors" => $module->doors, 
                    "data"  => array()
                ));
            }

            // Get the array last key
            $targetGroup = array_key_last($groupedModules);

            // Push module data to target modules group
            array_push($groupedModules[$targetGroup]["data"], array(
                "id"          => $module->id,
                "name"        => $module->name,
                "doors"       => $module->doors,
                "drawers"     => $module->drawers,
                "shelves"     => $module->shelves,
                "description" => $module->description,
                "image"       => $module->image
            ));

            // Set $lastDoorsAmount to current doors amount
            $lastDoorsAmount = $module->doors;
        }

        // Encode the formatted data as JSON
        $modules = json_encode($groupedModules);

        // Return a response to the request
        return response()->json($modules);
    }

    /**
     * Responds a AJAX request with module model data, based on the provided model id.
     * 
     * @param Request $request
     * 
     * @return response
     */
    public function ajaxGetById(Request $request)
    {
        // Get the module
        $module = Module::where("module_models.id", $request->input("id"))
            ->join("images", "module_models.image", "=", "images.id")
            ->select([
                "module_models.id",
                "module_models.name",
                "module_models.doors_amount as doors",
                "module_models.height",
                "module_models.width",
                "module_models.depth",
                "module_models.description",
                "images.src as image"
            ])
            ->first();

        // Encode the recovered data as JSON
        $module = json_encode($module);

        // Return a response to the request
        return response()->json($module);
    }

    /**
     * Responsds a AJAX request with a slider name, searching by the slider id provided by POST.
     * 
     * @param Request $request
     * 
     * @return response
     */
    public function ajaxGetSliderById(Request $request)
    {
        // Get the slider
        $slider = Slider::select(["name"])
            ->where("id", $request->input("slider"))
            ->first();

        // Encode the slider data as JSON
        $slider = json_encode($slider);

        // Return $slider as response to the request
        return response()->json($slider);
    }
}
