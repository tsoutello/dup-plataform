<?php

namespace App\Http\Controllers;

use App\Image;
use App\Color;
use App\Client;
use App\Module;
use App\Mirror;
use App\Puller;
use App\Slider;
use App\Furniture;
use App\Structure;
use App\ShelfSupport;
use Illuminate\Http\Request;
use App\MountingEnvironments;
use Illuminate\Support\Facades\DB;
use App\Mail\SendBudgetDataToAdmin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendPiecesBudgetDataToAdmin;
use App\Mail\SendIronwaresBudgetDataToAdmin;

class BudgetController extends Controller
{
    /**
     * Controller constructor. Prevent not allowed users access the system.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            // Verifies if the allowed user field is 1
            if (Auth::user()->allowed != "1") {
                // Stores logout url
                $logout = route('sair');

                // Alert the user to request access and redirect to loggof
                echo "
                    <script>
                        alert('Seu acesso não foi liberado. Contate o administrador.');
                        window.location.href='{$logout}';
                    </script>
                ";
            } else {
                // Continue with the request
                return $next($request);
            } 
        });
    }

    /**
     * Load the new client view.
     * 
     * @param Request $request
     * 
     * @return Illuminate\View\View
     */
    public function client(Request $request)
    {
        // Remove the existing (if was) client data to budget
        $request->session()->pull("clientData");

        // Remove newBudget session if exists
        $request->session()->pull("newBudget");

        // Return the new client view
        return view("budgets.new-client");
    }
    
    /**
     * Stores the client data to the budget, and redirect to the budget initial page.
     * 
     * @param Request $request
     * 
     * @return redirect
     */
    public function addClientToBudget(Request $request)
    {
        // Get the POST data
        $name    = $request->input("name");
        $email   = $request->input("email");
        $phone   = ($request->hasAny(["phone"]) ? $request->input("phone") : null);
        $address = ($request->hasAny(["address"]) ? $request->input("address") : null);
        
        // Verifies if the client already exists
        $client = Client::verifyClientExists($email, Auth::id());
        
        // Verifies the client validation result
        if ($client === false) {
            // Set $client to an array with client POST data
            $client = [$name, $email, $phone, $address];
        } else {
            // Set $client to retrieved client data
            $client = [$client->name, $client->email, $client->phone, $client->address];
        }
        
        // Remove the existing (if was) client data to target budget
        $request->session()->pull("clientData");

        // Remove existing (if was) newBudget session
        $request->session()->pull("newBudget"); 
        
        // Stores the client data into clientData session
        $request->session()->put("clientData", $client);
        
        // Redirect to budget initial page
        return redirect(route('new-budget-initial'));
    }
    
    /**
     * Loads the budget initial view.
     * 
     * @return mixed
     */
    public function initials(Request $request)
    {
        // Verifies if client data was setted
        if (!$request->session()->has('clientData')) {
            // Redirect to add client screen
            return redirect(route('new-budget-client'));
        }

        // View data
        $data = array();

        // Pass the environments to view
        $data["environments"] = MountingEnvironments::join('images', "mounting_environments.ambient_image", "=", "images.id")
            ->get([
                "mounting_environments.id", 
                "mounting_environments.ambient_name", 
                "mounting_environments.available", 
                "images.src as image"
            ]);

        // Get the individual pieces image and pass to view
        $data["individualPieces"] = Image::where('alias', 'individual-pieces')
            ->select(['src'])
            ->first()
            ->src;

        // Get the ironwares image and pass to view
        $data["ironwares"] = Image::where('alias', 'ironware')
            ->select(['src'])
            ->first()
            ->src;

        // Pass a environment target to view, if not was any defined, set to 0
        $data["environmentTarget"] = ($request->session()->has("environmentTarget") ? $request->session()->get("environmentTarget") : 0);

        // Loads the budgets.initial screen
        return view('budgets.initials', $data);
    }

    /**
     * Add environment settings.
     * 
     * @param Request $request
     * 
     * @return redirect
     */
    public function addInitials(Request $request)
    {
        // Get the POST data
        $environmentName   = $request->input("ambient-alias");
        $environmentId     = $request->input("ambient-id");
        $environmentTarget = $request->input("environment-target");

        // Clear the budgetTarget and newBudget.{budgetTarget}.environment session
        $request->session()->pull("environmentTarget");

        // Session path to new data
        $newSessionPath = "newBudget.environment.{$environmentTarget}";

        // Set the new environment settings to the target budget
        $request->session()->put("{$newSessionPath}", array(
            "environmentID"   => $environmentId,
            "environmentName" => $environmentName
        ));

        // Set the environment target to be used in next screen
        $request->session()->put("environmentTarget", $environmentTarget);

        // Redirect to the next page
        return redirect(route('new-budget-furniture'));
    }

    /**
     * Loads the furniture choose screen.
     * 
     * @param Request $request
     * 
     * @return mixed
     */
    public function furniture(Request $request)
    {
        // Verifies if client data and budgetTarget was setted
        if (!$request->session()->has('clientData') || !$request->session()->has('environmentTarget')) {
            // Redirect to add client screen
            return redirect(route('new-budget-client'));
        }

        // Get the budget target
        $environmentTarget = $request->session()->get('environmentTarget');

        // Stores the existing data session path
        $sessionPath = "newBudget.environment.{$environmentTarget}";

        // Verifies if the environment to this target was setted
        if (!$request->session()->has($sessionPath) || is_null($request->session()->get("{$sessionPath}.environmentID"))) {
            // Redirect to environment selection view
            return redirect(route('new-budget-initial'));
        }

        // Stores view data
        $data = array();

        // Pass the furnitures to view
        $data["furnitures"] = Furniture::join("images", "furnitures.image", "=", "images.id")
            ->get([
                "furnitures.id", 
                "furnitures.available", 
                "furnitures.name", 
                "images.src as image"
            ]);

        // Pass $environmentTarget to view
        $data["environmentTarget"] = $environmentTarget;

        // Verifies if exists a specific furniture target
        $data["furnitureTarget"] = ($request->session()->has("furnitureTarget") ? $request->session()->get("furnitureTarget") : 0);

        // Loads furniture view
        return view('budgets.furniture', $data);
    }

    /**
     * Add a furniture to target budget.
     * 
     * @param Request $request
     * 
     * @return redirect
     */
    public function addFurniture(Request $request)
    {
        // Get the form data
        $furnitureName     = $request->input('furniture-alias');
        $furnitureId       = $request->input('furniture-id');
        $environmentTarget = $request->input('environment-target');
        $furnitureTarget   = $request->input('furniture-target');

        // Stores session path to write less code
        $sessionPath = "newBudget.environment.{$environmentTarget}.furnitures";

        // Clear the environment, furnitureTarget and target furniture data in session
        $request->session()->pull("environmentTarget");
        $request->session()->pull("furnitureTarget");
        $request->session()->pull("{$sessionPath}.furniture");

        // Set the new furniture settings to the target budget
        $request->session()->put("{$sessionPath}.{$furnitureTarget}.furniture", [
            "furnitureID"   => $furnitureId,
            "furnitureAlias" => $furnitureName
        ]);
        
        // Set the environment and furniture targets to be used in next screen
        $request->session()->put("environmentTarget", $environmentTarget);
        $request->session()->put("furnitureTarget", $furnitureTarget);
        
        // Redirect to the next page
        return redirect(route('new-budget-structure'));
    }
    
    /**
     * Loads structure settings screen to the target budget.
     * 
     * @param Request $request
     * 
     * @return mixed
     */
    public function structure(Request $request)
    {
        /* -------------------- Session validations -------------------- */
        // Verifies if client data and budgetTarget was setted
        if (!$request->session()->has('clientData') || !$request->session()->has('environmentTarget')) {
            // Redirect to add client screen
            return redirect(route('new-budget-client'));
        }
        
        // Get the environment target
        $environmentTarget = $request->session()->get('environmentTarget');
        
        // Get the existing session path, with index in target environment
        $sessionPath = "newBudget.environment.{$environmentTarget}";
        
        // Verifies if the environment to this target was setted
        if (!$request->session()->has($sessionPath) || is_null($request->session()->get("{$sessionPath}.environmentID"))) {
            // Redirect to environment selection screen
            return redirect(route('new-budget-initial'));
        }
        
        // Verifies if furnitureTarget was setted
        if (!$request->session()->has("furnitureTarget")) {
            // Redirect to furniture selection screen
            return redirect(route('new-budget-furniture'));
        }
        
        // Get the furniture target
        $furnitureTarget = $request->session()->get("furnitureTarget");
        
        // Set $sessionPath to index in furniture target
        $sessionPath = "{$sessionPath}.furnitures.{$furnitureTarget}";
        
        // Verifies if the furniture to this target was setted
        if (!$request->session()->has("{$sessionPath}.furniture") || is_null($request->session()->get("{$sessionPath}.furniture.furnitureID"))) {
            // Redirect to environment selection view
            return redirect(route('new-budget-furniture'));
        }
        
        // Stores view data
        $data = array();
        
        /* -------------------- View default data -------------------- */
        
        // Pass the structures to view
        $data["structures"] = Structure::join("images", "structures.image", "=", "images.id")
            ->get([
                "structures.id",
                "structures.name",
                "structures.available",
                "images.src as image",
            ]);
        
        // Pass the Advanced customization card image to view
        $data["advancedCustomization"] = Image::where("alias", "advanced-customization")
            ->select(["src"])
            ->first()
            ->src;
        
        // Pass the coupled suitcase card image to view
        $data["coupledSuitcase"] = Image::where("alias", "coupled-suitcase")
            ->select(["src"])
            ->first()
            ->src;
            
        // Pass an array with the disabled advanced customizations to view
        $data['advancedCustomizationDisabled'] = array(
            ["img" => "uploads/advanced-customization/ceiling-slope.png", "text" => "Desnível de Teto"],
            ["img" => "uploads/advanced-customization/tamponade.png", "text" => "Tamponamento"],
            ["img" => "uploads/advanced-customization/thickened-side.png", "text" => "Lateral Engrossada"],
            ["img" => "uploads/advanced-customization/built-in-socket.png", "text" => "Tomada Embutida"],
            ["img" => "uploads/advanced-customization/machining.png", "text" => "Usinagem"]
        );
        
        // Pass target furniture name to the view
        $data["furnitureName"] = $request->session()->get("{$sessionPath}.furniture.furnitureAlias");
            
        // Pass $environmentTarget and $furnitureTarget to view
        $data["environmentTarget"] = $environmentTarget;
        $data["furnitureTarget"]   = $furnitureTarget;
            
        // Pass the module mirrors to view
        $data['mirrors'] = Mirror::join("images", "mirrors.image", "=", "images.id")
            ->select([
                "mirrors.id",
                "mirrors.name",
                "mirrors.color_name",
                "images.src as image"
            ])
            ->get();

        // Update $sessionPath to put index in existing furniture target data
        $sessionPath = "{$sessionPath}.data";
        
        /* -------------------- Data flow variable data, that can exist or not, and when not must to be default data -------------------- */

        // print_r($request->session()->get($sessionPath));
        // die();

        // Stores module model data
        $moduleData = array();
            
        // Verifies if the target budget has a pre-selected model
        if ($request->session()->has("{$sessionPath}.model")) { 
            // Get the existant module data
            $moduleData = (object)[
                "id"           => $request->session()->get("{$sessionPath}.model.modelID"),
                "name"         => Module::where("id", $request->session()->get("{$sessionPath}.model.modelID"))
                                    ->select(["name"])
                                    ->first()
                                    ->name,
                "moduleWidth"  => $request->session()->get("{$sessionPath}.model.modelWidth"),
                "moduleHeight" => $request->session()->get("{$sessionPath}.model.modelHeight"),
                "moduleDepth"  => $request->session()->get("{$sessionPath}.model.modelDepth"),
                "doors"        => Module::where("id", $request->session()->get("{$sessionPath}.model.modelID"))
                                    ->select(["doors_amount"])
                                    ->first()
                                    ->doors_amount,
                "image"        => Image::where("id", Module::where("id", 
                                        $request->session()->get("{$sessionPath}.model.modelID"))->select(["image"])->first()->image)
                                    ->select(["src"])
                                    ->first()
                                    ->src
            ];
        }
        // Get the first registered module model in module_models table 
        else {  
            // Get the module data
            $moduleData = Module::join("images", "module_models.image", "=", "images.id")
                ->select([
                    "module_models.id",
                    "module_models.name",
                    "module_models.doors_amount as doors",
                    "module_models.width as moduleWidth",
                    "module_models.height as moduleHeight",
                    "module_models.depth as moduleDepth",
                    "images.src as image"
                ])
                ->first();
        }

        // Add $moduleData to the view data
        $data["moduleData"] = $moduleData;

        // Verifies if the user already has selected a finishing option
        if ($request->session()->has("{$sessionPath}.finishing")) {
            // Switch in finishing option
            switch ($request->session()->get("{$sessionPath}.finishing.type")) {
                // No finishing
                case "no-finishing":
                    // Pass $noFinishingChecked to view
                    $data['noFinishingChecked'] = true;

                    // End case
                    break;

                // Right Finishing
                case "right-finishing":
                    // Pass $rightFinishingChecked to view
                    $data['rightFinishingChecked'] = true;

                    // End case
                    break;

                // Left Finishing
                case "left-finishing":
                    // Pass $lightFinishingChecked to view
                    $data['leftFinishingChecked'] = true;

                    // End case
                    break;

                // Both sides Finishing
                case "both-finishing":
                    // Pass $bothFinishingChecked to view
                    $data['bothFinishingChecked'] = true;

                    // End case
                    break;
            }

            // Pass finishing width to view
            $data["finishingWidth"] = $request->session()->get("{$sessionPath}.finishing.width");
        } else {
            // Pass $noFinishingChecked to view, to automatic check no-finishing option
            $data['noFinishingChecked'] = true;

            // Pass default finishing width as 70 to view
            $data["finishingWidth"] = 70;
        }

        // Verifies if the user was setted internal thickness
        if ($request->session()->has("{$sessionPath}.thickness")) {
            // Verifies which is the setted internal thickness
            if ($request->session()->get("{$sessionPath}.thickness.internal") == 18) {
                // Pass medium internal thickness as default checked to view
                $data['mediumInternalThickness'] = true;
            } else if ($request->session()->get("{$sessionPath}.thickness.internal") == 25) {
                // Pass large internal thickness as default checked to view
                $data['largeInternalThickness'] = true;
            } else {
                // Pass default internal thickness as default checked to view
                $data['defaultInternalThickness'] = true;
            }

            // Pass the thickness value
            $data["internalThickness"] = $request->session()->get("{$sessionPath}.thickness.internal");
        } else {
            // Pass default internal thickness as default checked to view
            $data['defaultInternalThickness'] = true;

            // Pass the thickness value
            $data["internalThickness"] = 15;
        }

        // Verifies if the user was setted external thickness
        if ($request->session()->has("{$sessionPath}.thickness.external")) {
            // Verifies which is the setted external thickness
            if ($request->session()->get("{$sessionPath}.thickness.external") == 18) {
                // Pass medium external thickness as default checked to view
                $data['mediumExternalThickness'] = true;
            } else if ($request->session()->has("{$sessionPath}.thickness.external") == 25) {
                // Pass large external thickness as default checked to view
                $data['largeExternalThickness'] = true;
            } else {
                // Pass default external thickness as default checked to view
                $data['defaultExternalThickness'] = true;
            }

            // Pass the thickness value
            $data["externalThickness"] = $request->session()->get("{$sessionPath}.thickness.external");
        } else {
            // Pass default external thickness as default checked to view
            $data['defaultExternalThickness'] = true;

            // Pass the thickness value
            $data["externalThickness"] = 15;
        }

        // Verifies if the user was setted shelves thickness
        if ($request->session()->has("{$sessionPath}.thickness.shelves")) {
            // Verifies which is the setted shelves thickness
            if ($request->session()->get("{$sessionPath}.thickness.shelves") == 18) {
                // Pass medium shelves thickness as default checked to view
                $data['mediumShelvesThickness'] = true;
            } else if ($request->session()->has("{$sessionPath}.thickness.shelves") == 25) {
                // Pass large shelves thickness as default checked to view
                $data['largeShelvesThickness'] = true;
            } else {
                // Pass default shelves thickness as default checked to view
                $data['defaultShelvesThickness'] = true;
            }

            // Pass the thickness value
            $data["shelvesThickness"] = $request->session()->get("{$sessionPath}.thickness.shelves");
        } else {
            // Pass default shelves thickness as default checked to view
            $data['defaultShelvesThickness'] = true;

            // Pass the thickness value
            $data["shelvesThickness"] = 15;
        }

        // Verifies if the user was setted mirror doors settings
        if ($request->session()->has("{$sessionPath}.advancedCustomization")) {
            // Pass which mirror doors are checked
            if (in_array(1, $request->session()->get("{$sessionPath}.advancedCustomization.doors"))) {
                // Set mirror in left to true
                $data["mirrorLeft"] = true;
            } if (in_array(2, $request->session()->get("{$sessionPath}.advancedCustomization.doors"))) {
                // Set mirror in middle to true
                $data["mirrorMiddle"] = true;
            } if (in_array(3, $request->session()->get("{$sessionPath}.advancedCustomization.doors"))) {
                // Set mirror in right to true
                $data["mirrorRight"] = true;
            }

            // Pass name and color to view
            $data["mirrorName"] = $request->session()->get("{$sessionPath}.advancedCustomization.mirrorName");
            $data["mirrorCode"]  = $request->session()->get("{$sessionPath}.advancedCustomization.mirrorCode");
        }

        // print_r($data);
        // die();

        // Load the view
        return view("budgets.structure", $data);
    }

    /**
     * Stores structure settings.
     * 
     * @param Request $request
     * 
     * @return redirect
     */
    public function addStructure(Request $request)
    {
        // Get form data
        $modelId           = $request->input('model-id');
        $modelWidth        = intval($request->input('module-width')) / 1000;
        $modelDepth        = intval($request->input('module-depth')) / 1000;
        $modelHeight       = intval($request->input('module-height')) / 1000;
        $finishingType     = $request->input('finishing');
        $finishingWidth    = (!empty($request->input('left-finishing-width')) ? $request->input('left-finishing-width') : $request->input('right-finishing-width'));
        $internalThickness = $request->input('internal-thickness');
        $externalThickness = $request->input('external-thickness');
        $shelvesThickness  = $request->input('shelves-thickness');
        $mirrorDoorsSet    = $request->input('mirror-doors');
        $mirrorDoorsColor  = $request->input('mirror-doors-color');
        $mirrorDoorsCode   = $request->input('mirror-doors-color-code');
        $furnitureTarget   = $request->input('furniture-target');
        $environmentTarget = $request->input('environment-target');

        // Stores the session path to data
        $sessionPath = "newBudget.environment.{$environmentTarget}.furnitures.{$furnitureTarget}.data";

        // Clear existing data to the screen settings in target furniture
        $request->session()->pull("{$sessionPath}.model");
        $request->session()->pull("{$sessionPath}.finishing");
        $request->session()->pull("{$sessionPath}.thickness");
        $request->session()->pull("{$sessionPath}.advancedCustomization");

        // Stores the data to be stores in $sessionPath
        $furnitureData = array(
            // Model data
            "model" => [
                "modelID"     => $modelId,
                "modelWidth"  => $modelWidth,
                "modelHeight" => $modelHeight,
                "modelDepth"  => $modelDepth
            ],

            // Finishing data
            "finishing" => [
                "type"  => $finishingType,
                "width" => $finishingWidth
            ],

            // Thickness data
            "thickness" => [
                "internal" => $internalThickness,
                "external" => $externalThickness,
                "shelves"  => $shelvesThickness
            ]
        );

        // Verifies the mirror doors settings
        if (!is_null($mirrorDoorsSet) && count($mirrorDoorsSet) > 0 && !is_null($mirrorDoorsColor)) {
            // Adds the mirror doors configurations to furniture data
            $furnitureData["advancedCustomization"] = array(
                "doors"       => $mirrorDoorsSet,
                "mirrorName"  => $mirrorDoorsColor,
                "mirrorColor" => $mirrorDoorsCode
            );
        }

        // die(print_r($furnitureData));

        // Set session target structure data to $furnitureData indexes
        $request->session()->put("{$sessionPath}.model", $furnitureData["model"]);
        $request->session()->put("{$sessionPath}.finishing", $furnitureData["finishing"]);
        $request->session()->put("{$sessionPath}.thickness", $furnitureData["thickness"]);
        (isset($furnitureData["advancedCustomization"]) ? $request->session()->put("{$sessionPath}.advancedCustomization", $furnitureData["advancedCustomization"]) : '');

        // Reset environment and furniture target to avoid session to delete herself
        $request->session()->put("environmentTarget", $environmentTarget);
        $request->session()->put("furnitureTarget", $furnitureTarget);

        // Verifies if exists a param that set redirect page to this screen to overview
        if ($request->session()->has("backToOverview")) {
            // Delete this session
            $request->session()->pull("backToOverview");

            // Redirect to the budget overview
            return redirect(route('new-budget-overview'));
        } else {
            // Redirect to the next screen
            return redirect(route('new-budget-appearance'));
        }
    }

    /**
     * Loads the appearance screen to the target furniture.
     * 
     * @param Request $request
     * 
     * @return mixed
     */
    public function appearance(Request $request)
    {
        /* -------------------- Session validations -------------------- */

        // Verifies if client data and budgetTarget was setted
        if (!$request->session()->has('clientData') || !$request->session()->has('environmentTarget')) {
            // Redirect to add client screen
            return redirect(route('new-budget-client'));
        }
        
        // Get the environment target
        $environmentTarget = $request->session()->get('environmentTarget');
        
        // Get the existing session path, with index in target environment
        $sessionPath = "newBudget.environment.{$environmentTarget}";
        
        // Verifies if the environment to this target was setted
        if (!$request->session()->has($sessionPath) || is_null($request->session()->get("{$sessionPath}.environmentID"))) {
            // Redirect to environment selection screen
            return redirect(route('new-budget-initial'));
        }
        
        // Verifies if furnitureTarget was setted
        if ($request->session()->has("furnitureTarget") !== true) {
            // Redirect to furniture selection screen
            return redirect(route('new-budget-furniture'));
        }
        
        // Get the furniture target
        $furnitureTarget = $request->session()->get("furnitureTarget");
        
        // Set $sessionPath to index in furniture target
        $sessionPath = "{$sessionPath}.furnitures.{$furnitureTarget}";
        
        // Verifies if the furniture to this target was setted
        if (!$request->session()->has("{$sessionPath}.furniture") || is_null($request->session()->get("{$sessionPath}.furniture.furnitureID"))) {
            // Redirect to environment selection view
            return redirect(route('new-budget-furniture'));
        }

        // Verifies if the furniture structure settings to this furniture was setted
        if (!$request->session()->has("{$sessionPath}.data") || is_null($request->session()->get("{$sessionPath}.data"))) {
            // Redirect to furniture structure view
            return redirect(route('new-budget-structure'));
        }
        
        // Stores view data
        $data = array();

        /* -------------------- View default data -------------------- */
        
        // Pass default colors to view
        $data["colors"] = Color::select(["id", "name", "code"])->get();

        // Pass pullers data to view
        $data["pullers"] = Puller::join("images", "pullers.image", "=", "images.id")
            ->select([
                "pullers.id", 
                "pullers.name", 
                "pullers.sub_alias as alias", 
                "images.src as image"
            ])
            ->get();

        // Pass tape image header path to view
        $data["tapeImage"] = "assets/tape.png";

        // Stores app default sliders
        $sliders = Slider::join("images", "sliders.image", "=", "images.id")
            ->select([
                "sliders.id", 
                "sliders.name", 
                "sliders.type", 
                "images.src as image"
            ])
            ->get();

        // Stores the grouped sliders
        $slidersData = array();

        // Loop in $sliders
        foreach ($sliders as $index => $slider) {
            // Add the slider data to his type index in $slidersData
            $slidersData[$slider->type][] = $slider;
        }

        // Pass grouped sliders to view
        $data["sliders"] = $slidersData;

        // Pass app default shelves supports to view
        $data["supports"] = ShelfSupport::join("images", "shelf_supports.image", "=", "images.id")
            ->select([
                "shelf_supports.id",
                "shelf_supports.name",
                "shelf_supports.sub_alias as alias",
                "images.src as image"
            ])
            ->get();

        // Pass default puller colors to view
        $data["pullerColors"] = [
            (object)["name" => "Anodizado", "image" => "uploads/pullers/anodizado.png"],
            (object)["name" => "Champagne", "image" => "uploads/pullers/champagne.png"],
            (object)["name" => "Preto", "image" => "uploads/pullers/preto.png"]
        ];

        // Pass environment and furniture target to the view
        $data["environmentTarget"] = $environmentTarget;
        $data["furnitureTarget"] = $furnitureTarget;

        // Set $sessionPath to furniture data
        $sessionPath = "{$sessionPath}.data";

        /* -------------------- Data flow variable data, that can exist or not, and when not must to be default data -------------------- */

        // Verifies if exists moduleColor config
        if ($request->session()->has("{$sessionPath}.moduleColor")) {
            // Pass the moduleColor data to view
            $data["moduleInternalColor"] = $request->session()->get("{$sessionPath}.moduleColor.internal");
            $data["moduleExternalColor"] = $request->session()->get("{$sessionPath}.moduleColor.external");

            // Verifies if custom colors was setted
            if (!empty($request->session()->get("{$sessionPath}.moduleColor.externalCode"))) {
                // Pass external custom color to view
                $data["moduleExternalCode"] = $request->session()->get("{$sessionPath}.moduleColor.externalCode");
            }

            if (!empty($request->session()->get("{$sessionPath}.moduleColor.internalCode"))) {
                // Pass internal custom color to view
                $data["moduleInternalCode"] = $request->session()->get("{$sessionPath}.moduleColor.internalCode");
            }
        } else {
            // Pass default colors to view
            $data["moduleInternalColor"]     = $data["colors"][0]->name;
            $data["moduleInternalColorCode"] = $data["colors"][0]->code;
            $data["moduleExternalColor"]     = $data["colors"][0]->name;
            $data["moduleExternalColorCode"] = $data["colors"][0]->code;
        }

        // Verifies if exists tapeColor settings
        if ($request->session()->has("{$sessionPath}.tapeColor")) {
            // Pass the tapeColor data to view
            $data["tapeInternalColor"] = $request->session()->get("{$sessionPath}.tapeColor.internal");
            $data["tapeExternalColor"] = $request->session()->get("{$sessionPath}.tapeColor.external");

            // Verifies if custom colors was setted
            if (!empty($request->session()->get("{$sessionPath}.tapeColor.externalCode"))) {
                // Pass external custom color to view
                $data["tapeExternalCode"] = $request->session()->get("{$sessionPath}.tapeColor.externalCode");
            }

            if (!empty($request->session()->get("{$sessionPath}.tapeColor.internalCode"))) {
                // Pass internal custom color to view
                $data["tapeInternalCode"] = $request->session()->get("{$sessionPath}.tapeColor.internalCode");
            }
        } else {
            // Pass default colors to view
            $data["tapeInternalColor"]     = $data["colors"][0]->name;
            $data["tapeInternalColorCode"] = $data["colors"][0]->code;
            $data["tapeExternalColor"]     = $data["colors"][0]->name;
            $data["tapeExternalColorCode"] = $data["colors"][0]->code;
        }

        // Verifies if user was setted support to shelves
        if ($request->session()->has("{$sessionPath}.shelvesSupportId")) {
            // Pass support id to view
            $data["supportId"] = $request->session()->get("{$sessionPath}.shelvesSupportId");
        } else {
            // Pass default support id to view
            $data["supportId"] = 1;
        }

        // Pass support data to view
        $data["supportData"] = ShelfSupport::join("images", "shelf_supports.image", "=", "images.id")
            ->select([
                "name",
                "images.src as image"
            ])
            ->where("shelf_supports.id", $data["supportId"])
            ->first();

        // Verifies if the user was setted a slider
        if ($request->session()->has("{$sessionPath}.sliderId")) {
            // Pass slider id to view
            $data["sliderId"] = $request->session()->get("{$sessionPath}.sliderId");
        } else {
            // Pass default slider id to view
            $data["sliderId"] = 1;
        }

        // Pass selected slider data to view
        $data["sliderData"] = Slider::join("images", "sliders.image", "=", "images.id")
            ->select([
                "sliders.name",
                "images.src as image"
            ])
            ->where("sliders.id", $data["sliderId"])
            ->first();

        // Verifies if the user was setted a door puller
        if ($request->session()->has("{$sessionPath}.doorsPuller.pullerName")) {
            // Pass puller name to view
            $data["doorPullerName"] = $request->session()->get("{$sessionPath}.doorsPuller.pullerName");

            // Search by the puller image in DB
            $pullerImage = Puller::join("images", "pullers.image", "=", "images.id")
                ->select([
                    "images.src"
                ])
                ->where("pullers.name", $data["doorPullerName"])
                ->first();

            // Verifies if the puller image search was returned results
            if (!empty($pullerImage)) {
                // Pass the image to view
                $data["doorPullerImage"] = $pullerImage->src;
            } else {
                // Pass a empty image src
                $data["doorPullerImage"] = "";
            }
        }
        else {
            // Pass default puller name and image
            $data["doorPullerName"]  = $data["pullers"][0]->name;
            $data["doorPullerImage"] = $data["pullers"][0]->image;
        }
        
        // Verifies if the user was setted a custom color to door pullers
        if ($request->session()->has("{$sessionPath}.doorsPuller.colors.colorName") 
        && !is_null($request->session()->get("{$sessionPath}.doorsPuller.colors.colorCode"))) {
            // Get puller color data and pass to view
            $data["doorPullerColorCode"]  = $request->session()->get("{$sessionPath}.doorsPuller.colors.colorCode");
            $data["doorPullerColorName"]  = $request->session()->get("{$sessionPath}.doorsPuller.colors.colorName");
            $data["doorPullerColorImage"] = "http://www.thecolorapi.com/id?format=svg&named=false&hex=".str_replace("#", "", $data["doorPullerColorCode"]);
        }
        else if ($request->session()->has("{$sessionPath}.doorsPuller.colors.colorName")) {
            // Get puller color data and pass to view
            $data["doorPullerColorName"] = $request->session()->get("{$sessionPath}.doorsPuller.colors.colorName");
            
            // Verifies which image use
            if ($data["pullerColors"][0]->name == $data["doorPullerColorName"]) {
                $data["doorPullerColorImage"] = $data["pullerColors"][0]->image;
            }
            else if ($data["pullerColors"][1]->name == $data["doorPullerColorName"]) {
                $data["doorPullerColorImage"] = $data["pullerColors"][0]->image;
            }
            else if ($data["pullerColors"][2]->name == $data["doorPullerColorName"]) {
                $data["doorPullerColorImage"] = $data["pullerColors"][0]->image;
            }
        }
        else {
            // Pass default selected colors to view
            $data["doorPullerColorName"]  = $data["pullerColors"][0]->name;
            $data["doorPullerColorImage"] = "/".$data["pullerColors"][0]->image;
        }

        // Verifies if the user was setted no puller option
        if ($request->session()->has("{$sessionPath}.doorsPuller.noPuller")
            && $request->session()->get("{$sessionPath}.doorsPuller.noPuller") == true) {
            // Set all doors puller configs to no-puller
            $data["doorPullerName"]       = "SEM Puxador";
            $data["doorPullerImage"]      = "uploads/generic/no-puller.png";
            $data["doorPullerColorName"]  = "SEM Puxador";
            $data["doorPullerColorImage"] = "/uploads/generic/no-puller.png";
            $data["doorNoPuller"]         = true;
        }

        // Verifies if the user was setted a door puller
        if ($request->session()->has("{$sessionPath}.drawersPuller.drawerName")) {
            // Pass puller name to view
            $data["drawerPullerName"] = $request->session()->get("{$sessionPath}.drawersPuller.drawerName");

            // Search by the puller image in DB
            $pullerImage = Puller::join("images", "pullers.image", "=", "images.id")
                ->select([
                    "images.src"
                ])
                ->where("pullers.name", $data["drawerPullerName"])
                ->first();

            // Verifies if the puller image search was returned results
            if (!empty($pullerImage)) {
                // Pass the image to view
                $data["drawerPullerImage"] = $pullerImage->src;
            } else {
                // Pass a empty image src
                $data["drawerPullerImage"] = "";
            }
        }
        else {
            // Pass default puller name and image
            $data["drawerPullerName"]  = $data["pullers"][0]->name;
            $data["drawerPullerImage"] = $data["pullers"][0]->image;
        }

        // Verifies if the user was setted a custom color to door pullers
        if ($request->session()->has("{$sessionPath}.drawersPuller.colors.colorName") 
            && !is_null($request->session()->get("{$sessionPath}.drawersPuller.colors.colorCode"))) {
            // Get puller color data and pass to view
            $data["drawerPullerColorCode"]  = $request->session()->get("{$sessionPath}.drawersPuller.colors.colorCode");
            $data["drawerPullerColorName"]  = $request->session()->get("{$sessionPath}.drawersPuller.colors.colorName");
            $data["drawerPullerColorImage"] = "http://www.thecolorapi.com/id?format=svg&named=false&hex=".str_replace("#", "", $data["drawerPullerColorCode"]);
        }
        else if ($request->session()->has("{$sessionPath}.drawersPuller.colors.colorName")) {
            // Get puller color data and pass to view
            $data["drawerPullerColorName"] = $request->session()->get("{$sessionPath}.drawersPuller.colors.colorName");
            
            // Verifies which image use
            if ($data["pullerColors"][0]->name == $data["drawerPullerColorName"]) {
                $data["drawerPullerColorImage"] = "/".$data["pullerColors"][0]->image;
            }
            else if ($data["pullerColors"][1]->name == $data["drawerPullerColorName"]) {
                $data["drawerPullerColorImage"] = "/".$data["pullerColors"][1]->image;
            }
            else if ($data["pullerColors"][2]->name == $data["drawerPullerColorName"]) {
                $data["drawerPullerColorImage"] = "/".$data["pullerColors"][2]->image;
            }
        }
        else {
            // Pass default selected colors to view
            $data["drawerPullerColorName"]  = $data["pullerColors"][0]->name;
            $data["drawerPullerColorImage"] = "/".$data["pullerColors"][0]->image;
        }

        // Verifies if the user was setted no puller option
        if ($request->session()->has("{$sessionPath}.drawersPuller.noPuller")
            && $request->session()->get("{$sessionPath}.drawersPuller.noPuller") == true) {
            // Set all doors puller configs to no-puller
            $data["drawerPullerName"]       = "SEM Puxadore";
            $data["drawerPullerImage"]      = asset("uploads/generic/no-puller.png");
            $data["drawerPullerColorName"]  = "SEM Puxador";
            $data["drawerPullerColorImage"] = asset("uploads/generic/no-puller.png");
            $data["drawerNoPuller"]         = true;
        }

        // die(print_r($data));
        
        // Load the view
        return view("budgets.appearance", $data);
    }

    /**
     * Stores appearance settings.
     * 
     * @param Request $request
     * 
     * @return redirect
     */
    public function addAppearance(Request $request)
    {
        // Get form data
        $moduleInternalColor     = $request->input('module-internal-color');
        $moduleExternalColor     = $request->input('module-external-color');
        $moduleInternalColorCode = $request->input('module-internal-color-code');
        $moduleExternalColorCode = $request->input('module-external-color-code');
        $tapeInternalColor       = $request->input('tape-internal-color');
        $tapeExternalColor       = $request->input('tape-external-color');
        $tapeInternalColorCode   = $request->input('tape-internal-color-code');
        $tapeExternalColorCode   = $request->input('tape-external-color-code');
        $doorPullerName          = $request->input('door-puller-name');
        $doorPullerColorName     = $request->input('door-puller-color-name');
        $doorPullerColorCode     = $request->input('door-puller-color-code');
        $doorNoPuller            = (is_null($request->input('door-no-puller')) ? false : true);
        $drawerPullerName        = $request->input('drawer-puller-name');
        $drawerPullerColorName   = $request->input('drawer-puller-color-name');
        $drawerPullerColorCode   = $request->input('drawer-puller-color-code');
        $drawerNoPuller          = (is_null($request->input('drawer-no-puller')) ? false : true);
        $sliderId                = $request->input('slider-id');
        $shelveSupportId         = $request->input('shelve-support-id');
        $environmentTarget       = $request->input('environment-target');
        $furnitureTarget         = $request->input('furniture-target');

        // Stores the session path to data
        $sessionPath = "newBudget.environment.{$environmentTarget}.furnitures.{$furnitureTarget}.data";

        // Clear existing data to the screen settings in target furniture
        $request->session()->pull("{$sessionPath}.moduleColor");
        $request->session()->pull("{$sessionPath}.tapeColor");
        $request->session()->pull("{$sessionPath}.doorsPuller");
        $request->session()->pull("{$sessionPath}.drawersPuller");
        $request->session()->pull("{$sessionPath}.sliderId");
        $request->session()->pull("{$sessionPath}.shelvesSupportId");

        // Stores the data to be stores in $sessionPath
        $furnitureData = array(
            // Module Colors
            "moduleColor" => [
                "internal"     => $moduleInternalColor,
                "internalCode" => $moduleInternalColorCode,
                "external"     => $moduleExternalColor,
                "externalCode" => $moduleExternalColorCode
            ],

            // Tape Colors
            "tapeColor" => [
                "internal"     => $tapeInternalColor,
                "internalCode" => $tapeInternalColorCode,
                "external"     => $tapeExternalColor,
                "externalCode" => $tapeExternalColorCode
            ],

            // Doors pullers
            "doorsPuller" => [
                "pullerName" => $doorPullerName,
                "colors"     => [
                    "colorName" => $doorPullerColorName,
                    "colorCode" => $doorPullerColorCode
                ],
                "noPuller" => $doorNoPuller
            ],

            // Drawers pullers
            "drawersPuller" => [
                "drawerName" => $drawerPullerName,
                "colors"     => [
                    "colorName" => $drawerPullerColorName,
                    "colorCode" => $drawerPullerColorCode
                ],
                "noPuller" => $drawerNoPuller
            ],

            // Slider id
            "sliderId" => $sliderId,

            // Shelves support id
            "shelvesSupportId" => $shelveSupportId
        );

        // Put the new data in session
        $request->session()->put("{$sessionPath}.moduleColor", $furnitureData["moduleColor"]);
        $request->session()->put("{$sessionPath}.tapeColor", $furnitureData["tapeColor"]);
        $request->session()->put("{$sessionPath}.doorsPuller", $furnitureData["doorsPuller"]);
        $request->session()->put("{$sessionPath}.drawersPuller", $furnitureData["drawersPuller"]);
        $request->session()->put("{$sessionPath}.sliderId", $furnitureData["sliderId"]);
        $request->session()->put("{$sessionPath}.shelvesSupportId", $furnitureData["shelvesSupportId"]);

        // Redirect to the next screen
        return redirect(route('new-budget-overview'));
    }

    /**
     * Loads budget overview, when it is not created yet.
     * 
     * @param Request $request
     * 
     * @return mixed
     */
    public function overview(Request $request)
    {
        /* -------------------- Session validations -------------------- */

        // Verifies if client data and budgetTarget was setted
        if (!$request->session()->has('clientData') || !$request->session()->has('environmentTarget')) {
            // Redirect to add client screen
            return redirect(route('new-budget-client'));
        }
        
        // Get the environment target
        $environmentTarget = $request->session()->get('environmentTarget');
        
        // Get the existing session path, with index in target environment
        $sessionPath = "newBudget.environment.{$environmentTarget}";
        
        // Verifies if the environment to this target was setted
        if (!$request->session()->has($sessionPath) || is_null($request->session()->get("{$sessionPath}.environmentID"))) {
            // Redirect to environment selection screen
            return redirect(route('new-budget-initial'));
        }
        
        // Verifies if furnitureTarget was setted
        if ($request->session()->has("furnitureTarget") !== true) {
            // Redirect to furniture selection screen
            return redirect(route('new-budget-furniture'));
        }
        
        // Get the furniture target
        $furnitureTarget = $request->session()->get("furnitureTarget");
        
        // Set $sessionPath to index in furniture target
        $sessionPath = "{$sessionPath}.furnitures.{$furnitureTarget}";
        
        // Verifies if the furniture to this target was setted
        if (!$request->session()->has("{$sessionPath}.furniture") || is_null($request->session()->get("{$sessionPath}.furniture.furnitureID"))) {
            // Redirect to environment selection view
            return redirect(route('new-budget-furniture'));
        }

        // Verifies if the furniture structure settings to this furniture was setted
        if (!$request->session()->has("{$sessionPath}.data") || is_null($request->session()->get("{$sessionPath}.data"))) {
            // Redirect to furniture structure view
            return redirect(route('new-budget-structure'));
        }
        
        // Stores view data
        $data = array();

        // Stores current budget data
        $budgetData = $request->session()->get("newBudget");

        // die(var_dump($budgetData));
        // $request->session()->put("newBudget.environment.0.furnitures.0.furniture.furnitureAlias", "Teste");

        /* -------------------- View default data -------------------- */
        
        return view("budgets.no-completed-overview", $budgetData);
    }

    /**
     * Saves the budget and send the email with data.
     * 
     * @param Request $request
     * 
     * @return redirect
     */
    public function create(Request $request)
    {
        // Stores default colors (ungroupped)
        $colorsUngroup = Color::select(["name"])->get();
        
        // Stores groupped default colors
        $colors = array();

        // Loop in $colorsUngroup to group them
        foreach ($colorsUngroup as $color) {
            // Add the color name to $colors
            array_push($colors, $color->name);
        }

        // Stores session data
        $budgetData = $request->session()->get("newBudget.environment");

        // Stores view data individual parts
        $clientData            = array();
        $environments          = array();
        $modules               = array();
        $modulesData           = array();
        $advancedCustomization = array();
        $moduleSizes           = array();
        $leftFinishings        = array();
        $rightFinishings       = array();
        $thicknessData         = array();
        $moduleColors          = array();
        $tapeColors            = array();
        $doorsPullers          = array();
        $drawersPullers        = array();
        $extras                = array();

        // Get client data
        $clientData = (object)[
            "user"  => Auth::user()->name,
            "email" => Auth::user()->email
        ];
        
        // Loop controller
        $e = 1;

        // Loop budget data
        foreach ($budgetData as $envTarget => $environmentData) {
            // Add this environment to $environments
            array_push($environments, (object)[
                "target" => $envTarget+1,
                "name"   => $environmentData["environmentName"]
            ]);


            // Loop in environmentData to get furnitures
            foreach ($environmentData["furnitures"] as $furnitureTarget => $furnitureData) {
                // Shortuct to $furnitureData["data]
                $furniture = $furnitureData["data"];

                /* ---------------------------------- Module Structure ---------------------------------- */

                // Add this furniture to $modules
                array_push($modules, (object)[
                    "target"    => $e,
                    "envTarget" => $envTarget+1,
                    "name"      => $furnitureData["furniture"]["furnitureAlias"]
                ]);

                // Get module data
                array_push($modulesData, (object)[
                    "name"      => Module::select("name")->where("id", $furniture["model"]["modelID"])->first()->name,
                    "left"      => ($furniture["finishing"]["type"] == "left-finshing" || $furniture["finishing"]["type"] == "both-finishing" ? "Sim" : "Não"),
                    "right"     => ($furniture["finishing"]["type"] == "right-finshing" || $furniture["finishing"]["type"] == "both-finishing" ? "Sim" : "Não"),
                    "target"    => $e,
                    "envTarget" => $envTarget+1
                ]);

                // Verifies if the user was setted mirror doors
                if (isset($furniture["advancedCustomization"])) {
                    // Stores the mirror set
                    $mirrorDoorsSet = "";

                    // Verifies if user was setted a mirror in left door
                    if (in_array("1", $furniture["advancedCustomization"]["doors"])) {
                        // Add 'ESQUERDA' to $mirrorDoorsSet, with a comma if needed
                        $mirrorDoorsSet = (strlen($mirrorDoorsSet) > 0 ? "{$mirrorDoorsSet}, ESQUERDA" : "ESQUERDA");
                    }

                    // Verifies if user was setted a mirror in center door
                    if (in_array("2", $furniture["advancedCustomization"]["doors"])) {
                        // Add 'CENTRO' to $mirrorDoorsSet, with a comma if needed
                        $mirrorDoorsSet = (strlen($mirrorDoorsSet) > 0 ? "{$mirrorDoorsSet}, CENTRO" : "CENTRO");
                    }

                    // Verifies if user was setted a mirror in right door
                    if (in_array("3", $furniture["advancedCustomization"]["doors"])) {
                        // Add 'DIREITA' to $mirrorDoorsSet, with a comma if needed
                        $mirrorDoorsSet = (strlen($mirrorDoorsSet) > 0 ? "{$mirrorDoorsSet}, DIREITA" : "DIREITA");
                    }

                    // Get module advanced customization settings
                    array_push($advancedCustomization, (object)[
                        "doors"       => (strlen($mirrorDoorsSet) > 0 ? $mirrorDoorsSet : "Não"),
                        "mirror"      => (strlen($mirrorDoorsSet) > 0 ? $furniture["advancedCustomization"]["mirrorName"] : "Não"),
                        "mirrorColor" => (strlen($mirrorDoorsSet) > 0 && !empty($furniture["advancedCustomization"]["mirrorColor"]) && !is_null($furniture["advancedCustomization"]["mirrorColor"]) ? $furniture["advancedCustomization"]["mirrorColor"] : "Não"),
                        "target"      => $e,
                        "envTarget"   => $envTarget+1
                    ]);
                } else {
                    // Add a line specifying NO mirrors
                    array_push($advancedCustomization, (object)[
                        "doors"       => "Não",
                        "mirror"      => "Não",
                        "mirrorColor" => "Não",
                        "target"      => $e,
                        "envTarget"   => $envTarget+1
                    ]);
                }

                // Get module sizes
                array_push($moduleSizes, (object)[
                    "width"     => floatval($furniture["model"]["modelWidth"]) * 1000,
                    "height"    => floatval($furniture["model"]["modelHeight"]) * 1000,
                    "depth"     => floatval($furniture["model"]["modelDepth"]) * 1000,
                    "target"    => $e,
                    "envTarget" => $envTarget+1
                ]);

                // Verifies if the user was setted both or left finishings
                if ($furniture["finishing"]["type"] == "left-finishing" || $furniture["finishing"]["type"] == "both-finishing") {
                    // Get finishing configuration
                    array_push($leftFinishings, (object)[
                        "width"     => $furniture["finishing"]["width"],
                        "height"    => floatval($furniture["model"]["modelHeight"]) * 1000,
                        "depth"     => floatval($furniture["model"]["modelDepth"]) * 1000,
                        "target"    => $e,
                        "envTarget" => $envTarget+1
                    ]);
                } else {
                    // Pass left finishing configuration as NO finishing
                    array_push($leftFinishings, (object)[
                        "width"     => "Não",
                        "height"    => "Não",
                        "depth"     => "Não",
                        "target"    => $e,
                        "envTarget" => $envTarget+1
                    ]);
                }

                // Verifies if the user was setted both or right finishings
                if ($furniture["finishing"]["type"] == "right-finishing" || $furniture["finishing"]["type"] == "both-finishing") {
                    // Get finishing configuration
                    array_push($rightFinishings, (object)[
                        "width"     => $furniture["finishing"]["width"],
                        "height"    => floatval($furniture["model"]["modelHeight"]) * 1000,
                        "depth"     => floatval($furniture["model"]["modelDepth"]) * 1000,
                        "target"    => $e,
                        "envTarget" => $envTarget+1
                    ]);
                } else {
                    // Pass right finishing configuration as NO finishing
                    array_push($rightFinishings, (object)[
                        "width"     => "Não",
                        "height"    => "Não",
                        "depth"     => "Não",
                        "target"    => $e,
                        "envTarget" => $envTarget+1
                    ]);
                }

                // Get module thickness
                array_push($thicknessData, (object)[
                    "internal"  => $furniture["thickness"]["internal"],
                    "external"  => $furniture["thickness"]["external"],
                    "shelves"   => $furniture["thickness"]["shelves"],
                    "target"    => $e,
                    "envTarget" => $envTarget+1
                ]);

                /* ---------------------------------- Module Appearance ---------------------------------- */

                // Get module colors
                array_push($moduleColors, (object)[
                    "internalName" => $furniture["moduleColor"]["internal"],
                    "internalCode" => (!in_array($furniture["moduleColor"]["internal"], $colors) ? "({$furniture["moduleColor"]["internalCode"]})" : ""),
                    "externalName" => $furniture["moduleColor"]["external"],
                    "externalCode" => (!in_array($furniture["moduleColor"]["external"], $colors) ? "({$furniture["moduleColor"]["externalCode"]})" : ""),
                    "target"       => $e,
                    "envTarget"    => $envTarget+1
                ]);

                // Get tape colors
                array_push($tapeColors, (object)[
                    "internalName" => $furniture["tapeColor"]["internal"],
                    "internalCode" => (!in_array($furniture["tapeColor"]["internal"], $colors) ? "({$furniture["tapeColor"]["internalCode"]})" : ""),
                    "externalName" => $furniture["tapeColor"]["external"],
                    "externalCode" => (!in_array($furniture["tapeColor"]["external"], $colors) ? "({$furniture["tapeColor"]["externalCode"]})" : ""),
                    "target"       => $e,
                    "envTarget"    => $envTarget+1
                ]);

                // Verifies if the user was setted no doors pullers
                if ($furniture["doorsPuller"]["noPuller"] !== true) {
                    // Get doors pullers
                    array_push($doorsPullers, (object)[
                        "name"      => $furniture["doorsPuller"]["pullerName"],
                        "colorName" => $furniture["doorsPuller"]["colors"]["colorName"],
                        "colorCode" => (
                            $furniture["doorsPuller"]["colors"]["colorCode"] == "Champagne" 
                                || $furniture["doorsPuller"]["colors"]["colorName"] == "Anodizado" 
                                || $furniture["doorsPuller"]["colors"]["colorName"] == "Preto"
                            ? "Não"
                            : $furniture["doorsPuller"]["colors"]["colorCode"]
                        ),
                        "target"    => $e,
                        "envTarget" => $envTarget+1
                    ]);
                } else {
                    // Pass doors pullers set as NO PULLER
                    array_push($doorsPullers, (object)[
                        "name"      => "SEM PUXADOR",
                        "colorName" => "SEM PUXADOR",
                        "colorCode" => "SEM PUXADOR",
                        "target"    => $e,
                        "envTarget" => $envTarget+1
                    ]);
                }

                // Verifies if the user was setted no drawers pullers
                if ($furniture["drawersPuller"]["noPuller"] !== true) {
                    // Get drawers pullers
                    array_push($drawersPullers, (object)[
                        "name"      => $furniture["drawersPuller"]["drawerName"],
                        "colorName" => $furniture["drawersPuller"]["colors"]["colorName"],
                        "colorCode" => (
                            $furniture["drawersPuller"]["colors"]["colorCode"] == "Champagne" 
                                || $furniture["drawersPuller"]["colors"]["colorName"] == "Anodizado" 
                                || $furniture["drawersPuller"]["colors"]["colorName"] == "Preto"
                            ? "Não"
                            : $furniture["drawersPuller"]["colors"]["colorCode"]
                        ),
                        "target"    => $e,
                        "envTarget" => $envTarget+1
                    ]);
                } else {
                    // Pass drawers pullers set as NO PULLER
                    array_push($drawersPullers, (object)[
                        "name"      => "SEM PUXADOR",
                        "colorName" => "SEM PUXADOR",
                        "colorCode" => "SEM PUXADOR",
                        "target"    => $e,
                        "envTarget" => $envTarget+1
                    ]);
                }

                // Extras (sliders and shelves supports)
                array_push($extras, (object)[
                    "slider"    => Slider::select("name")->where("id", $furniture["sliderId"])->first()->name,
                    "support"   => ShelfSupport::select("name")->where("id", $furniture["shelvesSupportId"])->first()->name,
                    "target"    => $e,
                    "envTarget" => $envTarget+1
                ]);

                // Increments loop controller
                $e++;
            }
        }

        // Pass the data to email
        $data = array(
            "clientData"            => $clientData,
            "environments"          => $environments,
            "modules"               => $modules,
            "modulesData"           => $modulesData,
            "advancedCustomization" => $advancedCustomization,
            "moduleSizes"           => $moduleSizes,
            "leftFinishings"        => $leftFinishings,
            "rightFinishings"       => $rightFinishings,
            "thicknessData"         => $thicknessData,
            "moduleColors"          => $moduleColors,
            "tapeColors"            => $tapeColors,
            "doorsPullers"          => $doorsPullers,
            "drawersPullers"        => $drawersPullers,
            "extras"                => $extras
        );

        // Try to send the budget data email and stores budget
        try {
            // Send the budget data email
            Mail::to(env("EMAIL_BUDGET_TARGET"))
                ->send(new SendBudgetDataToAdmin($data));

            // Stores client id
            $clientId = 0;

            // Search by the client
            $clientSearch = Client::verifyClientExists($request->session()->get("clientData.1"), Auth::id());

            // Verifies if the client exists
            if ($clientSearch === false) {
                // Stores client model instance
                $client = new Client;

                // Set client data
                $client->name    = $request->session()->get("clientData.0");
                $client->email   = $request->session()->get("clientData.1");
                $client->phone   = $request->session()->get("clientData.2");
                $client->address = $request->session()->get("clientData.3");

                // Save the client
                $client->save();

                // Get the client id
                $clientId = $client->id;
            } else {
                // Set to $clientId to retrieved id
                $clientId = $clientSearch->id;
            }

            // Save the budget in database
            DB::table('budgets')
                ->insert([
                    "user"   => Auth::user()->id,
                    "client" => $clientId,
                    "data"   => json_encode($request->session()->get("newBudget"))
                ]);

            // Clear all budget/target/client session data
            $request->session()->pull("newBudget");
            $request->session()->pull("environmentTarget");
            $request->session()->pull("furnitureTarget");
            $request->session()->pull("clientData");

            // Redirect user to home with success message alert
            return redirect('/home')->with(["message" => "Orçamento registrado com sucesso! Em cerca de 1 hora você receberá uma mensagem em seu email com a resposta."]);
        } catch (Exception $e) {
            // Debug error message
            // die(var_dump($e->getMessage()));

            // Redirect user to home with error message alert
            return redirect('/home')->with(["message" => "Ocorreu um erro. Contate o administrador."]);
        }
    }

    /**
     * Recovery a user already created budget.
     * 
     * @param int $id
     * 
     * @return mixed
     */
    public function getOverview(int $id)
    {
        // Search by a budget with provided id, created by current user
        $budgetData = DB::table('budgets')
            ->select("data")
            ->where("id", $id)
            ->where("user", Auth::id())
            ->first();

        // Verifies if budget search was returned results
        if (!empty($budgetData)) {
            // Loads the overview with decoded JSON data
            return view("budgets.completed-overview", ['environment' => json_decode($budgetData->data, true)["environment"]]);
        } else {
            // Render 404 view
            abort('404');
        }
    }

    /**
     * Loads the view that allow the user to create a new ironwares-only budget.
     * 
     * @param Request $request
     * 
     * @return mixed
     */
    public function ironwaresOnlyView(Request $request)
    {
        // Verifies if client data was setted
        if (!$request->session()->has('clientData')) {
            // Redirect to add client screen
            return redirect(route('new-budget-client'));
        }

        // Loads the view
        return view("budgets.alternative.new-ironwares-only-budget");
    }

    /**
     * Creates a new ironwares-only budget.
     * 
     * @param Request $request
     * 
     * @return redirect
     */
    public function createIronwaresBudget(Request $request)
    {
        // Get POST data
        $name        = $request->input("name");
        $quantity    = $request->input("quantity");
        $description = $request->input("description");

        // Get current user name and email
        $clientData = (object)[
            "user"  => Auth::user()->name,
            "email" => Auth::user()->email
        ];

        // Stores email data
        $data = array(
            "name"        => $name,
            "quantity"    => $quantity,
            "description" => $description,
            "clientData"  => $clientData
        );

        // Try to send admin email
        try {
            // Send budget data email to admin
            Mail::to(env("EMAIL_BUDGET_TARGET"))
                ->send(new SendIronwaresBudgetDataToAdmin($data));

            // Redirect user to home with success message
            return redirect('/home')->with(["message" => "Orçamento registrado com sucesso! Em cerca de 1 hora você receberá uma mensagem em seu email com a resposta."]);
        } catch (Exception $e) {
            // Show the error
            // die(var_dump($e->getMessage()));

            // Redirect user to home with error message
            return redirect('/home')->with(["message" => "Ocorreu um erro. Contate o administrador"]);
        }
    }

    /**
     * Loads the view that allow the user to create a new pieces-only budget.
     * 
     * @param Request $request
     * 
     * @return mixed
     */
    public function piecesOnlyView(Request $request)
    {
        // Verifies if client data was setted
        if (!$request->session()->has('clientData')) {
            // Redirect to add client screen
            return redirect(route('new-budget-client'));
        }

        // Stores view data
        $data = array();

        // Get the app colors
        $data["colors"] = Color::select("code", "name")->get();

        // Loads the view
        return view("budgets.alternative.new-pieces-only-budget", $data);
    }

    /**
     * Creates a new pieces-only budget.
     * 
     * @param Request $request
     * 
     * @return redirect
     */
    public function createPiecesOnlyBudget(Request $request)
    {
        // Stores default colors (ungroupped)
        $colorsUngroup = Color::select(["name"])->get();
        
        // Stores groupped default colors
        $colors = array();

        // Loop in $colorsUngroup to group them
        foreach ($colorsUngroup as $color) {
            // Add the color name to $colors
            array_push($colors, $color->name);
        }

        // Get POST data
        $name           = $request->input('name');
        $thickness      = $request->input('thickness');
        $quantity       = $request->input('quantity');
        $description    = $request->input('description');
        $tapeC1         = $request->input('tape-c1');
        $tapeC2         = $request->input('tape-c2');
        $tapeL1         = $request->input('tape-l1');
        $tapeL2         = $request->input('tape-l2');
        $pieceColorName = $request->input('piece-color');
        $pieceColorCode = $request->input('piece-color-code');
        $tapeColorName  = $request->input('tape-color');
        $tapeColorCode  = $request->input('tape-color-code');

        // Get current user data
        $clientData = (object)[
            "user"  => Auth::user()->name,
            "email" => Auth::user()->email
        ];

        // Stores email data
        $data = [
            "name"           => $name,
            "thickness"      => $thickness,
            "quantity"       => $quantity,
            "description"    => $description,
            "tapeC1"         => (is_null($tapeC1) ? 'Não' : 'Sim'),
            "tapeC2"         => (is_null($tapeC2) ? 'Não' : 'Sim'),
            "tapeL1"         => (is_null($tapeL1) ? 'Não' : 'Sim'),
            "tapeL2"         => (is_null($tapeL2) ? 'Não' : 'Sim'),
            "pieceColor"     => $pieceColorName,
            "pieceColorCode" => (in_array($pieceColorName, $colors) ? '' : "({$pieceColorCode})"),
            "tapeColor"      => $tapeColorName,
            "tapeColorCode"  => (in_array($tapeColorName, $colors) ? '' : "({$tapeColorCode})"),
            "clientData"     => $clientData
        ];

        // Try to send admin email
        try {
            // Send budget data email to admin
            Mail::to(env("EMAIL_BUDGET_TARGET"))
                ->send(new SendPiecesBudgetDataToAdmin($data));

            // Redirect user to home with success message
            return redirect('/home')->with(["message" => "Orçamento registrado com sucesso! Em cerca de 1 hora você receberá uma mensagem em seu email com a resposta."]);
        } catch (Exception $e) {
            // Show the error
            // die(var_dump($e->getMessage()));

            // Redirect user to home with error message
            return redirect('/home')->with(["message" => "Ocorreu um erro. Contate o administrador"]);
        }
    }


    /**
     * Responds a AJAX request, with user registered clients.
     * 
     * @param Request $request
     * 
     * @return response
     */
    public function ajaxSearchClients(Request $request)
    {
        // Retrieve the user clients
        $clients = Client::getUserClients(Auth::id());

        // Encode to $clients to JSON
        $clients = json_encode($clients);

        // Return $clients as a response to request
        return response()->json($clients);
    }

    /**
     * Prepare session structure to change furniture structure, using data provided by POST.
     * 
     * @param Request $request
     * 
     * @return void
     */
    public function ajaxPrepareStructure(Request $request)
    {
        // Get POST data
        $environmentTarget = $request->input("environmentTarget");
        $furnitureTarget   = $request->input("furnitureTarget");

        // Clear the existing target control sessions
        $request->session()->pull("environmentTarget");
        $request->session()->pull("furnitureTarget");

        // Set the new target control sessions
        $request->session()->put("environmentTarget", $environmentTarget);
        $request->session()->put("furnitureTarget", $furnitureTarget);

        // Set a session specifying to structure screen redirect back to overview
        $request->session()->put("backToOverview", true);
    }

    /**
     * Prepare session structure to change furniture appearance, using data provided by POST.
     * 
     * @param Request $request
     * 
     * @return void
     */
    public function ajaxPrepareAppearance(Request $request)
    {
        // Get POST data
        $environmentTarget = $request->input("environmentTarget");
        $furnitureTarget   = $request->input("furnitureTarget");

        // Clear the existing target control sessions
        $request->session()->pull("environmentTarget");
        $request->session()->pull("furnitureTarget");

        // Set the new target control sessions
        $request->session()->put("environmentTarget", $environmentTarget);
        $request->session()->put("furnitureTarget", $furnitureTarget);
    }

    /**
     * Update a furniture name, with targets passed via AJAX POST.
     * 
     * @param Request $request
     * 
     * @return void
     */
    public function ajaxUpdateFurnitureName(Request $request)
    {
        // Get POST data
        $environmentTarget = $request->input("environmentTarget");
        $furnitureTarget   = $request->input("furnitureTarget");
        $furnitureName     = $request->input("furnitureName");

        // Verifies if the target specified exists
        if ($request->session()->has("newBudget.environment.{$environmentTarget}.furnitures.{$furnitureTarget}")) {
            // Update the furniture name
            $request->session()->put("newBudget.environment.{$environmentTarget}.furnitures.{$furnitureTarget}.furniture.furnitureAlias", $furnitureName);
        }
    }
    
    /**
     * Return new budget current data to AJAX request.
     * 
     * @param Request $request
     * 
     * @return response
     */
    public function ajaxGetBudgetData(Request $request)
    {
        // Get session data
        $sessionData = $request->session()->get("newBudget");

        // Encode session data to JSON
        $sessionData = json_encode($sessionData);

        // Respond the request with JSON data
        return response()->json($sessionData);
    }

    /**
     * Prepare session ambient to add a new furniture, using POST AJAX provided data.
     * 
     * @param Request $request
     * 
     * @return void
     */
    public function ajaxPrepareNewFurniture(Request $request)
    {
        // Get POST data
        $environmentTarget = $request->input("environmentTarget");
        $furnitureTarget   = $request->input("furnitureTarget");

        // Clear the existing target control sessions
        $request->session()->pull("environmentTarget");
        $request->session()->pull("furnitureTarget");

        // Set the new target control sessions
        $request->session()->put("environmentTarget", $environmentTarget);
        $request->session()->put("furnitureTarget", $furnitureTarget);
    }

    /**
     * Prepare session ambient to add a new environment, using POST AJAX provided data.
     * 
     * @param Request $request
     * 
     * @return void
     */
    public function ajaxPrepareNewEnvironment(Request $request)
    {
        // Get POST data
        $environmentTarget = $request->input("environmentTarget");

        // Clear the existing target control sessions
        $request->session()->pull("environmentTarget");
        $request->session()->pull("furnitureTarget");

        // Set the new target control sessions
        $request->session()->put("environmentTarget", $environmentTarget);
        $request->session()->put("furnitureTarget", 0);
    }
}

?>