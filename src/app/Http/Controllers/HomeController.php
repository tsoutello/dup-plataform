<?php

namespace App\Http\Controllers;

use \DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            // Verifies if the allowed user field is 1
            if (Auth::user()->allowed != "1") {
                // Stores logout url
                $logout = route('sair');

                // Alert the user to request access and redirect to loggof
                echo "
                    <script>
                        alert('Seu acesso não foi liberado. Contate o administrador.');
                        window.location.href='{$logout}';
                    </script>
                ";
            } else {
                // Continue with the request
                return $next($request);
            } 
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Stores view data
        $data = array();

        // Look for the user budgets count
        $data['budgetsNum'] = DB::table("budgets")->where('user', Auth::id())->count();
        
        // Verifies if the user has any budget
        if ($data['budgetsNum'] > 0) {
            // Get user budgets data
            $budgets = DB::table('budgets')
                ->join("clients", "budgets.client", "=", "clients.id")
                ->select([
                    "budgets.id",
                    "budgets.created_at",
                    "clients.name as clientName"
                ])
                ->where("budgets.user", Auth::id())
                ->get();

            // Set Brazil timezone
            date_default_timezone_set("America/Sao_Paulo");

            // Loop recovered budgets to format then
            foreach ($budgets as $index => $budget) {
                // Format created_at data
                $createdAt = date("d/m/Y", strtotime($budget->created_at));

                // Instantiate a DateTime object to created at and now dates
                $createdAtDate = new DateTime($budget->created_at);
                $nowDate       = new DateTime(date("Y-m-d H:i:s"));

                // Get the date time object
                $diff = $createdAtDate->diff($nowDate);

                // Verifies if the user was created this budget at least one year ago
                if ($diff->y > 0) {
                    // Set $diff to n years ago
                    $diff = "há {$diff->y} anos";
                }
                // Verifies if the user was created this budget at least one month ago
                else if ($diff->m > 0) {
                    // Set $diff to n years ago
                    $diff = "há {$diff->m} meses";
                }
                // Verifies if the user was created this budget at least one day ago
                else if ($diff->d > 0) {
                    // Set $diff to n days ago
                    $diff = "há {$diff->m} dias";
                }
                // Verifies if the user was created this budget at least one hour ago
                else if ($diff->h > 0) {
                    // Set $diff to n hours ago
                    $diff = "há {$diff->h} horas";
                }
                // Verifies if the user was created this budget at least one minute ago
                else if ($diff->i > 0) {
                    // Set $diff to n minutes ago
                    $diff = "há {$diff->i} minutos";
                }
                else {
                    // Set $diff to now
                    $diff = "agora mesmo";
                }

                // Add the formatted data to view data
                $data['budgets'][$index] = (object)[
                    "id"         => $budget->id,
                    "clientName" => $budget->clientName,
                    "createdAt"  => $createdAt,
                    "diff"       => $diff
                ];
            }
        }

        // Render the home view
        return view('home', $data);
    }
}
