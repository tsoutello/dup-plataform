<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * Verifies if a client exists.
     * 
     * @param string $email
     * @param int $userId
     * 
     * @return mixed
     */
    public static function verifyClientExists(String $email, Int $userId)
    {
        // Search by the user client
        $client = Client::where("email", $email)->where("user_id", $userId)->limit(1)->get();

        // Verifies if the search returned any result
        if ($client->isEmpty()) {
            // Return false
            return false;
        } else {
            // Return the client data
            return $client[0];
        }
    }

    /**
     * Retrieve all user clients.
     * 
     * @param int $userId
     * 
     * @return array $clients
     */
    public static function getUserClients(Int $userId)
    {
        // Retrieve the clients with user_id field assigned to provided $userId
        $clients = Client::select(['name', 'email'])->where("user_id", $userId)->get();

        // Verifies if the search has returned any result
        if ($clients->isEmpty()) {
            // Return a empty array
            return array();
        } else {
            // Return the retrieved results
            return $clients;
        }
    }
}
