-- MariaDB dump 10.19  Distrib 10.6.3-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: dup
-- ------------------------------------------------------
-- Server version	10.6.3-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `budgets`
--

DROP TABLE IF EXISTS `budgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `client` int(11) NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budgets`
--

LOCK TABLES `budgets` WRITE;
/*!40000 ALTER TABLE `budgets` DISABLE KEYS */;
INSERT INTO `budgets` VALUES (1,1,1,'{\"environment\":[{\"environmentID\":\"1\",\"environmentName\":\"Quarto 3\",\"furnitures\":[{\"furniture\":{\"furnitureID\":\"1\",\"furnitureAlias\":\"Guarda-Roupas 2\"},\"data\":{\"model\":{\"modelID\":\"7\",\"modelWidth\":2.2,\"modelHeight\":2.65,\"modelDepth\":0.6},\"finishing\":{\"type\":\"left-finishing\",\"width\":\"75\"},\"thickness\":{\"internal\":\"15\",\"external\":\"15\",\"shelves\":\"15\"},\"advancedCustomization\":{\"doors\":[\"1\",\"2\",null],\"mirrorName\":\"Vidro\",\"mirrorColor\":null},\"moduleColor\":{\"internal\":\"Roxo\",\"internalCode\":\"#836FFF\",\"external\":\"Gianduia Tr...\",\"externalCode\":\"#8f8681\"},\"tapeColor\":{\"internal\":\"Gianduia Tr...\",\"internalCode\":\"#8f8681\",\"external\":\"Grafite Trama\",\"externalCode\":\"#2f2d2e\"},\"doorsPuller\":{\"pullerName\":\"Cava\",\"colors\":{\"colorName\":\"Preto\",\"colorCode\":null},\"noPuller\":false},\"drawersPuller\":{\"drawerName\":null,\"colors\":{\"colorName\":null,\"colorCode\":null},\"noPuller\":true},\"sliderId\":\"4\",\"shelvesSupportId\":\"4\"}},{\"furniture\":{\"furnitureID\":\"1\",\"furnitureAlias\":\"Guarda-Roupas 01\"},\"data\":{\"model\":{\"modelID\":\"6\",\"modelWidth\":1.8,\"modelHeight\":2.65,\"modelDepth\":0.6},\"finishing\":{\"type\":\"no-finishing\",\"width\":null},\"thickness\":{\"internal\":\"15\",\"external\":\"15\",\"shelves\":\"15\"},\"moduleColor\":{\"internal\":\"Branco Ultra\",\"internalCode\":\"#fefefe\",\"external\":\"Branco Ultra\",\"externalCode\":\"#fefefe\"},\"tapeColor\":{\"internal\":\"Branco Ultra\",\"internalCode\":\"#fefefe\",\"external\":\"Branco Ultra\",\"externalCode\":\"#fefefe\"},\"doorsPuller\":{\"pullerName\":\"Tipo G\",\"colors\":{\"colorName\":\"Anodizado\",\"colorCode\":null},\"noPuller\":false},\"drawersPuller\":{\"drawerName\":\"Tipo G\",\"colors\":{\"colorName\":\"Navy\",\"colorCode\":\"#000080\"},\"noPuller\":false},\"sliderId\":\"1\",\"shelvesSupportId\":\"3\"}}]},{\"environmentID\":\"1\",\"environmentName\":\"Outro Quarto\",\"furnitures\":[{\"furniture\":{\"furnitureID\":\"1\",\"furnitureAlias\":\"Outro Guarda-Roupas\"},\"data\":{\"model\":{\"modelID\":\"22\",\"modelWidth\":3.3,\"modelHeight\":2.65,\"modelDepth\":0.6},\"finishing\":{\"type\":\"both-finishing\",\"width\":\"30\"},\"thickness\":{\"internal\":\"15\",\"external\":\"15\",\"shelves\":\"15\"},\"moduleColor\":{\"internal\":\"Branco Ultra\",\"internalCode\":\"#fefefe\",\"external\":\"Branco Ultra\",\"externalCode\":\"#fefefe\"},\"tapeColor\":{\"internal\":\"Branco Ultra\",\"internalCode\":\"#fefefe\",\"external\":\"Branco Ultra\",\"externalCode\":\"#fefefe\"},\"doorsPuller\":{\"pullerName\":\"Tipo G\",\"colors\":{\"colorName\":\"Anodizado\",\"colorCode\":null},\"noPuller\":false},\"drawersPuller\":{\"drawerName\":\"Tipo G\",\"colors\":{\"colorName\":\"Anodizado\",\"colorCode\":null},\"noPuller\":false},\"sliderId\":\"1\",\"shelvesSupportId\":\"1\"}}]}]}','2021-07-26 13:48:15');
/*!40000 ALTER TABLE `budgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clients_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'Testador','adr@adr.com',NULL,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'Branco Ultra','#fefefe','2021-07-15 02:47:31','2021-07-15 02:47:31'),(2,'Branco TX','#e9e9e9','2021-07-15 02:47:31','2021-07-15 02:47:31'),(3,'Cinza Sagra...','#7d7f7e','2021-07-15 02:47:31','2021-07-15 02:47:31'),(4,'Preto TX','#151b20','2021-07-15 02:47:31','2021-07-15 02:47:31'),(5,'Gianduia Tr...','#8f8681','2021-07-15 02:47:31','2021-07-15 02:47:31'),(6,'Titânio Trama','#cec4bb','2021-07-15 02:47:31','2021-07-15 02:47:31'),(7,'Grafite Trama','#2f2d2e','2021-07-15 02:47:31','2021-07-15 02:47:31');
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `furnitures`
--

DROP TABLE IF EXISTS `furnitures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `furnitures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` bigint(20) unsigned NOT NULL,
  `mounting_environment` bigint(20) unsigned NOT NULL,
  `available` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `furnitures`
--

LOCK TABLES `furnitures` WRITE;
/*!40000 ALTER TABLE `furnitures` DISABLE KEYS */;
INSERT INTO `furnitures` VALUES (1,'Guarda-Roupas',34,1,1,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(2,'Criado-mudo',29,1,0,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(3,'Cabeceira',31,1,0,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(4,'Closet',30,1,0,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(5,'Maleiro',32,1,0,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(6,'Painel',33,1,0,'2021-07-15 02:47:31','2021-07-15 02:47:31');
/*!40000 ALTER TABLE `furnitures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `src` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,NULL,'uploads/environments/bathroom.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(2,NULL,'uploads/environments/kitchen.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(3,NULL,'uploads/environments/living-room.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(4,NULL,'uploads/environments/other.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(5,NULL,'uploads/environments/room.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(6,NULL,'uploads/models/room/wardrobe/model-1.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(7,NULL,'uploads/models/room/wardrobe/model-2.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(8,NULL,'uploads/models/room/wardrobe/model-3.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(9,NULL,'uploads/models/room/wardrobe/model-4.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(10,NULL,'uploads/models/room/wardrobe/model-5.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(11,NULL,'uploads/models/room/wardrobe/model-6.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(12,NULL,'uploads/models/room/wardrobe/model-7.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(13,NULL,'uploads/models/room/wardrobe/model-8.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(14,NULL,'uploads/models/room/wardrobe/model-9.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(15,NULL,'uploads/models/room/wardrobe/model-10.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(16,NULL,'uploads/models/room/wardrobe/model-11.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(17,NULL,'uploads/models/room/wardrobe/model-12.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(18,NULL,'uploads/models/room/wardrobe/model-13.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(19,NULL,'uploads/models/room/wardrobe/model-14.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(20,NULL,'uploads/models/room/wardrobe/model-15.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(21,NULL,'uploads/models/room/wardrobe/model-17.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(22,NULL,'uploads/models/room/wardrobe/model-18.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(23,NULL,'uploads/models/room/wardrobe/model-19.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(24,NULL,'uploads/models/room/wardrobe/model-20.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(25,NULL,'uploads/models/room/wardrobe/model-21.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(26,NULL,'uploads/models/room/wardrobe/model-22.jpg','2021-07-15 02:47:31','2021-07-15 02:47:31'),(27,'individual-pieces','uploads/generic/individual-pieces.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(28,'ironware','uploads/generic/ironware.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(29,NULL,'uploads/furnitures/bedside-table.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(30,NULL,'uploads/furnitures/closet.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(31,NULL,'uploads/furnitures/headboard.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(32,NULL,'uploads/furnitures/panel.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(33,NULL,'uploads/furnitures/suitcase.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(34,NULL,'uploads/furnitures/wardrobe.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(35,NULL,'uploads/structures/wardrobe/sliding-doors.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(36,NULL,'uploads/structures/wardrobe/swing-doors.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(37,'advanced-customization','uploads/generic/advanced-customization.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(38,NULL,'uploads/generic/bronze-mirror.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(39,NULL,'uploads/generic/bronze-reflector-mirror.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(40,NULL,'uploads/generic/common-mirror.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(41,NULL,'uploads/generic/white-glass.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(42,'wardrobe-advanced-customization-item','advanced-customization/built-in-socket.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(43,'wardrobe-advanced-customization-item','advanced-customization/ceiling-slope.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(44,'wardrobe-advanced-customization-item','advanced-customization/machining.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(45,'wardrobe-advanced-customization-item','advanced-customization/mirror-doors.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(46,'wardrobe-advanced-customization-item','advanced-customization/tamponade.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(47,'wardrobe-advanced-customization-item','advanced-customization/thickened-side.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(48,'puller-anodizado','uploads/pullers/anodizado.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(49,'puller-champagne','uploads/pullers/champagne.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(50,'puller-preto','uploads/pullers/preto.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(51,'no-puller','uploads/pullers/no-puller.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(52,NULL,'uploads/pullers/anti-bending.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(53,NULL,'uploads/pullers/collegato.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(54,NULL,'uploads/pullers/pit.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(55,NULL,'uploads/pullers/point.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(56,NULL,'uploads/pullers/shell.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(57,NULL,'uploads/pullers/type-g.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(58,'coupled-suitcase','uploads/generic/coupled-suitcase.png','2021-07-15 02:47:31','2021-07-15 02:47:31'),(59,NULL,'uploads/supports/vb.png','2021-07-22 13:16:20','2021-07-22 13:16:20'),(60,NULL,'uploads/supports/vb-double.png','2021-07-22 13:16:20','2021-07-22 13:16:20'),(61,NULL,'uploads/supports/vb56.png','2021-07-22 13:16:20','2021-07-22 13:16:20'),(62,NULL,'uploads/supports/bolt.png','2021-07-22 13:16:20','2021-07-22 13:16:20'),(63,NULL,'uploads/sliders/slider-1.png','2021-07-22 13:16:20','2021-07-22 13:16:20'),(64,NULL,'uploads/sliders/slider-2.png','2021-07-22 13:16:20','2021-07-22 13:16:20'),(65,NULL,'uploads/sliders/slider-3.png','2021-07-22 13:16:20','2021-07-22 13:16:20'),(66,NULL,'uploads/sliders/slider-4.png','2021-07-22 13:16:20','2021-07-22 13:16:20');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (7,'2014_10_12_000000_create_users_table',1),(8,'2014_10_12_100000_create_password_resets_table',1),(9,'2019_08_19_000000_create_failed_jobs_table',1),(11,'2021_07_09_163538_create_mounting_environments_table',1),(12,'2021_07_09_163545_create_doors_table',1),(14,'2021_07_09_163612_create_wood_specifications_table',1),(15,'2021_07_09_163620_create_mirrors_table',1),(16,'2021_07_09_163627_create_colors_table',1),(17,'2021_07_09_163632_create_pullers_table',1),(18,'2021_07_09_163639_create_doors_sliders_table',1),(19,'2021_07_09_163652_create_shelf_supports_table',1),(20,'2021_07_09_163657_create_images_table',1),(21,'2021_07_09_163708_create_budgets_table',1),(22,'2021_07_09_163524_create_clients_table',2),(23,'2021_07_14_120645_create_furniture_table',3),(24,'2021_07_14_122010_create_structures_table',3),(25,'2021_07_14_142354_create_sliders_table',3),(27,'2021_07_09_163555_create_module_models_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mirrors`
--

DROP TABLE IF EXISTS `mirrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mirrors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mirrors`
--

LOCK TABLES `mirrors` WRITE;
/*!40000 ALTER TABLE `mirrors` DISABLE KEYS */;
INSERT INTO `mirrors` VALUES (1,'Comum','Espelho comum',40,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(2,'Vidro','Vidro Branco',41,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(3,'Bronze','Espelho Bronze',38,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(4,'Bronze','Bronze Reflecto',39,'2021-07-15 02:47:31','2021-07-15 02:47:31');
/*!40000 ALTER TABLE `mirrors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_models`
--

DROP TABLE IF EXISTS `module_models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_models` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doors_amount` int(11) NOT NULL,
  `drawers_amount` int(11) NOT NULL,
  `shelves_amount` int(11) NOT NULL,
  `width` double(4,2) NOT NULL,
  `height` double(4,2) NOT NULL,
  `depth` double(4,2) NOT NULL,
  `image` bigint(20) unsigned NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_models`
--

LOCK TABLES `module_models` WRITE;
/*!40000 ALTER TABLE `module_models` DISABLE KEYS */;
INSERT INTO `module_models` VALUES (1,'MODELO 1 (2P8GAV2CAB)',2,8,0,1.80,2.65,0.60,6,'2 Portas deslizantes, 2 Cabideiros, 8 Gavetas e 2 Maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(2,'MODELO 2 (2P6GAV2SAP2CAB)',2,6,0,1.80,2.65,0.60,7,'2 Portas deslizantes, 2 Cabideiros, 6 Gavetas, 2 Sapateiros e 2 Maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(3,'MODELO 3 (2P4GAV4SAP2CAB)',2,4,0,1.80,2.65,0.60,8,'2 Portas deslizantes, 2 Cabideiros, 4 Gavetas, 4 Sapateiros e 2 Maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(4,'MODELO 4 (4GAV3CAB)',4,8,0,1.80,2.65,0.60,9,'2 Portas deslizantes, 3 Cabideiros, 4 Gavetas e 2 Maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(5,'MODELO 5 (3GAV1SAP3CAB)',3,8,0,1.80,2.65,0.60,10,'2 Portas deslizantes, 3 Cabideiros, 3 Gavetas, 1 Sapateiros e 2 Maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(6,'MODELO 6 (2GAV2SAP3CAB)',2,8,0,1.80,2.65,0.60,11,'2 Portas deslizantes, 3 Cabideiros, 2 Gavetas, 2 Sapateiros e 2 Maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(7,'MODELO 7 (8GAV3CAB5PRATP)',3,8,5,2.20,2.65,0.60,12,'3 Portas deslizantes, 3 Cabideiros, 8 Gavetas, 5 Prateleiras e 2 Maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(8,'MODELO 8 (6GAV2SAP3CAB5PRATP)',3,6,5,2.20,2.65,0.60,13,'3 Portas deslizantes, 3 Cabideiros, 6 Gavetas, 2 Sapateiro, 5 Prateleiras e 3 Maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(9,'MODELO 9 (4GAV4SAP3CAB5PRATP)',3,4,5,2.20,2.65,0.60,14,'3 Portas deslizantes, 3 Cabideiros, 4 Gavetas, 4 Sapateiro, 5 Prateleiras  e 3 Maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(10,'MODELO 10 (6GAV2SAP2CAB5PRATG)',3,6,5,2.20,2.65,0.60,15,'3 Portas deslizantes, 2 Cabideiros, 6 Gavetas, 2 Sapateiro, 5 Prateleiras G e 2 Maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(11,'MODELO 11 (4GAV4SAP2CAB5PRATG)',3,4,5,2.20,2.65,0.60,16,'3 Portas deslizantes, 2 Cabideiros, 4 Gavetas, 4 Sapateiro, 5 Prateleiras G e 2 Maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(12,'MODELO 12 (4GAV4SAP2CAB5PRATG1GAVP)',3,5,5,2.20,2.65,0.60,17,'3 Portas deslizantes, 2 Cabideiros, 4 Gavetas, 4 Sapateiro, 5 Prateleiras G, 2 Maleiros e 1 Gaveta P <br><br>3 divisões com 2 portas','2021-07-15 02:47:31','2021-07-15 02:47:31'),(13,'MODELO 13 (4GAV8SAP2CAB2PRAT)',3,4,2,2.20,2.65,0.60,18,'3 Portas deslizantes, 2 cabideiros, 4 gavetas, 8 sapateiros, 2 prateleiras, 3 maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(14,'MODELO 14 (4GAV4SAP2CAB4PRAT)',3,4,4,2.20,2.65,0.60,19,'3 Portas deslizantes, 2 cabideiros, 4 gavetas, 4 sapateiros, 4 prateleiras, 3 maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(15,'MODELO 15 (8GAV2CAB4PRAT)',3,8,4,2.20,2.65,0.60,20,'3 Portas deslizantes, 2 cabideiros, 8 gavetas, 4 prateleiras, 3 maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(16,'MODELO 16 (4GAV4SAP3CAB2PRAT)',3,4,2,2.20,2.65,0.60,21,'3 Portas deslizantes, 3 cabideiros, 4 gavetas, 4 sapateiros, 2 prateleiras, 3 maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(17,'MODELO 17 (6GAV6SAP2CAB2PRAT)',3,6,2,2.20,2.65,0.60,22,'3 Portas deslizantes, 2 cabideiros, 6 gavetas, 6 sapateiros, 2 prateleiras, 2 maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(18,'MODELO 18 (8GAV4SAP4CAB7PRAT)',4,8,7,3.30,2.65,0.60,23,'4 Portas deslizantes, 4 cabideiros, 8 gavetas, 4 sapateiros, 7 prateleiras, 4 maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(19,'MODELO 19 (6GAV6SAP4CAB7PRAT)',4,6,7,3.30,2.65,0.60,24,'4 Portas deslizantes, 4 cabideiros, 6 gavetas, 6 sapateiros, 7 prateleiras, 4 maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(20,'MODELO 20 (4GAV8SAP4CAB7PRAT)',4,4,7,3.30,2.65,0.60,25,'4 Portas deslizantes, 4 cabideiros, 4 gavetas, 8 sapateiros, 7 prateleiras, 4 maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(21,'MODELO 21 (8GAV4CAB10PRAT)',4,8,8,3.30,2.65,0.60,26,'4 Portas deslizantes, 4 cabideiros, 8 gavetas, 10 prateleiras, 4 maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31'),(22,'MODELO 22 (4GAV4SAP5CAB3PRAT)',4,4,3,3.30,2.65,0.60,26,'4 Portas deslizantes, 5 cabideiros, 4 gavetas, 4 sapateira, 3 prateleiras, 4 maleiros','2021-07-15 02:47:31','2021-07-15 02:47:31');
/*!40000 ALTER TABLE `module_models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mounting_environments`
--

DROP TABLE IF EXISTS `mounting_environments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mounting_environments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ambient_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ambient_image` bigint(20) unsigned NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mounting_environments`
--

LOCK TABLES `mounting_environments` WRITE;
/*!40000 ALTER TABLE `mounting_environments` DISABLE KEYS */;
INSERT INTO `mounting_environments` VALUES (1,'Quarto',5,1,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(2,'Cozinha',2,0,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(3,'Banheiro',1,0,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(4,'Sala',3,0,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(5,'Outro Ambiente',4,0,'2021-07-15 02:47:31','2021-07-15 02:47:31');
/*!40000 ALTER TABLE `mounting_environments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pullers`
--

DROP TABLE IF EXISTS `pullers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pullers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` bigint(20) unsigned NOT NULL,
  `top_seller` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pullers`
--

LOCK TABLES `pullers` WRITE;
/*!40000 ALTER TABLE `pullers` DISABLE KEYS */;
INSERT INTO `pullers` VALUES (1,'Tipo G','Perfil Tipo G',57,1,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(2,'Concha','Puxador Concha',56,0,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(3,'Cava','Puxador Cava 45',54,0,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(4,'Ponto','Inox Polido',55,0,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(5,'Collegato','Perfil Collegato',53,0,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(6,'Anti Empeno','Inox polido',52,0,'2021-07-15 02:47:31','2021-07-15 02:47:31');
/*!40000 ALTER TABLE `pullers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shelf_supports`
--

DROP TABLE IF EXISTS `shelf_supports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shelf_supports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shelf_supports`
--

LOCK TABLES `shelf_supports` WRITE;
/*!40000 ALTER TABLE `shelf_supports` DISABLE KEYS */;
INSERT INTO `shelf_supports` VALUES (1,'Pino Pratelei...','Perfil Tipo G',62,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(2,'VB','Puxador Cava 45',59,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(3,'VB 56','Puxador Concha',61,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(4,'VB Duplo','Sem Puxador',60,'2021-07-15 02:47:31','2021-07-15 02:47:31');
/*!40000 ALTER TABLE `shelf_supports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (1,'Telescópia PESADA','Telescópica',63,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(2,'Telescópia PESADA FECHO TOQUE','Telescópica',64,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(3,'Inivisível Com amortecimento','Invisível',65,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(4,'Inivisível ONETOUCH','Invisível',66,'2021-07-15 02:47:31','2021-07-15 02:47:31');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `structures`
--

DROP TABLE IF EXISTS `structures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` bigint(20) unsigned NOT NULL,
  `furniture` bigint(20) unsigned NOT NULL,
  `available` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `structures`
--

LOCK TABLES `structures` WRITE;
/*!40000 ALTER TABLE `structures` DISABLE KEYS */;
INSERT INTO `structures` VALUES (1,'Portas de CORRER',35,1,1,NULL,NULL),(2,'Portas de GIRO',36,1,0,NULL,NULL);
/*!40000 ALTER TABLE `structures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Autônomo',
  `email_verified_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Teste 01','dgs190plc@outlook.com','Autônomo','2021-07-09 21:31:26','$2y$10$vlFMOMufHR4rN3B.wqr9reC3DuOOz/5xECqgLtAKqIVhdUv.mzO5u','IXwl5h0sK9QaAAbSVyEud3IAAHNiBRNUvMvGRVR7m2G0UMbL13r8c3E0Jxdb','2021-07-10 00:31:26','2021-07-10 00:31:26');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wood_specifications`
--

DROP TABLE IF EXISTS `wood_specifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wood_specifications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `thickness_measure` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wood_specifications`
--

LOCK TABLES `wood_specifications` WRITE;
/*!40000 ALTER TABLE `wood_specifications` DISABLE KEYS */;
INSERT INTO `wood_specifications` VALUES (1,15,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(2,18,'2021-07-15 02:47:31','2021-07-15 02:47:31'),(3,25,'2021-07-15 02:47:31','2021-07-15 02:47:31');
/*!40000 ALTER TABLE `wood_specifications` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-26 19:52:00
