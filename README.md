# DUP - Laravel
Aqui você encontrará um guia para instalação do MVP DUP, escrito em PHP, sob o framework Laravel 7, atualmente na versão **1.0.0-dev**

#### Tópicos
* Requisitos
	* Instalação dos requisitos em computadores Windows
* Instalação local
* Instalação em uma hospedagem Hostinger

## Requisitos
Como requisitos para o projeto, estão os itens listados abaixo:
* PHP >= 7.2
* MySQL >= 8.0 (ou MariaDB ^10)
* Composer 2
* Git mais recente

### Instalação dos requisitos em computadores Windows
Para execução local em uma máquina Windows, é recomendado que você faça o download de uma pilha AMP que tiver mais familiaridade (XAMPP, WAMPP, etc), dado que estes já possuem o PHP e o MySQL disponíveis. Após concluir o download e a instalação da pilha, o próximo passo é acessar [a página de downloads do git](https://git-scm.com/downloads), e fazer o download para a plataforma Windows.

Após concluir a instalação do Git, o último requisito é acessar [https://git-scm.com/downloads](https://git-scm.com/downloads) e fazer o download e instalação para sua plataforma

## Instalação local
Para instalar localmente, basta baixar o projeto zippado do repositório principal e descompacta-lo em uma pasta de sua preferência.
Após isso, renomeio o arquivo *.env.example* dentro da pasta *src/* para *.env* e preencha os dados de conexão do banco de dados (DB) e do email usado para envios (MAIL).

Por último, você deve abrir um shell de comando (em máquinas Windows, utilize o shell que vem embutido em sua pilha AMP), navegar até a pasta onde o projeto foi extraído e rodar os seguintes comandos (a partir da raiz do projeto)

```
cd src

php artisan migrate
php artisan serve
```

Após isso, a aplicação poderá ser acessada a partir de qualquer navegador, pelo endereço http://localhost:8000
